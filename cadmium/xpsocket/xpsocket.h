/**
@file
@brief Hides the differences between POSIX and WinSock sockets.

Copyright (C) 2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_XPSOCKET_H__
#define __QQQ_XPSOCKET_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Include platform-specific, network-related header files for general use. */

#ifdef _WIN32
  /* See http://stackoverflow.com/questions/12765743/getaddrinfo-on-win32 */
  #ifndef _WIN32_WINNT
    #define _WIN32_WINNT 0x0501  /* Windows XP. */
  #endif
  #include <winsock2.h>
  #include <Ws2tcpip.h>
#else
  /* Assume that any non-Windows platform uses POSIX-style sockets instead. */
  #include <sys/socket.h>
  #include <arpa/inet.h>
  #include <netdb.h>
  #include <unistd.h>
  /* Hide the differences between WinSock and POSIX socket handle types. */
  #define INVALID_SOCKET -1 /**< @brief This definition differs from Windows. */
  typedef int SOCKET;       /**< @brief This is an UNSIGNED value on Windows. */
#endif

/**
@brief Determine whether a given socket value is "valid" or not.

@param sock The socket value to check.
@return True if the socket is considered "valid", or false if not.
*/

#ifdef _WIN32
  #define sockValid(sock) ((sock) != INVALID_SOCKET)
#else
  #define sockValid(sock) ((sock) >= 0)
#endif

/**
@brief Initialise networking. Call this first.

This has no effect on POSIX systems. On Windows, it readies the WinSock DLL.

@return Zero on success, or non-zero on falure.
*/

int sockInit(void);

/**
@brief Shut down networking. Call this last.

This has no effect on POSIX systems. On Windows, it frees the WinSock DLL.

@return Zero on success, or non-zero on falure.
*/

int sockQuit(void);

/**
@brief Attempt to open a new TCP socket.

There's nothing Windows/POSIX-specific in here; this is just for convenience.

@param address A web address, such as "www.example.com"
@param port A port number, such as "80".
@return A valid socket handle on success, or INVALID_SOCKET on failure.
*/

SOCKET sockConnect(const char *address, const char *port);

/**
@brief Close a previously-opened socket.

The internal details are slightly different for Windows and POSIX, but it MUST
be called on both platforms.

@param sock The socket to be closed.
@return Zero on success, or non-zero on falure.
*/

int sockClose(SOCKET *sock);

/**
@brief Send data over a socket.

There's nothing Windows/POSIX-specific here; this is just a convenient wrapper
around the usual send() function.

@param sock The socket to send data from.
@param data The data to be sent.
@return Zero on success, or non-zero on falure.
*/

int sockSend(SOCKET sock, const char *data);

/**
@brief Receive data from a socket.

There's nothing Windows/POSIX-specific here; this is just a convenient way to
receive an arbitrary-length string.

@param sock The socket to receive data.
@return A malloc()'d string on success, or NULL on failure.
*/

char *sockRecv(SOCKET sock);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_XPSOCKET_H__ */
