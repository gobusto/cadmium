/**
@file
@brief Hides the differences between POSIX and WinSock sockets.

Copyright (C) 2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "xpsocket.h"

/*
[PUBLIC] Initialise networking. WINDOWS ONLY: Ready the WinSock DLL.
*/

int sockInit(void)
{
  #ifdef _WIN32
    WSADATA wsa_data;
    return WSAStartup(MAKEWORD(1, 1), &wsa_data);
  #else
    return 0;
  #endif
}

/*
[PUBLIC] Shut-down networking. WINDOWS ONLY: Stop using the WinSock DLL.
*/

int sockQuit(void)
{
  #ifdef _WIN32
    return WSACleanup();
  #else
    return 0;
  #endif
}

/*
[PUBLIC] Open a socket. Nothing Windows-specific here - just for convenience.
*/

SOCKET sockConnect(const char *address, const char *port)
{
  SOCKET sock;

  struct addrinfo *address_list;
  struct addrinfo *info;

  /* Get a linked list of address information structures. */
  if (getaddrinfo(address, port, NULL, &address_list) != 0) { return INVALID_SOCKET; }

  /* Go through each item in the list and attempt to connect with it. */
  for (info = address_list; info; info = info->ai_next)
  {
    sock = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
    if (sockValid(sock))
    {
      /* FIXME: This is a BLOCKING socket, so the program could freeze here! */
      if (connect(sock, info->ai_addr, info->ai_addrlen) == 0) { break; }
      sockClose(&sock);
    }
  }

  /* Free the address information list and return the resulting socket ID. */
  freeaddrinfo(address_list);
  return sock;
}

/*
[PUBLIC] Close a socket.
*/

int sockClose(SOCKET *sock)
{
  int status = 0;
  if (!sock) { return 0; }

  #ifdef _WIN32
    status = shutdown(*sock, SD_BOTH);
    if (status == 0) { status = closesocket(*sock); }
  #else
    status = shutdown(*sock, SHUT_RDWR);
    if (status == 0) { status = close(*sock); }
  #endif

  *sock = INVALID_SOCKET;
  return status;
}

/*
[PUBLIC] Send data over a socket.
*/

int sockSend(SOCKET sock, const char *d) { return send(sock, d, strlen(d), 0) < 0; }

/*
[PUBLIC] Receive data from a socket.
*/

char *sockRecv(SOCKET sock)
{
  char *result = NULL;
  size_t result_len = 0;

  while (1)
  {

    char buffer[64];
    void *tmp = NULL;

    size_t buffer_len = recv(sock, buffer, 64, 0);
    if (buffer_len < 1) { return result; }

    tmp = realloc(result, result_len + buffer_len + 1);
    if (!tmp) { free(result); return NULL; } else { result = (char*)tmp; }

    memcpy(&result[result_len], buffer, buffer_len);
    result_len += buffer_len;
    result[result_len] = 0;

  }
}
