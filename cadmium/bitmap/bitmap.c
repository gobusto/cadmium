/**
@file
@brief Provides utilities for loading image files.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

/**
@brief Create a new (blank) image structure.

@param w The width of the image in pixels.
@param h The height of the image in pixels.
@param bpp The number of bits per pixel.
@return A new image structure on success, or `NULL` on failure.
*/

static BitmapImage *bmpCreate(BitmapSize w, BitmapSize h, BitmapBPP bpp)
{
  BitmapImage *image = NULL;

  if (w < 1 || h < 1) { return NULL; }
  if (bpp != 8 && bpp != 16 && bpp != 24 && bpp != 32) { return NULL; }

  image = (BitmapImage*)malloc(sizeof(BitmapImage));
  if (!image) { return NULL; }
  memset(image, 0, sizeof(BitmapImage));

  image->w = w;
  image->h = h;
  image->bpp = bpp;

  image->data = (BitmapByte*)malloc(sizeof(BitmapByte) * w * h * bpp / 8);

  if (!image->data)
  {
    free(image);
    return NULL;
  }

  return image;
}

/*
[PUBLIC] Load an image file into memory.
*/

BitmapImage *bmpLoadData(int len, const unsigned char *data)
{
  BitmapImage *image = NULL;
  stbi_uc *stb_data;
  int x, y, channels;

  if (!stbi_info_from_memory(data, len, &x, &y, &channels)) { return NULL; }
  channels = channels < 4 ? 3 : 4;
  stb_data = stbi_load_from_memory(data, len, &x, &y, NULL, channels);

  if (stb_data) { image = bmpCreate(x, y, channels * 8); }
  if (image) { memcpy(image->data, stb_data, x * y * channels); }
  stbi_image_free(stb_data);

  return image;
}

/*
[PUBLIC] Free a previously-created image structure from memory.
*/

BitmapImage *bmpDestroy(BitmapImage *image)
{
  if (!image) { return NULL; }
  if (image->data) { free(image->data); }
  free(image);
  return NULL;
}
