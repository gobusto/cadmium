/**
@file
@brief Provides utilities for loading image files.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CADMIUM_BITMAP_H__
#define __CADMIUM_BITMAP_H__

#ifdef __cplusplus
extern "C" {
#endif

/* General types used to represent images: */
typedef unsigned long BitmapSize; /**< @brief Stores width/height values. */
typedef unsigned char BitmapBPP;  /**< @brief Stores bits-per-pixel values. */
typedef unsigned char BitmapByte; /**< @brief Stores individual R/G/B/A data. */

/**
@brief This structure represents a single image.

Possible bits-per-pixel values are:

+ 8 (Grey)
+ 16 (Interleaved Grey + Opacity)
+ 24 (Interleaved RGB)
+ 32 (Interleaved RGBA)

Note that all images are represented as 8-bits-per-channel, with no padding for
per-row alignment.
*/

typedef struct
{

  BitmapSize w;     /**< @brief The width of the image in pixels. */
  BitmapSize h;     /**< @brief The height of the image in pixels. */
  BitmapBPP  bpp;   /**< @brief The number of bits per pixel. */
  BitmapByte *data; /**< @brief A raw data buffer representing pixel values. */

} BitmapImage;

/**
@brief Load an image file into memory.

@param len The length od the data buffer.
@param data A raw binary data buffer.
@return A new image structure on success, or `NULL` on failure.
*/

BitmapImage *bmpLoadData(int len, const unsigned char *data);

/**
@brief Free a previously-created image structure from memory.

@param image The image structure to be deleted.
@return Always returns `NULL`.
*/

BitmapImage *bmpDestroy(BitmapImage *image);

#ifdef __cplusplus
}
#endif

#endif /* __CADMIUM_BITMAP_H__ */
