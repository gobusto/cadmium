/**
@file
@brief Handles CSS parsing. See <http://www.quirksmode.org/dom/w3c_css.html>

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CSS_PARSER_H__
#define __CSS_PARSER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include "error/error.h"

#define CSS_SHEET_DISABLED  0x01  /**< @brief Is this sheet disabled? */
#define CSS_SHEET_RESERVED1 0x02  /**< @brief Not used.               */
#define CSS_SHEET_RESERVED2 0x04  /**< @brief Not used.               */
#define CSS_SHEET_RESERVED3 0x08  /**< @brief Not used.               */
#define CSS_SHEET_RESERVED4 0x10  /**< @brief Not used.               */
#define CSS_SHEET_RESERVED5 0x20  /**< @brief Not used.               */
#define CSS_SHEET_RESERVED6 0x40  /**< @brief Not used.               */
#define CSS_SHEET_RESERVED7 0x80  /**< @brief Not used.               */

#define CSS_PROPERTY_IMPORTANT 0x01 /**< @brief Is this value `!important`? */
#define CSS_PROPERTY_RESERVED1 0x02 /**< @brief Not used.                   */
#define CSS_PROPERTY_RESERVED2 0x04 /**< @brief Not used.                   */
#define CSS_PROPERTY_RESERVED3 0x08 /**< @brief Not used.                   */
#define CSS_PROPERTY_RESERVED4 0x10 /**< @brief Not used.                   */
#define CSS_PROPERTY_RESERVED5 0x20 /**< @brief Not used.                   */
#define CSS_PROPERTY_RESERVED6 0x40 /**< @brief Not used.                   */
#define CSS_PROPERTY_RESERVED7 0x80 /**< @brief Not used.                   */

typedef unsigned char css_flag_t; /**< @brief Flags bitfield. */

/**
@brief This structure describes a single import within a stylesheet.

For example, an import might look like this: `@import "a.css" screen;`

@todo Handle the "media query list" stuff (`projection, tv;` etc.)
*/

typedef struct
{

  char *uri;  /**< @brief The URI of the resource to be imported. */

} css_import_s;

/**
@brief This structure describes a single selector within a rule.

For example, a selector might look like this: `p > b`
*/

typedef struct
{

  size_t num_parts; /**< @brief Length of the selector parts array.  */
  char **part;      /**< @brief Array of selector parts: `[p, >, b]` */

} css_selector_s;

/**
@brief This structure describes a single property within a rule.

For example, a property might look like this: `color: blue;`
*/

typedef struct
{

  char *name;   /**< @brief The property name, such as `color`. */
  char *value;  /**< @brief The property value, such as `blue`. */

  css_flag_t flags; /**< @brief Only for `!important` at the moment. */

} css_property_s;

/**
@brief This structure describes a single rule within a stylesheet.

For example, a single rule might look like this: `p > b, h1 { color:blue; }`
*/

typedef struct
{

  size_t num_selectors;       /**< @brief Number of selectors stored.    */
  css_selector_s **selector;  /**< @brief Array of selector text values. */

  size_t num_properties;      /**< @brief Number of properties stored.   */
  css_property_s **property;  /**< @brief Array of property name/values. */

} css_rule_s;

/**
@brief This structure describes a single stylesheet.

This structure is designed to store CSS data in much the same format as it is
given in the source `.css` file, allowing rules to be accessed by index.
*/

typedef struct
{

  char *href;  /**< @brief The `href` property of this stylesheet. */

  size_t num_imports;     /**< @brief Number of `@import` rules. */
  css_import_s **import;  /**< @brief Array of `@import` rules. */

  size_t num_rules;   /**< @brief Number of CSS rules. */
  css_rule_s **rule;  /**< @brief Array of CSS rules. */

  css_flag_t flags; /**< @brief Only for `disabled` at the moment. */

} css_s;

/**
@brief Create a new (blank) stylesheet.

This is equivalent to the Internet Explorer `createStyleSheet()` JS function.

@param href The `href` property of this stylesheet.
@return A new stylesheet on success, or NULL on failure.
*/

css_s *cssCreate(const char *href);

/**
@brief Free a previously-created stylesheet from memory.

@param css The stylesheet structure to be deleted.
@return Always returns NULL.
*/

css_s *cssDelete(css_s *css);

/**
@brief Create a new stylesheet based on a string of CSS text.

This is useful for creating a stylesheet from CSS text downloaded via a network
connection.

@param text A string of ASCII (or UTF-8, or Latin-1) text representing CSS.
@param href The `href` property of this stylesheet.
@return A new stylesheet on success, or NULL on failure.
*/

css_s *cssLoadText(const char *text, const char *href);

/**
@brief Load a stylesheet from a CSS file.

The `href` property is inferred from the filename as a `file://` URI.

@param filename The name and path of the CSS file to be loaded.
@return A new stylesheet on success, or NULL on failure.
*/

css_s *cssLoadFile(const char *filename);

/**
@brief Export a stylesheet structure to a CSS file.

@param css The stylesheet to be exported.
@param filename The name of the file to save as.
@return ERROR_SUCCESS on success, or a suitable error value on failure.
*/

error_t cssSave(css_s *css, const char *filename);

/**
@brief Add a new rule to a stylesheet.

This is equivalent to the Internet Explorer `addRule()` JS function, which does
not require an index to be specified (unlike the W3C standard version).

@param css The stylesheet to be updated.
@param text A string of CSS-style text, such as `p > b, h1 { color: blue; }`
@return ERROR_SUCCESS on success, or a suitable error value on failure.
*/

error_t cssCreateRule(css_s *css, const char *text);

/**
@brief Add a new rule to a stylesheet at a specified position.

This is more or less equivalent to the Javascript `insertRule()` function; only
one rule can be added at a time (Firefox raises a SyntaxError if any extra text
is present after the end of the first "complete" rule).

@param css The stylesheet to be updated.
@param text A string of CSS-style text, such as `p > b, h1 { color: blue; }`
@param i The index at which to insert the new rule.
@return ERROR_SUCCESS on success, or a suitable error value on failure.
*/

error_t cssCreateRuleAtIndex(css_s *css, const char *text, size_t i);

/**
@brief Remove a rule from a stylesheet.

This is more or less equivalent to the Javascript `deleteRule()` function (W3C)
or the `removeRule()` equivalent used by Internet Explorer.

@param css The stylesheet to be updated.
@param i The index of the rule to be deleted.
@return ERROR_SUCCESS on success, or a suitable error value on failure.
*/

error_t cssDeleteRuleAtIndex(css_s *css, size_t i);

/**
@brief Find a property within a rule by name.

This covers two JS functions: `getPropertyValue()` and `getPropertyPriority()`.

@param rule The rule to look at.
@param name The property to search for.
@return The property structure for the specified name (if it exists) or NULL.
*/

css_property_s *cssGetProperty(css_rule_s *rule, const char *name);

/**
@brief Set the value of a property within a rule.

This is more or less equivalent to the Javascript `setProperty()` function; the
`!important` flag must be set explicitly (Firefox ignores `green !important` as
an importance flag indicator and also ignores any values other than "important"
or `null` for the second parameter).

If `text` is `NULL`, the property is deleted as per `removeProperty()` (this is
also what Firefox does).

@param rule The rule to update.
@param name The property to search for.
@param value The new value. An empty string (or `NULL`) *removes* the property.
@param flags The new flags value.
@return ERROR_SUCCESS on success, or a suitable error value on failure.
*/

error_t cssSetProperty(css_rule_s *rule, const char *name, const char *value, css_flag_t flags);

#ifdef __cplusplus
}
#endif

#endif /* __CSS_PARSER_H__ */
