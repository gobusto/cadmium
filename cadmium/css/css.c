/**
@file
@brief Handles CSS parsing. See <https://www.w3.org/TR/css-syntax-3/>

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "css.h"

/**
@brief Determine if a character is considered to be CSS ident character.

Technically, this should include various unicode characters; however, it's only
used to verify property names at the moment, and they (should) always be ASCII.

@param c The character code to check.
@return TRUE if the character is a valid "ident" character or FALSE if not.
*/

#define cssIdent(c) (\
  ((c) >= 'A' && (c) <= 'Z') || ((c) >= '0' && (c) <= '9') || \
  ((c) >= 'a' && (c) <= 'z') || ((c) == '-' || (c) == '_')    \
)

/**
@brief Determine if a character is considered to be CSS whitespace.

Only the characters "space" (U+0020), "tab" (U+0009), "line feed" (U+000A),
"carriage return" (U+000D), and "form feed" (U+000C) can occur in whitespace.

See <http://www.w3.org/TR/css3-selectors/#selector-syntax> for details.

@param c The character code to check.
@return TRUE if the character is considered to be a CSS space or FALSE if not.
*/

#define cssSpace(c) ((c) == 32 || ((c) >= 9 && (c) != 11 && (c) <= 13))

/**
@brief Determine if a character is considered to be a CSS combinator.

As of CSS3, "combinators" are: whitespace, "greater-than sign" (U+003E, >),
"plus sign" (U+002B, +) and "tilde" (U+007E, ~).

See <http://www.w3.org/TR/css3-selectors/#selector-syntax> for details.

@param c The character code to check.
@return TRUE if the character is considered to be a combinator or FALSE if not.
*/

#define cssCombinator(c) (cssSpace(c) || (c) == '>' || (c) == '+' || (c) == '~')

/**
@brief Copy the data between two pointers into a new string.

@param a The start of the data to be copied. This byte is included.
@param b The end of the data to be copied. This byte is NOT included.
@param ignore_whitespace If true, non-quoted whitespace characters are skipped.
@return A new malloc'd buffer containing the data (plus a NULL terminator).
*/

static char *cssCopyString(const char *a, const char *b, int ignore_whitespace)
{
  char *text;
  int pass;

  if (!a || !b) { return NULL; }

  for (pass = 0; pass < 2; ++pass)
  {
    int quote = 0;
    size_t len, i;

    for (i = 0, len = 0; &a[i] < b; ++i)
    {
      /* Determine whether this character should be included in the result: */
      if (!ignore_whitespace || quote || !cssSpace(a[i]))
      {
        if (pass == 1) { text[len] = a[i]; }
        ++len;
      }
      /* Even when "ignoring" whitespace, include it if it's within quotes: */
      if (a[i] == '\'' || a[i] == '"') quote = (quote == a[i]) ? 0 : a[i];
    }
    /* At the end of the first pass, allocate some memory to store the copy: */
    if (pass == 0)
    {
      text = (char*)malloc(len + 1);
      if (!text) { return NULL; }
      text[len] = 0;
    }
  }

  return text;
}

/**
@brief Determine if a string of CSS text contains comments.

This can be used to only create a comment-less copy if it's actually necessary.

@todo Other changes:  <https://www.w3.org/TR/css-syntax-3/#input-preprocessing>

@param text A string of CSS text.
@return TRUE if the text contains one or more comments, or FALSE if not.
*/

static int cssHasComments(const char *text)
{
  int quote_char = 0;
  for (; text && *text; ++text)
    if (quote_char == 0 && text[0] == '/' && text[1] == '*')
      return 1;
    else if (*text == '\'' || *text == '"')
      quote_char = (quote_char == *text) ? 0 : *text;
  return 0;
}

/**
@brief Return a copy of a CSS text string with all comments removed.

@todo Other changes:  <https://www.w3.org/TR/css-syntax-3/#input-preprocessing>

@param original_text A string of CSS text.
@return A new malloc'd string on success, or NULL on failure.
*/

static char *cssStripComments(const char *original_text)
{
  char *copy_of_text = NULL;
  int pass;

  if (!original_text) { return NULL; }

  for (pass = 0; pass < 2; ++pass)
  {
    const char *c = original_text;
    size_t len = 0;
    int quote_char = 0;

    /* Calculate the length of the string WITHOUT comments: */
    while (c && *c)
    {
      /* Keep track of whether we're currently within a quoted string... */
      if (*c == '\'' || *c == '"') { quote_char = (quote_char == *c) ? 0 : *c; }
      /* ...so that we can ignore "comments" if they start within quotes: */
      if (!quote_char && c[0] == '/' && c[1] == '*')
      {
        c = strstr(c + 2, "*/");
        if (c) { c += 2; }
      }
      else
      {
        if (copy_of_text) { copy_of_text[len] = *c; }
        ++c; ++len;
      }
    }

    /* At the end of the first pass, allocate memory for the copy: */
    if (pass == 0)
    {
      copy_of_text = (char*)malloc(len + 1);
      if (!copy_of_text) { return NULL; }
      copy_of_text[len] = 0;
    }
  }

  return copy_of_text;
}

/**
@brief Internal import deletion function.

@todo Free the media query list stuff (when it gets implemented).

@param import The import structure to be deleted.
*/

static void cssDeleteImport(css_import_s *import)
{
  if (!import) { return; }
  free(import->uri);
  free(import);
}

/**
@brief Internal selector deletion function.

@param selector The selector structure to be deleted.
*/

static void cssDeleteSelector(css_selector_s *selector)
{
  size_t i;

  if (!selector) { return; }

  if (selector->part)
  {
    for (i = 0; i < selector->num_parts; ++i) { free(selector->part[i]); }
    free(selector->part);
  }

  free(selector);
}

/**
@brief Internal property deletion function.

@param property The property structure to be deleted.
*/

static void cssDeleteProperty(css_property_s *property)
{
  if (!property) { return; }
  free(property->name);
  free(property->value);
  free(property);
}

/**
@brief Internal rule deletion function.

Unlike the public version, this function accepts a pointer, not an index.

@param rule The rule structure to be deleted.
*/

static void cssDeleteRule(css_rule_s *rule)
{
  size_t i;

  if (!rule) { return; }

  if (rule->selector)
  {
    for (i = 0; i < rule->num_selectors; ++i) { cssDeleteSelector(rule->selector[i]); }
    free(rule->selector);
  }

  if (rule->property)
  {
    for (i = 0; i < rule->num_properties; ++i) { cssDeleteProperty(rule->property[i]); }
    free(rule->property);
  }

  free(rule);
}

/**
@brief Parse an `@abc` rule, skipping over any unrecognised ones.

@todo Support at least `@import` rules!

@param css about
@param text about
@param error about
@return writeme
*/

static const char *cssParseAtRule(css_s *css, const char *text, error_t *error)
{
  const char *c;
  int brace_level = 0;
  int quote_char = 0;

  if (!css || !text || !error) { return NULL; }

  *error = ERROR_SUCCESS;

  /* For unknown @rules, simply search for the terminating ; or } character: */
  for (c = text; *c; ++c)
  {
    if (quote_char == 0)
    {
      if      (*c == '\'' || *c == '"') { quote_char = *c; }
      else if (*c == ';') { break; }
      else if (*c == '{') { ++brace_level; }
      else if (*c == '}')
      {
        if (--brace_level < 1) { break; }
      }
    }
    else if (quote_char == *c) { quote_char = 0; }
  }

  /* Return the NEXT character, unless we've reached the end of the string: */
  return *c ? c + 1 : c;
}

/**
@brief about

@param rule about
@param a about
@param b about
@return writeme
*/

static error_t cssAddSelectorPart(css_rule_s *rule, const char *a, const char *b)
{
  if (a == b) { return ERROR_SUCCESS; }

  if (rule)
  {
    css_selector_s *sel = rule->selector[rule->num_selectors - 1];

    void *tmp = realloc(sel->part, sizeof(char*) * (sel->num_parts + 1));
    if (!tmp) { return ERROR_OUT_OF_MEMORY; }
    sel->part = (char**)tmp;

    sel->part[sel->num_parts] = cssCopyString(a, b, 0);
    if (!sel->part[sel->num_parts]) { return ERROR_OUT_OF_MEMORY; }
    ++sel->num_parts;
  }

  return ERROR_SUCCESS;
}

/**
@brief Parse the selector(s) part of a rule.

@param rule about
@param text about
@param error about
@return writeme
*/

static const char *cssParseSelectors(css_rule_s *rule, const char *text, error_t *error)
{
  const char *c = text;

  int quote_char = 0;
  int attr_block = 0;
  int combinator = 0;
  int prev_char = 0;

  if (!text || !error) { return NULL; }

  *error = ERROR_OUT_OF_MEMORY;

  while (*c)
  {

    /* Allocate memory for a new selector whenever one occurs: */
    if (rule && prev_char == 0)
    {
      void *tmp = realloc(rule->selector, sizeof(css_selector_s*) * (rule->num_selectors + 1));
      if (!tmp)
      {
        *error = ERROR_OUT_OF_MEMORY;
        return c + 1;
      }
      rule->selector = (css_selector_s**)tmp;

      rule->selector[rule->num_selectors] = (css_selector_s*)malloc(sizeof(css_selector_s));
      if (!rule->selector[rule->num_selectors])
      {
        *error = ERROR_OUT_OF_MEMORY;
        return c + 1;
      }
      memset(rule->selector[rule->num_selectors], 0, sizeof(css_selector_s));

      ++rule->num_selectors;
    }

    if (quote_char)
    {
      if (*c == quote_char) { quote_char = 0; }
    }
    else if (attr_block)
    {
      if (*c == ']') { attr_block = 0; }
    }
    else if (!attr_block && strchr(",{", *c))
    {
      if (prev_char == 0 || (combinator && !cssSpace(combinator)))
      {
        /* These can only appear after a simple selector. */
        *error = ERROR_SYNTAX;
        return c + 1;
      }
      /* Add everything up to this point as a new part of the selector: */
      *error = cssAddSelectorPart(rule, text, c);
      if (*error != ERROR_SUCCESS) { return c + 1; }
      /* If this is actually the end, stop here: */
      if (*c == '{') { text = ++c; break; }
      /* Otherwise, move to the start of the next selector statement: */
      for (++c; cssSpace(*c); ++c) { }
      text = c;
      combinator = 0;
      prev_char = 0;
      continue;
    }
    else if (!attr_block && cssCombinator(*c))
    {
      if (prev_char == 0)
      {
        /* A combinator cannot appear at the start of a selector: */
        *error = ERROR_SYNTAX;
        return c + 1;
      }
      else if (combinator && !cssSpace(combinator) && !cssSpace(*c))
      {
        /* If a non-whitespace combinator exists, a second one is invalid: */
        *error = ERROR_SYNTAX;
        return c + 1;
      }
      else if (!combinator || cssSpace(combinator))
      {
        /* Add everything up to this point as a new part of the selector: */
        if (!combinator)
        {
          *error = cssAddSelectorPart(rule, text, c);
          if (*error != ERROR_SUCCESS) { return c + 1; }
        }
        /* If the combinator is missing (or whitespace), use this instead: */
        combinator = *c;
      }
      text = c + 1;
    }
    else
    {
      if (combinator)
      {
        char char_as_str[2] = {0, 0};
        char_as_str[0] = combinator;
        /* Add everything up to this point as a new part of the selector: */
        *error = cssAddSelectorPart(rule, char_as_str, char_as_str + 1);
        if (*error != ERROR_SUCCESS) { return c + 1; }

        text = c;
        combinator = 0;
      }

      if (prev_char == 0 || strchr("#.:[", *c))
      {
        if (prev_char != 0 && strchr("#.:[", prev_char))
        {
          *error = ERROR_SYNTAX;
          return c;
        }
        else
        {
          if (*c == '[') { attr_block = 1; }
        }

        *error = cssAddSelectorPart(rule, text, c);
        if (*error != ERROR_SUCCESS) { return c + 1; }

        text = c;
      }
      else if (*c == '*')
      {
        if (prev_char != 0 && prev_char != '|' && !cssCombinator(prev_char))
        {
          *error = ERROR_SYNTAX;
          return c;
        }

        *error = cssAddSelectorPart(rule, text, c);
        if (*error != ERROR_SUCCESS) { return c + 1; }

        text = c;
      }
      else if (ispunct(*c) && *c != '-' && *c != '_' && *c != '|')
      {
        *error = ERROR_SYNTAX;
        return c;
      }

    }

    /* Move to the next character (using `continue` above avoids this). */
    prev_char = *c; ++c;
  }

  return c;

}

/**
@brief Parse the properties(s) part of a rule.

@param rule about
@param text about
@param error about
@return writeme
*/

static const char *cssParseProperties(css_rule_s *rule, const char *text, error_t *error)
{
  if (!rule || !text || !error) { return NULL; }

  /* Keep parsing rules until we find the end of this block. */
  while (text && *text && *text != '}')
  {
    const char *c;
    int quote_char = 0;

    char *name;
    char *value;
    css_flag_t flags = 0;

    /* We may come across a `}` (end-of-rule) instead of a new property: */
    while (*text && cssSpace(*text)) { ++text; }
    if (*text == 0 || *text == '}') { break; }

    /* Get the "name" part of the property: */
    for (c = text; *c && *c != ':'; ++c) { }
    name = cssCopyString(text, c, 0);
    text = (*c == ':') ? c+1 : c;

    /* Get the "value" part of the property: */
    for (c = text; *c; ++c)
    {
      if (quote_char == 0 && (*c == '!' || *c == ';' || *c == '}'))
        break;
      else if (*c == '\'' || *c == '"')
        quote_char = (quote_char == *c) ? 0 : *c;
    }
    value = cssCopyString(text, c, 0);
    text = (*c == ';') ? c+1 : c;

    /* Get the "!important" part of a property, if it exists: */
    if (*text == '!')
    {
      flags = CSS_PROPERTY_IMPORTANT;
      for (c = text; *c && *c != ';' && *c != '}'; ++c) {}
      text = (*c == ';') ? c+1 : c;
    }

    /* Attempt to add this property to the rule: */
    *error = cssSetProperty(rule, name, value, flags);
    free(name);
    free(value);
  }

  return *text ? text+1 : text;
}

/**
@brief about

writeme

@param css about
@param text about
@param error about
@return writeme
*/

static const char *cssParseRule(css_s *css, const char *text, error_t *error)
{
  void *tmp;

  if (!css || !text || !error) { return NULL; }

  tmp = realloc(css->rule, sizeof(css_rule_s*) * (css->num_rules + 1));
  if (!tmp)
  {
    *error = ERROR_OUT_OF_MEMORY;
    return text + 1;
  }
  css->rule = (css_rule_s**)tmp;

  css->rule[css->num_rules] = (css_rule_s*)malloc(sizeof(css_rule_s));
  if (!css->rule[css->num_rules])
  {
    *error = ERROR_OUT_OF_MEMORY;
    return text + 1;
  }
  memset(css->rule[css->num_rules], 0, sizeof(css_rule_s));

  text = cssParseSelectors(css->rule[css->num_rules], text, error);
  if (*error != ERROR_SUCCESS)
  {
    cssDeleteRule(css->rule[css->num_rules]);
    return (text && *text) ? text+1 : text;
  }
  return cssParseProperties(css->rule[css->num_rules++], text, error);
}

/*
[PUBLIC] Create a new (blank) stylesheet.
*/

css_s *cssCreate(const char *href)
{
  css_s *css = (css_s*)malloc(sizeof(css_s));
  if (!css) { return NULL; }
  memset(css, 0, sizeof(css_s));

  if (href)
  {
    css->href = (char*)malloc(strlen(href) + 1);
    if (!css->href) { return cssDelete(css); }
    strcpy(css->href, href);
  }

  return css;
}

/*
[PUBLIC] Free a previously-created stylesheet from memory.
*/

css_s *cssDelete(css_s *css)
{
  size_t i = 0;

  if (!css) { return NULL; }

  if (css->import)
  {
    for (i = 0; i < css->num_imports; ++i) { cssDeleteImport(css->import[i]); }
    free(css->import);
  }

  if (css->rule)
  {
    for (i = 0; i < css->num_rules; ++i) { cssDeleteRule(css->rule[i]); }
    free(css->rule);
  }

  free(css->href);
  free(css);
  return NULL;
}

/*
[PUBLIC] Create a new stylesheet based on a string of CSS text.
*/

css_s *cssLoadText(const char *text, const char *href)
{
  css_s *css = NULL;

  const char *data = text;
  const char *c;

  if (cssHasComments(data)) { data = cssStripComments(data); }
  if (!data) { return NULL; }

  css = cssCreate(href);
  for (c = data; css && c && *c; )
  {
    error_t error = ERROR_SUCCESS;
    if      (*c == '@'    ) { c = cssParseAtRule(css, c, &error); }
    else if (!cssSpace(*c)) { c = cssParseRule(css, c, &error);   }
    else                    { ++c;                                  }
  }

  if (data != text) { free((void*)data); }
  return css;
}

/*
[PUBLIC] Load a stylesheet from a CSS file.
*/

css_s *cssLoadFile(const char *filename)
{
  FILE *src;
  long length;
  char *text;
  css_s *css;

  if (!filename) { return NULL; }
  src = fopen(filename, "rb");
  if (!src) { return NULL; }

  fseek(src, 0, SEEK_END);
  length = ftell(src);

  text = (char*)malloc(length + 1);
  if (text)
  {
    rewind(src);
    fread(text, 1, length, src);
    text[length] = 0;
  }
  fclose(src);

  /* TODO: The filename should be be prefixed with `file://` as per Firefox. */
  css = cssLoadText(text, filename);
  free(text);
  return css;
}

/*
[PUBLIC] Export a stylesheet structure to a CSS file.
*/

error_t cssSave(css_s *css, const char *filename)
{
  FILE * out;
  size_t i, j, k;

  if (!css || !filename) { return ERROR_INVALID_PARAMETER; }
  out = fopen(filename, "wb");
  if (!out) { return ERROR_FILE_IO; }

  for (i = 0; i < css->num_imports; ++i)
  {
    fprintf(out, "@import \"%s\";\n", css->import[i]->uri);
  }

  for (i = 0; i < css->num_rules; ++i)
  {

    for (j = 0; j < css->rule[i]->num_selectors; ++j)
    {
      if (j > 0) { fprintf(out, ", "); }
      for (k = 0; k < css->rule[i]->selector[j]->num_parts; ++k)
      { fprintf(out, "%s", css->rule[i]->selector[j]->part[k]); }
    }
    fprintf(out, " {\n");

    for (j = 0; j < css->rule[i]->num_properties; ++j)
    {
      fprintf(out, "  %s", css->rule[i]->property[j]->name);
      fprintf(out, ": %s", css->rule[i]->property[j]->value);
      if (css->rule[i]->property[j]->flags & CSS_PROPERTY_IMPORTANT)
      { fprintf(out, " !important"); }
      fprintf(out, ";\n");
    }
    fprintf(out, "}\n");

  }

  fclose(out);
  return ERROR_SUCCESS;
}

/*
[PUBLIC] Add a new rule to a stylesheet.
*/

error_t cssCreateRule(css_s *css, const char *text)
{
  error_t error;

  const char *data = text;
  const char *c;

  /* If the text contains comments, create a copy with the comments removed: */
  if (!data) { return ERROR_INVALID_PARAMETER; }
  if (cssHasComments(data)) { data = cssStripComments(data); }
  if (!data) { return ERROR_OUT_OF_MEMORY; }

  /* Parse the text in the same way as we would parse an actual CSS file: */
  for (c = data; cssSpace(*c); ++c) { }
  c = cssParseRule(css, c, &error);

  /* NOTE: Extra text after a rule is a syntax error in this circumstance... */
  for (; error == ERROR_SUCCESS && *c; ++c)
  {
    if (!cssSpace(*c))
    {
      /* ...the rule WAS created successfully, but this must now be undone: */
      error = ERROR_SYNTAX;
      cssDeleteRuleAtIndex(css, css->num_rules - 1);
    }
  }

  /* Free the comment-free copy (if it exists) and return the result: */
  if (data != text) { free((void*)data); }
  return error;
}

/*
[PUBLIC] Add a new rule to a stylesheet at a specified position.
*/

error_t cssCreateRuleAtIndex(css_s *css, const char *text, size_t i)
{
  error_t error;

  /* The index can be one higher than the highest slot to append to the end: */
  if (!css || i > css->num_rules) { return ERROR_INVALID_PARAMETER; }

  /* Append the new rule to the end as per usual, and move it afterwards: */
  error = cssCreateRule(css, text);
  if (error == ERROR_SUCCESS)
  {
    size_t r = css->num_rules - 1;
    css_rule_s *rule = css->rule[r];
    for (; r > i; --r) { css->rule[r] = css->rule[r - 1]; }
    css->rule[i] = rule;
  }
  return error;
}

/*
[PUBLIC] Remove a rule from a stylesheet.
*/

error_t cssDeleteRuleAtIndex(css_s *css, size_t i)
{
  if (!css || i >= css->num_rules) { return ERROR_INVALID_PARAMETER; }

  cssDeleteRule(css->rule[i]);
  --css->num_rules;
  for (; i < css->num_rules; ++i) { css->rule[i] = css->rule[i + 1]; }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] Find a property within a rule by name.
*/

css_property_s *cssGetProperty(css_rule_s *rule, const char *name)
{
  size_t s, i;

  if (!rule || !name) { return NULL; }

  /* We're only interested in the actual name; ignore spaces on either side: */
  for (                ;          cssSpace(name[0  ]); ++name) { }
  for (s = strlen(name); s > 0 && cssSpace(name[s-1]); --s   ) { }

  /* Compare the stored name with the (stripped of spaces) `name` parameter: */
  for (i = 0; i < rule->num_properties; ++i)
    if (!strncmp(rule->property[i]->name, name, s) && !rule->property[i]->name[s])
      return rule->property[i];
  return NULL;
}

/*
[PUBLIC] Set the value of a property within a rule.
*/

error_t cssSetProperty(css_rule_s *rule, const char *name, const char *value, css_flag_t flags)
{
  css_property_s *property;
  size_t name_len, i;

  if (!rule || !name)
    return ERROR_INVALID_PARAMETER;

  /* Ensure that the name is valid, ignoring any leading/trailing spaces: */
  while (cssSpace(*name))
    ++name;

  if (*name == 0)
    return ERROR_SYNTAX;
  else if (name[0] == '-' && (name[1] == '-' || !cssIdent(name[1])))
    return ERROR_SYNTAX;

  for (name_len = 0; name[name_len] && !cssSpace(name[name_len]); ++name_len)
    if (!cssIdent(name[name_len]))
      return ERROR_SYNTAX;
  for (i = name_len; name[i]; ++i)
    if (!cssSpace(name[i]))
      return ERROR_SYNTAX;

  /* If the value doesn't actually contain anything, treat is as NULL: */
  while (value && cssSpace(*value))
    ++value;
  if (value && *value == 0)
    value = NULL;

  /* If the property doesn't exist, but a value is specified, create it: */
  property = cssGetProperty(rule, name);
  if (!property && value)
  {
    void *tmp = realloc(rule->property, sizeof(css_property_s*) * (rule->num_properties + 1));
    if (!tmp) { return ERROR_OUT_OF_MEMORY; }
    rule->property = (css_property_s**)tmp;

    property = (css_property_s*)malloc(sizeof(css_property_s));
    if (!property) { return ERROR_OUT_OF_MEMORY; }
    memset(property, 0, sizeof(css_property_s));

    property->name = (char*)malloc(name_len + 1);
    if (!property->name) { free(property); return ERROR_OUT_OF_MEMORY; }
    memcpy(property->name, name, name_len);
    property->name[name_len] = 0;

    rule->property[rule->num_properties++] = property;
  }

  /* Attempt to update the value: */
  free(property->value);
  if (value)
    property->value = cssCopyString(value, &value[strlen(value)], 1);
  else
    property->value = NULL;
  property->flags = flags;

  /* If no value was given (or something went wrong), delete the property: */
  if (!property->value)
  {
    /* TODO: Delete */
  }

  /* The property should exist if a value was specified, or be NULL if not: */
  return (!property == !value) ? ERROR_SUCCESS : ERROR_OUT_OF_MEMORY;
}
