/**
@file
@brief Defines various error-related functions and constants.

Copyright (C) 2015-2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "error.h"

/*
[PUBLIC] Get a string representation of a given error condition.
*/

const char *xxxErrorText(const error_t error)
{
  switch (error)
  {
    case ERROR_INVALID_PARAMETER: return "Invalid parameter(s)";
    case ERROR_OUT_OF_MEMORY: return "Out of memory";
    case ERROR_FILE_IO: return "File I/O error";
    case ERROR_SOCKET_IO: return "Socket I/O";
    case ERROR_COULDNT_CONNECT: return "Connection failure";
    case ERROR_UNKNOWN_PROTOCOL: return "Unknown protocol";
    case ERROR_SYNTAX: return "Syntax error";
    case ERROR_SUCCESS: return "";
    default: ;
  }
  return "Unknown error";
}
