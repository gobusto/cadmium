/**
@file
@brief Defines various error-related functions and constants.

Copyright (C) 2015-2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __XXX_ERROR_H__
#define __XXX_ERROR_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates possible error statuses.
*/

typedef enum
{

  ERROR_INVALID_PARAMETER,  /**< @brief Unacceptable function parameter(s).   */
  ERROR_OUT_OF_MEMORY,      /**< @brief Reported if malloc()/realloc() fails. */
  ERROR_FILE_IO,            /**< @brief Something went wrong with a file.     */
  ERROR_SOCKET_IO,          /**< @brief Something went wrong with a socket.   */
  ERROR_COULDNT_CONNECT,    /**< @brief Couldn't connect to a server.         */
  ERROR_UNKNOWN_PROTOCOL,   /**< @brief Unknown protocol (such as, irc://).   */
  ERROR_SYNTAX,             /**< @brief Something wasn't formatted correctly. */
  ERROR_SUCCESS             /**< @brief Everything went OK i.e. not an error. */

} error_t;

/**
@brief Get a string representation of a given error condition.

@param error The enum value we're interested in.
@return A pointer to a C-style string constant describing the value specified.
*/

const char *xxxErrorText(const error_t error);

#ifdef __cplusplus
}
#endif

#endif /* __XXX_ERROR_H__ */
