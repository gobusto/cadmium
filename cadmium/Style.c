/**
@file
@brief All functionality related to visual styling.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <string.h>
#include "Style.h"

/*
[PUBLIC] Determine whether a particular box should be displayed inline or not.
*/

StyleDisplay styleDisplay(const Box *box)
{
  switch (box ? box->kind : BOX_UNKNOWN)
  {
    /* These elements are happy to be put alongside one other: */
    case BOX_TEXT:
    case BOX_ICON:
    case BOX_EMOJI:
    case BOX_LINK:
    case BOX_BOLD:
    case BOX_ITALIC:
    case BOX_CODE_INLINE:
      return STYLE_DISPLAY_INLINE;

    case BOX_ICON_CONTAINER:
    case BOX_GUI_BUTTON:
    case BOX_GUI_ADDRESS_BAR:
    case BOX_GUI_SEARCH_BOX:
    case BOX_GUI_TAB:
    case BOX_GUI_INPUT:
      return STYLE_DISPLAY_INLINE_BLOCK;

    /* These elements should appear on their own line: */
    case BOX_GUI:
    case BOX_GUI_TOOLBAR:
    case BOX_GUI_TAB_BAR:
    case BOX_GUI_CONTENT:
    case BOX_GOPHER_FILE:
    case BOX_GOPHER_MENU:
    case BOX_GOPHER_QUERY:
    case BOX_GOPHER_ERROR:
    case BOX_GOPHER_TELNET:
    case BOX_HEADER1:
    case BOX_HEADER2:
    case BOX_HEADER3:
    case BOX_HEADER4:
    case BOX_HEADER5:
    case BOX_HEADER6:
    case BOX_BULLET_LIST:
    case BOX_NUMBER_LIST:
    case BOX_LIST_ITEM:
    case BOX_QUOTE:
    case BOX_CODE_BLOCK:
    case BOX_HORIZONTAL_RULE:
    case BOX_IMAGE:
    case BOX_UNKNOWN:
    case BOX_PARAGRAPH:
    case BOX_LINEBREAK:
    default:
      return STYLE_DISPLAY_BLOCK;
  }
}

/*
[PUBLIC] Get styling information for a particular box, with no inheritance.
*/

error_t styleDirect(const Box *box, Style *style)
{
  if (!box || !style) { return ERROR_INVALID_PARAMETER; }

  memset(style, 0, sizeof(Style));
  style->color[3] = 255;

  switch (box->kind)
  {
    /* Base settings for the browser toolbars: */
    case BOX_GUI:
      style->font_weight = STYLE_WEIGHT_NORMAL;
      style->font_style = STYLE_SLANT_NORMAL;
      style->text_decoration = STYLE_UNDERLINE_NONE;
      style->font_family = STYLE_FONT_VARIABLE;
      style->font_size = 24;
      style->color[0] = 90;
      style->color[1] = 90;
      style->color[2] = 90;
      style->background[0] = 242;
      style->background[1] = 242;
      style->background[2] = 242;
      style->background[3] = 255;
      style->flags = STYLE_FLAG_FONTSIZE | STYLE_FLAG_FGCOLOUR | STYLE_FLAG_BGCOLOUR;
    break;

    /* Base settings for the actual page content: */
    case BOX_GUI_CONTENT:
      style->font_weight = STYLE_WEIGHT_NORMAL;
      style->font_style = STYLE_SLANT_NORMAL;
      style->text_decoration = STYLE_UNDERLINE_NONE;
      style->font_family = box->text ? STYLE_FONT_VARIABLE : STYLE_FONT_MONOSPACE;
      style->font_size = 14;
      style->background[0] = 255;
      style->background[1] = 255;
      style->background[2] = 255;
      style->background[3] = 255;
      style->padding[STYLE_SIDE_TOP] = 32;
      style->padding[STYLE_SIDE_LEFT] = 32;
      style->padding[STYLE_SIDE_RIGHT] = 32;
      style->padding[STYLE_SIDE_BOTTOM] = 32;
      style->flags = STYLE_FLAG_FONTSIZE | STYLE_FLAG_FGCOLOUR | STYLE_FLAG_BGCOLOUR;
    break;

    /* General text-type boxes: */

    case BOX_HEADER1: if (box->kind == BOX_HEADER1) style->font_size = 32;
    case BOX_HEADER2: if (box->kind == BOX_HEADER2) style->font_size = 24;
    case BOX_HEADER3: if (box->kind == BOX_HEADER3) style->font_size = 18;
    case BOX_HEADER4: if (box->kind == BOX_HEADER4) style->font_size = 16;
    case BOX_HEADER5: if (box->kind == BOX_HEADER5) style->font_size = 13;
    case BOX_HEADER6: if (box->kind == BOX_HEADER6) style->font_size = 10;
      style->flags = STYLE_FLAG_FONTSIZE;
    case BOX_BOLD:
      if (box->kind == BOX_BOLD) { style->font_weight = STYLE_WEIGHT_BOLD; }
    case BOX_ITALIC:
      if (box->kind == BOX_ITALIC) { style->font_style = STYLE_SLANT_ITALIC; }
    case BOX_TEXT:
    case BOX_PARAGRAPH:
    case BOX_LINEBREAK:
      style->mouse = STYLE_MOUSE_IBEAM;
    break;

    case BOX_EMOJI: style->font_family = STYLE_FONT_EMOJI; break;
    case BOX_ICON:  style->font_family = STYLE_FONT_ICON;  break;

    /* GUI boxes: */

    case BOX_GUI_TOOLBAR:
      style->padding[STYLE_SIDE_TOP] = 4;
      style->padding[STYLE_SIDE_LEFT] = 4;
      style->padding[STYLE_SIDE_RIGHT] = 4;
      style->padding[STYLE_SIDE_BOTTOM] = 4;
      style->border[STYLE_SIDE_BOTTOM] = 1;
      style->border_color[0] = 180;
      style->border_color[1] = 180;
      style->border_color[2] = 180;
      style->border_color[3] = 255;
    break;

    case BOX_GUI_BUTTON:
      style->mouse = STYLE_MOUSE_HAND;
      style->min_width = 24;
      style->margin[STYLE_SIDE_LEFT] = 1;
      style->margin[STYLE_SIDE_RIGHT] = 1;
      style->padding[STYLE_SIDE_LEFT] = 4;
      style->padding[STYLE_SIDE_RIGHT] = 4;
      style->padding[STYLE_SIDE_BOTTOM] = 2;
      style->border[STYLE_SIDE_TOP] = 1;
      style->border[STYLE_SIDE_LEFT] = 1;
      style->border[STYLE_SIDE_RIGHT] = 1;
      style->border[STYLE_SIDE_BOTTOM] = 1;
      style->border_color[0] = 180;
      style->border_color[1] = 180;
      style->border_color[2] = 180;
      style->border_color[3] = 255;
    break;

    case BOX_GUI_SEARCH_BOX:
    case BOX_GUI_ADDRESS_BAR:
      style->margin[STYLE_SIDE_LEFT] = 1;
      style->margin[STYLE_SIDE_RIGHT] = 1;

      style->font_family = STYLE_FONT_VARIABLE;
      style->font_size = 20;

      style->padding[STYLE_SIDE_LEFT] = 8;

      style->color[0] = 60;
      style->color[1] = 60;
      style->color[2] = 60;

      style->background[0] = 200;
      style->background[1] = 200;
      style->background[2] = 200;
      style->background[3] = 255;

      style->flags = STYLE_FLAG_FONTSIZE | STYLE_FLAG_FGCOLOUR | STYLE_FLAG_BGCOLOUR;
    break;

    case BOX_GUI_INPUT:
      style->mouse = STYLE_MOUSE_IBEAM;
      style->min_width = 160;

      style->font_family = STYLE_FONT_VARIABLE;
      style->font_size = 14;
      style->background[0] = 255;
      style->background[1] = 255;
      style->background[2] = 255;
      style->background[3] = 255;

      style->border[STYLE_SIDE_TOP] = 1;
      style->border[STYLE_SIDE_LEFT] = 1;
      style->border[STYLE_SIDE_RIGHT] = 1;
      style->border[STYLE_SIDE_BOTTOM] = 1;

      style->padding[STYLE_SIDE_LEFT] = 5;
      style->padding[STYLE_SIDE_RIGHT] = 5;
      style->padding[STYLE_SIDE_TOP] = 3;
      style->padding[STYLE_SIDE_BOTTOM] = 4;

      style->border_color[0] = 180;
      style->border_color[1] = 180;
      style->border_color[2] = 180;
      style->border_color[3] = 255;
      style->flags = STYLE_FLAG_FONTSIZE | STYLE_FLAG_FGCOLOUR | STYLE_FLAG_BGCOLOUR;
    break;

    case BOX_GUI_TAB_BAR:
    break;

    case BOX_GUI_TAB:
    break;

    /* Markdown boxes: */

    case BOX_QUOTE:
      style->mouse = STYLE_MOUSE_IBEAM;
    break;

    case BOX_CODE_INLINE:
    case BOX_CODE_BLOCK:
      style->mouse = STYLE_MOUSE_IBEAM;
      style->font_family = STYLE_FONT_MONOSPACE;
    break;

    case BOX_LINK:
      style->mouse = STYLE_MOUSE_HAND;
      style->text_decoration = STYLE_UNDERLINE_SINGLE;
      style->color[2] = 255;
      style->flags = STYLE_FLAG_FGCOLOUR;
    break;

    case BOX_BULLET_LIST:
    case BOX_NUMBER_LIST:
    case BOX_HORIZONTAL_RULE:
    case BOX_LIST_ITEM:
    break;

    case BOX_IMAGE:
      style->mouse = STYLE_MOUSE_HAND;
      style->margin[STYLE_SIDE_TOP] = 5;
      style->margin[STYLE_SIDE_BOTTOM] = 5;
      style->border[STYLE_SIDE_TOP] = 1;
      style->border[STYLE_SIDE_BOTTOM] = 1;
      style->border[STYLE_SIDE_LEFT] = 1;
      style->border[STYLE_SIDE_RIGHT] = 1;
      style->padding[STYLE_SIDE_BOTTOM] = 6;
      style->padding[STYLE_SIDE_LEFT] = 12;
      style->padding[STYLE_SIDE_RIGHT] = 12;
      style->padding[STYLE_SIDE_TOP] = 6;
      style->border_color[0] = 128;
      style->border_color[1] = 128;
      style->border_color[2] = 128;
      style->border_color[3] = 255;
    break;

    /* Gopher items: */

    case BOX_ICON_CONTAINER:
      style->margin[STYLE_SIDE_TOP] = 2;
      style->margin[STYLE_SIDE_RIGHT] = 8;
    break;

    case BOX_GOPHER_ERROR:
      style->border_top_left_radius = 4;
      style->border_top_right_radius = 4;
      style->border_bottom_left_radius = 4;
      style->border_bottom_right_radius = 4;
      style->color[0] = 169;
      style->color[1] = 68;
      style->color[2] = 66;
      style->background[0] = 242;
      style->background[1] = 222;
      style->background[2] = 222;
      style->background[3] = 255;
      style->border_color[0] = 169;
      style->border_color[1] = 68;
      style->border_color[2] = 66;
      style->border_color[3] = 255;
      style->margin[STYLE_SIDE_TOP] = 5;
      style->margin[STYLE_SIDE_BOTTOM] = 5;
      style->border[STYLE_SIDE_TOP] = 1;
      style->border[STYLE_SIDE_BOTTOM] = 1;
      style->border[STYLE_SIDE_LEFT] = 1;
      style->border[STYLE_SIDE_RIGHT] = 1;
      style->padding[STYLE_SIDE_BOTTOM] = 6;
      style->padding[STYLE_SIDE_LEFT] = 12;
      style->padding[STYLE_SIDE_RIGHT] = 12;
      style->padding[STYLE_SIDE_TOP] = 6;
      style->flags = STYLE_FLAG_FGCOLOUR | STYLE_FLAG_BGCOLOUR;
    break;

    case BOX_GOPHER_FILE:
    case BOX_GOPHER_MENU:
    case BOX_GOPHER_QUERY:
    case BOX_GOPHER_TELNET:
    case BOX_UNKNOWN:
    default:
      if (box->kind == BOX_GOPHER_FILE)
      {
        style->background[0] = 51;
        style->background[1] = 122;
        style->background[2] = 183;
        style->background[3] = 255;
        style->border_color[0] = 46;
        style->border_color[1] = 109;
        style->border_color[2] = 164;
        style->border_color[3] = 255;
      }
      else if (box->kind == BOX_GOPHER_MENU)
      {
        style->background[0] = 68;
        style->background[1] = 157;
        style->background[2] = 68;
        style->background[3] = 255;
        style->border_color[0] = 57;
        style->border_color[1] = 132;
        style->border_color[2] = 57;
        style->border_color[3] = 255;
      }
      else if (box->kind == BOX_GOPHER_QUERY)
      {
        style->background[0] = 49;
        style->background[1] = 176;
        style->background[2] = 213;
        style->background[3] = 255;
        style->border_color[0] = 38;
        style->border_color[1] = 154;
        style->border_color[2] = 188;
        style->border_color[3] = 255;
      }
      else if (box->kind == BOX_GOPHER_TELNET)
      {
        style->background[0] = 240;
        style->background[1] = 173;
        style->background[2] = 78;
        style->background[3] = 255;
        style->border_color[0] = 238;
        style->border_color[1] = 162;
        style->border_color[2] = 54;
        style->border_color[3] = 255;
      }
      else
      {
        style->background[0] = 201;
        style->background[1] = 48;
        style->background[2] = 44;
        style->background[3] = 255;
        style->border_color[0] = 172;
        style->border_color[1] = 41;
        style->border_color[2] = 37;
        style->border_color[3] = 255;
      }
      if (box->kind != BOX_GOPHER_QUERY) { style->mouse = STYLE_MOUSE_HAND; }
      style->border_top_left_radius = 4;
      style->border_top_right_radius = 4;
      style->border_bottom_left_radius = 4;
      style->border_bottom_right_radius = 4;
      style->color[0] = 255;
      style->color[1] = 255;
      style->color[2] = 255;
      style->margin[STYLE_SIDE_TOP] = 5;
      style->margin[STYLE_SIDE_BOTTOM] = 5;
      style->border[STYLE_SIDE_TOP] = 1;
      style->border[STYLE_SIDE_BOTTOM] = 1;
      style->border[STYLE_SIDE_LEFT] = 1;
      style->border[STYLE_SIDE_RIGHT] = 1;
      style->padding[STYLE_SIDE_BOTTOM] = 6;
      style->padding[STYLE_SIDE_LEFT] = 12;
      style->padding[STYLE_SIDE_RIGHT] = 12;
      style->padding[STYLE_SIDE_TOP] = 6;
      style->flags = STYLE_FLAG_FGCOLOUR | STYLE_FLAG_BGCOLOUR;
    break;
  }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] Get styling information for a particular box including inherited values.
*/

error_t styleCalculated(const Box *box, Style *style)
{
  Style direct_style;

  if (!box || !style) { return ERROR_INVALID_PARAMETER; }

  /* If this box has a parent, handle that first. Otherwise, initialise... */
  if (box->parent) { styleCalculated(box->parent, style); }
  else             { memset(style, 0, sizeof(Style));     }

  /* Apply the styles for this box "on top of" what we have so far: */
  styleDirect(box, &direct_style);

  if (direct_style.font_weight != STYLE_WEIGHT_INHERIT)
    style->font_weight = direct_style.font_weight;
  if (direct_style.font_style != STYLE_SLANT_INHERIT)
    style->font_style = direct_style.font_style;
  if (direct_style.text_decoration != STYLE_UNDERLINE_INHERIT)
    style->text_decoration = direct_style.text_decoration;
  if (direct_style.font_family != STYLE_FONT_INHERIT)
    style->font_family = direct_style.font_family;
  if (direct_style.flags & STYLE_FLAG_FONTSIZE)
  {
    style->flags |= STYLE_FLAG_FONTSIZE;
    style->font_size = direct_style.font_size;
  }
  if (direct_style.flags & STYLE_FLAG_FGCOLOUR)
  {
    style->flags |= STYLE_FLAG_FGCOLOUR;
    style->color[0] = direct_style.color[0];
    style->color[1] = direct_style.color[1];
    style->color[2] = direct_style.color[2];
    style->color[3] = direct_style.color[3];
  }
  if (direct_style.flags & STYLE_FLAG_BGCOLOUR)
  {
    style->flags |= STYLE_FLAG_BGCOLOUR;
    style->background[0] = direct_style.background[0];
    style->background[1] = direct_style.background[1];
    style->background[2] = direct_style.background[2];
    style->background[3] = direct_style.background[3];
  }

  return ERROR_SUCCESS;
}
