/**
@file
@brief Essentially describes the contents of a single tab or browser window.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Model.h"

/**
@brief Get a suitable file name from a Gopher URI.

@param uri A Gopher selector, such as `gopher://somesite.org/I/image.jpg`
@return A `malloc()`'d string on success, or `NULL` on failure.
*/

static char *modelFileName(const char *uri)
{
  char *name;
  size_t i;

  /* Ignore anything before the final `/` (or `\`) character: */
  while (uri)
  {
    const char *tmp = strchr(uri, '/');
    if (!tmp) { tmp = strchr(uri, '\\'); }
    if (!tmp) { break; }
    uri = tmp + 1;
  }

  /* If there is no text after the last `/` character, use a "default" name: */
  if (!uri || uri[0] == 0) { uri = "untitled"; }

  /* Copy the name, replacing possibly-problematic characters: */
  name = (char*)malloc(strlen(uri) + 1);
  if (!name) { return NULL; }

  for (i = 0; uri[i]; ++i)
  {
    name[i] = (
      (uri[i] >= 'A' && uri[i] <= 'Z') ||
      (uri[i] >= 'a' && uri[i] <= 'z') ||
      (uri[i] >= '0' && uri[i] <= '9') ||
      uri[i] == '(' || uri[i] == ')' || uri[i] == '-' ||
      uri[i] == '[' || uri[i] == ']' || uri[i] == '.' ||
      uri[i] == ' '
    ) ? uri[i] : '_';
  }
  name[i] = 0;

  /* Return the result. Note that this must be `free()`'d be the caller! */
  return name;
}

/**
@brief about

@param model about
@return writeme
*/

static error_t modelConstrainScrolling(Model *model)
{
  HistoryItem *item;
  int w, h;

  if (modelState(model) != MODEL_FINISHED) { return ERROR_INVALID_PARAMETER; }
  item = historyCurrent(model->history);
  if (!item) { return ERROR_INVALID_PARAMETER; }

  w = 0;
  h = 0;

  if (model->bitmap)
  {
    w = model->bitmap->w;
    h = model->bitmap->h;
  }

  if (model->basic_layout)
  {
    w = model->basic_layout->w;
    h = model->basic_layout->h;
  }

  if (model->fancy_layout)
  {
    if (w < model->fancy_layout->w) w = model->fancy_layout->w;
    if (h < model->fancy_layout->h) h = model->fancy_layout->h;
  }

  if (item->x < 0) item->x = 0;
  if (item->x > w) item->x = w;
  if (item->y < 0) item->y = 0;
  if (item->y > h) item->y = h;

  return ERROR_SUCCESS;
}

/**
@brief about

@param model about
@return writeme
*/

static Model *modelReset(Model *model)
{
  if (!model) { return NULL; }

  sockClose(&model->sock);
  model->sock = INVALID_SOCKET;

  model->data_size = 0;
  free(model->data); model->data = NULL;

  model->gopher = gopherDestroyPage(model->gopher);
  model->bitmap = bmpDestroy(model->bitmap);

  model->basic_layout = boxDestroy(model->basic_layout);
  model->fancy_layout = boxDestroy(model->fancy_layout);

  return model;
}

/*
[PUBLIC] Create a new model structure, in a "default" state, ready to be used.
*/

Model *modelCreate(void)
{
  Model *model = NULL;

  model = (Model*)malloc(sizeof(Model));
  if (!model) { return NULL; }
  memset(model, 0, sizeof(Model));

  model->sock = INVALID_SOCKET;

  model->history = historyCreate();
  if (!model->history) { return modelDestroy(model); }
  return model;
}

/*
[PUBLIC] Destroy a previously-created model structure.
*/

Model *modelDestroy(Model *model)
{
  modelReset(model);
  historyDestroy(model->history);
  free(model);
  return NULL;
}

/*
[PUBLIC] Save a model's data to a file.
*/

error_t modelSave(const Model *model)
{
  char *file_name;
  FILE *out;

  if (modelState(model) != MODEL_FINISHED) { return ERROR_INVALID_PARAMETER; }

  file_name = modelFileName(historyCurrentUrl(model->history));
  if (!file_name) { return ERROR_OUT_OF_MEMORY; }

  out = fopen(file_name, "wb");
  if (!out) { free(file_name); return ERROR_FILE_IO; }

  fwrite(model->data, 1, model->data_size, out);
  fclose(out);

  free(file_name);
  return ERROR_SUCCESS;
}

/*
[PUBLIC] Recalculate the layout of a model.
*/

error_t modelLayout(Model *model, int w, BoxFlags flags)
{
  Box *box;

  if (!model) { return ERROR_INVALID_PARAMETER; }

  /* Don't bother generating a layout unless we NEED to: */
  if (modelState(model) != MODEL_FINISHED)
  {
    fprintf(stderr, "[DEBUG] modelLayout() - Data not fully downloaded!\n");
    return ERROR_SUCCESS;
  }
  else if (model->bitmap)
  {
    fprintf(stderr, "[DEBUG] modelLayout() - It's an image!\n");
    return ERROR_SUCCESS;
  }
  else if (model->encoding == TEXT_ENCODING_UNKNOWN)
  {
    fprintf(stderr, "[DEBUG] modelLayout() - Unknown text encoding!\n");
    return ERROR_SUCCESS;
  }
  fprintf(stderr, "[DEBUG] modelLayout() - OK, looks like we're good to go!\n");

  box = (flags & BOX_FLAG_MARKDOWN) ? model->fancy_layout : model->basic_layout;
  if (box)
  {
    return boxRecalculate(box, model->encoding, 0, 0, w, flags);
  }
  else if (model->gopher)
  {
    /* gopherSavePage(model->gopher, "debug.txt"); */
    fprintf(stderr, "[DEBUG] Building a layout based on a Gopher menu...\n");
    box = boxFromGopher(model->gopher, model->encoding, w, flags);
  }
  else
  {
    fprintf(stderr, "[DEBUG] Building a layout based on plain text...\n");
    box = boxFromText((const char*)model->data, model->encoding, w, flags);
  }
  (flags & BOX_FLAG_MARKDOWN) ? (model->fancy_layout = box) : (model->basic_layout = box);

  fprintf(stderr, "[DEBUG] %s\n", box ? "Done!" : "Failed!");
  return box ? ERROR_SUCCESS : ERROR_SYNTAX;
}

/**
@brief Handle a completed request by doing something useful with the data.

@param model The model we're interested in.
*/

static void modelHandleResponse(Model *model)
{
  const char *text;

  if (!model || !model->data) { return; }

  text = (const char*)model->data;
  model->encoding = textEncoding(text);

  fprintf(stderr, "[DEBUG] Text encoding is ");
  switch (model->encoding)
  {
    case TEXT_ENCODING_UTF8:   fprintf(stderr, "UTF-8"); break;
    case TEXT_ENCODING_LATIN1: fprintf(stderr, "Latin-1"); break;
    case TEXT_ENCODING_UNKNOWN: default: fprintf(stderr, "unknown"); break;
  }
  fprintf(stderr, "\n");

  if (model->encoding != TEXT_ENCODING_UNKNOWN)
  {
    fprintf(stderr, "[DEBUG] Is this a gopher menu?\n");
    model->gopher = gopherCreatePage(text);
    fprintf(stderr, "[DEBUG] %s\n", model->gopher ? "Yep!" : "Nope...");
  }

  if (!model->gopher)
  {
    fprintf(stderr, "[DEBUG] Is this an image file?\n");
    model->bitmap = bmpLoadData(model->data_size, model->data);
    fprintf(stderr, "[DEBUG] %s\n", model->bitmap ? "Yep!" : "Nope...");
  }
}

/**
@brief Load a local file as though it were a remote request.

@todo Properly handle `%20` et al. when attempting to open the file.

@param model The model used to handle the request.
@param uri A `file://` path.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

static error_t modelLoadFile(Model *model, const char *uri)
{
  FILE *fsrc;

  if (!model || !uri) { return ERROR_INVALID_PARAMETER; }
  fsrc = fopen(uri + 7, "rb");
  if (!fsrc) { return ERROR_FILE_IO; }

  fseek(fsrc, 0, SEEK_END);
  model->data_size = ftell(fsrc);
  rewind(fsrc);

  model->data = (unsigned char*)malloc(model->data_size + 1);
  if (model->data)
  {
    fread(model->data, 1, model->data_size, fsrc);
    model->data[model->data_size] = 0;
  }
  fclose(fsrc);

  if (!model->data) { return ERROR_OUT_OF_MEMORY; }
  modelHandleResponse(model);
  return ERROR_SUCCESS;
}

/**
@brief Send a HTTP request to a remote server.

See <https://www.ietf.org/rfc/rfc4266.txt>

@param model The model used to handle the request.
@param uri A `gopher://` path.
@param method Should be `GET`, `POST`, `PUT` etc.
@param flags These affect how the request is made.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

static error_t modelLoadGopher(Model *model, const char *uri)
{
  const char *default_port = "70";

  error_t error = ERROR_SUCCESS;
  int code = GOPHER_DIRECTORY;
  char *host;
  char *port;

  char *request;

  size_t i;

  /* This should never happen, but Just In Case(tm)... */
  if (!model) { return ERROR_INVALID_PARAMETER; }

  uri += 9;

  /* Get the base URL, such as "www.example.com". */
  for (i = 0; uri[i] != ':' && uri[i] != '/' && uri[i] != 0 &&
              uri[i] != '?' && uri[i] != '#'; ++i) { }
  if (!(host = (char*)malloc(i+1))) { return ERROR_OUT_OF_MEMORY; }
  memcpy(host, uri, i); host[i] = 0;
  uri = &uri[i];

  /* Treat "localhost" as "127.0.0.1": */
  if (strcmp(host, "localhost") == 0) { strcpy(host, "127.0.0.1"); }

  /* Get the port, if it is specified. */
  if (uri[0] == ':')
  {
    ++uri;
    for (i = 0; uri[i] >= '0' && uri[i] <= '9'; ++i) { }
    if (!(port = (char*)malloc(i+1))) { free(host); return ERROR_OUT_OF_MEMORY; }
    memcpy(port, uri, i); port[i] = 0;
    uri = &uri[i];
  }
  else { port = NULL; }

  /* The port may be followed by a "/" character: */
  if (uri[0] == '/')
  {
    ++uri;
    /* The first character after the "/" is the expected Gopher result type: */
    if (uri[0]) { code = uri[0]; ++uri; }
  }
  else if (uri[0]) { free(host); free(port); return ERROR_INVALID_PARAMETER; }

  fprintf(stderr, "[DEBUG] Gopher request host is '%s'\n", host);
  fprintf(stderr, "[DEBUG] Gopher request port is '%s'\n", port ? port : default_port);
  fprintf(stderr, "[DEBUG] Gopher request path is '%s'\n", uri);
  fprintf(stderr, "[DEBUG] Gopher request type is '%c'\n", code);

  i = strlen(uri) + 3;
  if ((request = (char*)malloc(i)))
  {
    sprintf(request, "%s\r\n", uri);

    model->sock = sockConnect(host, port ? port : default_port);

    if (sockValid(model->sock))
    {
      if (sockSend(model->sock, request) != 0)
      {
        sockClose(&model->sock);
        error = ERROR_SOCKET_IO;
      }
    }
    else { error = ERROR_COULDNT_CONNECT; }

    free(request);

  } else { error = ERROR_OUT_OF_MEMORY; }

  free(host);
  free(port);

  return error;
}

/*
[PUBLIC] Start a new `gopher://`, `file://`, or other request.
*/

error_t modelRequest(Model *model, const char *uri)
{
  error_t error;

  if (!model || !uri) { return ERROR_INVALID_PARAMETER; }

  error = historyAdd(model->history, uri);
  return error == ERROR_SUCCESS ? modelReload(model) : error;
}

/*
[PUBLIC] Start a new `gopher://`, `file://`, or other request.
*/

error_t modelReload(Model *model)
{
  const char *uri;

  if (!model) { return ERROR_INVALID_PARAMETER; }
  uri = historyCurrentUrl(model->history);
  if (!uri) { return ERROR_INVALID_PARAMETER; }

  modelReset(model);
  if      (!strncmp(uri, "file://",   7)) { return modelLoadFile(model, uri);   }
  else if (!strncmp(uri, "gopher://", 9)) { return modelLoadGopher(model, uri); }
  return ERROR_UNKNOWN_PROTOCOL;
}

/*
[PUBLIC] about
*/

error_t modelBack(Model *model)
{
  error_t error;

  if (!model) { return ERROR_INVALID_PARAMETER; }
  if (!historyCanGoBack(model->history)) { return ERROR_SUCCESS; }

  error = historyBack(model->history);
  return error == ERROR_SUCCESS ? modelReload(model) : error;
}

/*
[PUBLIC] about
*/

error_t modelForward(Model *model)
{
  error_t error;

  if (!model) { return ERROR_INVALID_PARAMETER; }
  if (!historyCanGoForward(model->history)) { return ERROR_SUCCESS; }

  error = historyForward(model->history);
  return error == ERROR_SUCCESS ? modelReload(model) : error;
}

/**
@brief Update a model (i.e. download more data from an open connection).

@param model The model to be updated.
@return A suitable "state" value, describing what the model is actually doing.
*/

static ModelState modelReceive(Model *model)
{
  char buffer[64];
  int buffer_len = 0;
  void *tmp = NULL;

  if (!model) { return MODEL_INVALID; }
  buffer_len = recv(model->sock, buffer, 64, 0);
  if (buffer_len < 1) { return MODEL_FINISHED; }

  tmp = realloc(model->data, model->data_size + buffer_len + 1);
  if (!tmp) { return MODEL_OUTOFMEMORY; }
  model->data = (unsigned char*)tmp;

  memcpy(&model->data[model->data_size], buffer, buffer_len);
  model->data_size += buffer_len;
  model->data[model->data_size] = 0;

  return MODEL_RECEIVING;
}

/*
[PUBLIC] Update a model (i.e. download more data from an open connection).
*/

ModelState modelUpdate(Model *model)
{
  ModelState status;

  if (!model) { return MODEL_INVALID; }
  else if (!sockValid(model->sock)) { return MODEL_IDLE; }

  status = modelReceive(model);
  if (status == MODEL_RECEIVING) { return MODEL_RECEIVING; }
  sockClose(&model->sock);

  modelHandleResponse(model);
  return status;
}

/*
[PUBLIC] about
*/

ModelState modelState(const Model *model)
{
  if (!model)
    return MODEL_INVALID;
  else if (sockValid(model->sock))
    return MODEL_RECEIVING;
  else if (model->data)
    return MODEL_FINISHED;
  return MODEL_IDLE;
}

/*
[PUBLIC] about
*/

error_t modelGetScroll(const Model *model, int *x, int *y)
{
  const HistoryItem *item;

  if (!model) { return ERROR_INVALID_PARAMETER; }

  item = historyCurrent(model->history);
  if (x) { *x = item ? item->x : 0; }
  if (y) { *y = item ? item->y : 0; }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] about
*/

error_t modelScrollBy(Model *model, int x, int y)
{
  HistoryItem *item;

  if (modelState(model) != MODEL_FINISHED) { return ERROR_INVALID_PARAMETER; }
  item = historyCurrent(model->history);
  if (!item) { return ERROR_INVALID_PARAMETER; }

  item->x += x;
  item->y += y;

  return modelConstrainScrolling(model);
}
