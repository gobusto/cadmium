/**
@file
@brief All functionality related to layout calculations.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Box.h"
#include "Style.h"
#include "../Render.h"  /* TODO: Don't rely on anything outside of `cadmium/` */
#include "encoding/unicode.h"

/* FontAwesome icon codepoints, encoded as UTF-8: */

#define BOX_ICON_ERROR        "\uF071"  /**< @brief about */
#define BOX_ICON_UNKNOWN      "\uF059"  /**< @brief about */
#define BOX_ICON_PHONEBOOK    "\uF095"  /**< @brief about */
#define BOX_ICON_TELNET       "\uF120"  /**< @brief about */
#define BOX_ICON_SEARCH       "\uF002"  /**< @brief about */
#define BOX_ICON_SERVER       "\uF1C0"  /**< @brief about */
#define BOX_ICON_FOLDER       "\uF115"  /**< @brief about */
#define BOX_ICON_FILE         "\uF016"  /**< @brief about */
#define BOX_ICON_ARCHIVE_FILE "\uF1C6"  /**< @brief about */
#define BOX_ICON_PDF_FILE     "\uF1C1"  /**< @brief about */
#define BOX_ICON_TEXT_FILE    "\uF0F6"  /**< @brief about */
#define BOX_ICON_CODE_FILE    "\uF1C9"  /**< @brief about */
#define BOX_ICON_IMAGE_FILE   "\uF1C5"  /**< @brief about */
#define BOX_ICON_AUDIO_FILE   "\uF1C7"  /**< @brief about */
#define BOX_ICON_VIDEO_FILE   "\uF1C8"  /**< @brief about */
#define BOX_ICON_EMAIL_FILE   "\uF003"  /**< @brief about */

#define BOX_ICON_URL          "\uF0AC"  /**< @brief about */
#define BOX_ICON_BACK         "\uF060"  /**< @brief about */
#define BOX_ICON_FORWARD      "\uF061"  /**< @brief about */
#define BOX_ICON_HOME         "\uF015"  /**< @brief about */
#define BOX_ICON_STOP         "\uF057"  /**< @brief about */
#define BOX_ICON_RELOAD       "\uF021"  /**< @brief about */
#define BOX_ICON_MARKDOWN     "\uF031"  /**< @brief about */
#define BOX_ICON_NEWLINES     "\uF149"  /**< @brief about */

/*
[PUBLIC] about
*/

const char *boxTypeText(const Box *box)
{
  if (!box) { return "(null)"; }

  switch (box->kind)
  {
    case BOX_TEXT:            return "BOX_TEXT";
    case BOX_EMOJI:           return "BOX_EMOJI";
    case BOX_ICON:            return "BOX_ICON";
    case BOX_LINEBREAK:       return "BOX_LINEBREAK";
    case BOX_PARAGRAPH:       return "BOX_PARAGRAPH";
    case BOX_IMAGE:           return "BOX_IMAGE";

    case BOX_GOPHER_FILE:     return "BOX_GOPHER_FILE";
    case BOX_GOPHER_MENU:     return "BOX_GOPHER_MENU";
    case BOX_GOPHER_QUERY:    return "BOX_GOPHER_QUERY";
    case BOX_GOPHER_ERROR:    return "BOX_GOPHER_ERROR";
    case BOX_GOPHER_TELNET:   return "BOX_GOPHER_TELNET";

    case BOX_HEADER1:         return "BOX_HEADER1";
    case BOX_HEADER2:         return "BOX_HEADER2";
    case BOX_HEADER3:         return "BOX_HEADER3";
    case BOX_HEADER4:         return "BOX_HEADER4";
    case BOX_HEADER5:         return "BOX_HEADER5";
    case BOX_HEADER6:         return "BOX_HEADER6";
    case BOX_BULLET_LIST:     return "BOX_BULLET_LIST";
    case BOX_NUMBER_LIST:     return "BOX_NUMBER_LIST";
    case BOX_LIST_ITEM:       return "BOX_LIST_ITEM";
    case BOX_LINK:            return "BOX_LINK";
    case BOX_QUOTE:           return "BOX_QUOTE";
    case BOX_CODE_BLOCK:      return "BOX_CODE_BLOCK";
    case BOX_CODE_INLINE:     return "BOX_CODE_INLINE";
    case BOX_ITALIC:          return "BOX_ITALIC";
    case BOX_BOLD:            return "BOX_BOLD";
    case BOX_HORIZONTAL_RULE: return "BOX_HORIZONTAL_RULE";

    case BOX_GUI:             return "BOX_GUI";
    case BOX_GUI_CONTENT:     return "BOX_GUI_CONTENT";
    case BOX_GUI_TOOLBAR:     return "BOX_GUI_TOOLBAR";
    case BOX_GUI_BUTTON:      return "BOX_GUI_BUTTON";
    case BOX_GUI_ADDRESS_BAR: return "BOX_GUI_ADDRESS_BAR";
    case BOX_GUI_SEARCH_BOX:  return "BOX_GUI_SEARCH_BOX";
    case BOX_GUI_TAB_BAR:     return "BOX_GUI_TAB_BAR";
    case BOX_GUI_TAB:         return "BOX_GUI_TAB";
    case BOX_GUI_INPUT:       return "BOX_GUI_INPUT";

    case BOX_ICON_CONTAINER:  return "BOX_ICON_CONTAINER";
    case BOX_UNKNOWN:         return "BOX_UNKNOWN";
    default:                  return "???";
  }
}

/**
@brief about

@param box about
@param indent about
@return writeme
*/

static void boxDebugDump(const Box *box, unsigned long indent)
{
  unsigned long i;
  if (!box) { return; }

  for (i = 0; i < indent; ++i) { fputc('\t', stderr); }

  fprintf(stderr, "%s (%d, %d, %d, %d) - %s\n",
    boxTypeText(box), box->x, box->y, box->w, box->h,
    box->text ? box->text : "<blank>"
  );

  for (i = 0; i < box->num_boxes; ++i) { boxDebugDump(box->box[i], indent + 1); }
}

/**
@brief about

This function does NOT count line breaks (`\r` or `\n`) as whitespace!

@todo Handle UTF-8 space characters too...?

@param text about
@param encoding about
@return The number of bytes used by a character, or 0 if not whitespace.
*/

static int boxWhiteSpace(const char *text, TextEncoding encoding)
{
  if (!text || encoding == TEXT_ENCODING_UNKNOWN) { return 0; }
  return *text == ' ' || *text == '\t';
}

/**
@brief about

@param text about
@param encoding about
@return The number of bytes used by a character, or 0 if not emoji.
*/

static int boxEmoji(const char *text, TextEncoding encoding)
{
  long value;
  int num_bytes;

  if (!text || encoding != TEXT_ENCODING_UTF8) { return 0; }
  num_bytes = utfDecodeUTF8(text, &value);
  return
    (value == 0x02139) || /* Letterlike Symbols -- just "Information Source" */
    (value >= 0x02190 && value <= 0x021FF) || /* Arrows                      */
    (value >= 0x02300 && value <= 0x023FF) || /* Miscellaneous Technical     */
    (value >= 0x025A0 && value <= 0x025FF) || /* Geometric Shapes            */
    (value >= 0x02600 && value <= 0x026FF) || /* Miscellaneous Symbols       */
    (value >= 0x02700 && value <= 0x027BF) || /* Dingbats                    */
    (value >= 0x027F0 && value <= 0x027FF) || /* Supplemental Arrows-A       */
    (value >= 0x02900 && value <= 0x0297F) || /* Supplemental Arrows-B       */
    (value >= 0x02B00 && value <= 0x02BFF) || /* Misc. Symbols & Arrows      */
    (value >= 0x1F000 && value <= 0x1F02F) || /* Mahjong Tiles               */
    (value >= 0x1F030 && value <= 0x1F09F) || /* Domino Tiles                */
    (value >= 0x1F0A0 && value <= 0x1F0FF) || /* Playing Cards               */
    (value >= 0x1F100 && value <= 0x1F1FF) || /* Enclosed Alphanumeric Supp. */
    (value >= 0x1F200 && value <= 0x1F2FF) || /* Enclosed Ideographic Supp.  */
    (value >= 0x1F300 && value <= 0x1F5FF) || /* Misc. Symbols & Pictographs */
    (value >= 0x1F600 && value <= 0x1F64F) || /* Emoticons                   */
    (value >= 0x1F680 && value <= 0x1F6FF) || /* Transport and Map Symbols   */
    (value >= 0x1F700 && value <= 0x1F77F) || /* Alchemical Symbols          */
    (value >= 0x1F780 && value <= 0x1F7FF) || /* Geometric Shapes Extended   */
    (value >= 0x1F800 && value <= 0x1F8FF) || /* Supplemental Arrows-C       */
    (value >= 0x1F900 && value <= 0x1F9FF)    /* Supp. Symbols & Pictographs */
  ? num_bytes : 0;
}

/**
@brief about

@param text about
@param encoding about
@return writeme
*/

#define boxText(t,e) (!boxWhiteSpace(t,e) && !boxEmoji(t,e) && (t)[0] != '\n' && (t)[0] != '\r')

/**
@brief about

@param box about
@param flags about
@return writeme
*/

static StyleDisplay boxStyleDisplay(const Box *box, BoxFlags flags)
{
  if (!box)
    return STYLE_DISPLAY_INLINE;
  if (box->kind == BOX_LINEBREAK && !(flags & BOX_FLAG_NEWLINES))
    return STYLE_DISPLAY_INLINE;
  return styleDisplay(box);
}

/**
@brief about

@param box about
@return writeme
*/

static error_t boxLineBreakOrParagraph(Box *box)
{
  Box *previous;

  if (!box) { return ERROR_INVALID_PARAMETER; }

  /* If there were no previous boxes, just ignore this line break... */
  if (box->num_boxes < 1) { return ERROR_SUCCESS; }
  previous = box->box[box->num_boxes - 1];

  if (previous->kind == BOX_LINEBREAK)
  {
    /* Instead of adding two line-breaks in a row, use a paragraph separator: */
    previous->kind = BOX_PARAGRAPH;
    return ERROR_SUCCESS;
  }
  else if (styleDisplay(previous) == STYLE_DISPLAY_BLOCK)
  {
    /* A single blank line after a block-level element is a new paragraph: */
    if (!boxCreate(box, BOX_PARAGRAPH, NULL, 0))
      return ERROR_OUT_OF_MEMORY;
    return ERROR_SUCCESS;
  }

  /* Otherwise, add a new line-break character: */
  if (!boxCreate(box, BOX_LINEBREAK, NULL, 0))
    return ERROR_OUT_OF_MEMORY;
  return ERROR_SUCCESS;
}

/**
@brief about

VERY UNFINISHED! Doesn't handle Markdown yet.

@param container about
@param text about
@param encoding about
@param flags
@return writeme
*/

static error_t boxParseLine(Box *container, const char *text, TextEncoding encoding, BoxFlags flags)
{
  if (!container) { return ERROR_INVALID_PARAMETER; }
  if (!text) { return ERROR_SUCCESS; }

  while (*text)
  {
    const char *text_end = text;
    size_t len = 0;
    int num_bytes;

    /* Leading whitespace: */
    while (*text_end && boxWhiteSpace(text_end, encoding)) { ++text_end; ++len; }

    num_bytes = boxEmoji(text_end, encoding);
    if (num_bytes > 0)
    {
      if (!boxCreate(container, BOX_EMOJI, text_end, num_bytes))
        return ERROR_OUT_OF_MEMORY;
      text_end += num_bytes;
    }
    else
    {
      switch (*text_end)
      {
        case 0:
        break;

        case '\n':
        case '\r':
          if (boxLineBreakOrParagraph(container) != ERROR_SUCCESS)
            return ERROR_OUT_OF_MEMORY;
          text_end += (text_end[0] == '\r' && text_end[1] == '\n') ? 2 : 1;
        break;

        default:
          while (*text_end && boxText(text_end, encoding)) { ++text_end; ++len; }
          while (*text_end && boxWhiteSpace(text_end, encoding)) { ++text_end; ++len; }

          if (!boxCreate(container, BOX_TEXT, text, len))
            return ERROR_OUT_OF_MEMORY;
        break;
      }
    }

    text = text_end;
  }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] Create a new box structure.
*/

Box *boxCreate(Box *parent, BoxType kind, const char *text, size_t len)
{
  Box *box;

  if (parent)
  {
    void *tmp = realloc(parent->box, sizeof(Box*) * (parent->num_boxes + 1));
    if (!tmp) { return NULL; }
    parent->box = (Box**)tmp;
  }

  box = (Box*)malloc(sizeof(Box));
  if (!box) { return NULL; }
  memset(box, 0, sizeof(Box));

  box->kind = kind;

  if (text)
  {
    box->text = (char*)malloc(len + 1);
    if (!box->text) { return boxDestroy(box); }
    strncpy(box->text, text, len);
    box->text[len] = 0;
  }

  if (parent) { parent->box[parent->num_boxes++] = box; }

  return box;
}

/*
[PUBLIC] Destroy a box, along with any child boxes that it contains.
*/

Box *boxDestroy(Box *box)
{
  if (!box) { return NULL; }

  if (box->box)
  {
    unsigned long i;
    for (i = 0; i < box->num_boxes; ++i) { boxDestroy(box->box[i]); }
    free(box->box);
  }

  free(box->text);
  free(box);
  return NULL;
}

/**
@brief Create a new container box, with an icon.

@param parent The box that contains this box, or `NULL` if none.
@param kind about
@param icon The icon to use
@param data The text value of the container box (usually a `gopher://` URL).
@return A new Box structure on success, or `NULL` on failure.
*/

static Box *boxCreateWithIcon(Box *parent, BoxType kind, const char *icon, const char *data)
{
  Box *box;
  Box *icon_container;

  if (!parent || !icon)
    return NULL;

  box = boxCreate(parent, kind, data, data ? strlen(data) : 0);
  if (!box)
    return NULL;

  if (!icon)
    return box;

  if (kind == BOX_GUI_BUTTON)
    icon_container = box; /* No `BOX_ICON_CONTAINER` wrapper for the icon. */
  else
    icon_container = boxCreate(box, BOX_ICON_CONTAINER, NULL, 0);

  if (!icon_container)
    return boxDestroy(box);

  if (!boxCreate(icon_container, BOX_ICON, icon, strlen(icon)))
    return boxDestroy(box);

  return box;
}

/*
[PUBLIC] Build a box hierarchy for the GUI.
*/

Box *boxFromToolbar(int page_width)
{
  const char *search_uri = "gopher://gopher.floodgap.com:70/7/v2/vs";

  Box *container;
  Box *toolbar;
  Box *item;

  container = boxCreate(NULL, BOX_GUI, NULL, 0);
  if (!container) { return NULL; }

  toolbar = boxCreate(container, BOX_GUI_TOOLBAR, NULL, 0);
  if (!toolbar)
    return boxDestroy(container);
  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_BACK, "BACK"))
    return boxDestroy(container);
  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_FORWARD, "FORWARD"))
    return boxDestroy(container);
  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_RELOAD, "RELOAD"))
    return boxDestroy(container);
  /*
  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_STOP, "STOP"))
    return boxDestroy(container);
  */
  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_HOME, "HOME"))
    return boxDestroy(container);

  item = boxCreateWithIcon(toolbar, BOX_GUI_ADDRESS_BAR, BOX_ICON_URL, NULL);
  if (!item)
    return boxDestroy(container);
  if (!boxCreate(item, BOX_GUI_INPUT, NULL, 0))
    return boxDestroy(container);

  item = boxCreateWithIcon(toolbar, BOX_GUI_SEARCH_BOX, BOX_ICON_SEARCH, search_uri);
  if (!item)
    return boxDestroy(container);
  if (!boxCreate(item, BOX_GUI_INPUT, NULL, 0))
    return boxDestroy(container);

  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_MARKDOWN, "MARKDOWN"))
    return boxDestroy(container);
  if (!boxCreateWithIcon(toolbar, BOX_GUI_BUTTON, BOX_ICON_NEWLINES, "NEWLINES"))
    return boxDestroy(container);

  toolbar = boxCreate(container, BOX_GUI_TAB_BAR, NULL, 0);
  if (!toolbar)
    return boxDestroy(container);

  if (boxRecalculate(container, TEXT_ENCODING_UTF8, 0, 0, page_width, BOX_FLAG_FONTSIZE) != ERROR_SUCCESS)
    return boxDestroy(container);
  /* boxDebugDump(container, 0); */
  return container;
}

/*
[PUBLIC] Build a box hierarchy from a Gopher menu.
*/

Box *boxFromGopher(const GopherPage *page, TextEncoding encoding, int w, BoxFlags flags)
{
  Box *container;
  unsigned long i;

  if (!page) { return NULL; }
  if (flags & BOX_FLAG_MARKDOWN)
    container = boxCreate(NULL, BOX_GUI_CONTENT, "Markdown", 8);
  else
    container = boxCreate(NULL, BOX_GUI_CONTENT, NULL, 0);
  if (!container) { return NULL; }

  for (i = 0; i < page->num_items; ++i)
  {
    const GopherItem *item = page->item[i];
    Box *box = NULL;
    const char *icon = NULL;

    switch (item->code)
    {
      case GOPHER_INFO:
        if (boxParseLine(container, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
        if (boxLineBreakOrParagraph(container) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;

      case GOPHER_PHONEBOOK:
        if (!icon) { icon = BOX_ICON_PHONEBOOK; }
      case GOPHER_TELNET:
      case GOPHER_TN3270:
        if (!icon) { icon = BOX_ICON_TELNET; }
        box = boxCreateWithIcon(container, BOX_GOPHER_TELNET, icon, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;

      case GOPHER_SERVER:
        if (!icon) { icon = BOX_ICON_SERVER; }
      case GOPHER_DIRECTORY:
        if (!icon) { icon = BOX_ICON_FOLDER; }
        box = boxCreateWithIcon(container, BOX_GOPHER_MENU, icon, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;

      case GOPHER_PDF:
      case GOPHER_PDF_2:
        if (!icon) { icon = BOX_ICON_PDF_FILE; }
      case GOPHER_BINHEX:
      case GOPHER_UUENCODE:
      case GOPHER_BIN_FILE:
        if (!icon) { icon = BOX_ICON_FILE; }
      case GOPHER_ARCHIVE:
        if (!icon) { icon = BOX_ICON_ARCHIVE_FILE; }
      case GOPHER_TEXT_FILE:
        if (!icon) { icon = BOX_ICON_TEXT_FILE; }
      case GOPHER_CSS:
      case GOPHER_XML:
      case GOPHER_HTML:
        if (!icon) { icon = BOX_ICON_CODE_FILE; }
      case GOPHER_SOUND:
        if (!icon) { icon = BOX_ICON_AUDIO_FILE; }
      case GOPHER_VIDEO:
      case GOPHER_MP4:
        if (!icon) { icon = BOX_ICON_VIDEO_FILE; }
      case GOPHER_EMAIL:
        if (!icon) { icon = BOX_ICON_EMAIL_FILE; }

        box = boxCreateWithIcon(container, BOX_GOPHER_FILE, icon, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;

      case GOPHER_PNG:
      case GOPHER_GIF:
      case GOPHER_IMAGE:
        box = boxCreateWithIcon(container, BOX_IMAGE, BOX_ICON_IMAGE_FILE, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;

      case GOPHER_SEARCH:
        box = boxCreateWithIcon(container, BOX_GOPHER_QUERY, BOX_ICON_SEARCH, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
        if (!boxCreate(box, BOX_PARAGRAPH, NULL, 0))
          return boxDestroy(container);
        if (!boxCreate(box, BOX_GUI_INPUT, NULL, 0))
          return boxDestroy(container);
      break;

      case GOPHER_ERROR:
        box = boxCreateWithIcon(container, BOX_GOPHER_ERROR, BOX_ICON_ERROR, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;

      default:
        box = boxCreateWithIcon(container, BOX_UNKNOWN, BOX_ICON_UNKNOWN, item->uri);
        if (!box)
          return boxDestroy(container);
        if (boxParseLine(box, item->text, encoding, flags) != ERROR_SUCCESS)
          return boxDestroy(container);
      break;
    }
  }

  if (boxRecalculate(container, encoding, 0, 0, w, flags | BOX_FLAG_FONTSIZE) != ERROR_SUCCESS)
    return boxDestroy(container);
  /* boxDebugDump(container, 0); */
  return container;
}

/*
[PUBLIC] Build a box hierarchy from a string of plain (or Markdown) text.
*/

Box *boxFromText(const char *text, TextEncoding encoding, int w, BoxFlags flags)
{
  Box *container;

  if (!text) { return NULL; }
  if (flags & BOX_FLAG_MARKDOWN)
    container = boxCreate(NULL, BOX_GUI_CONTENT, "Markdown", 8);
  else
    container = boxCreate(NULL, BOX_GUI_CONTENT, NULL, 0);
  if (!container) { return NULL; }

  if (boxParseLine(container, text, encoding, flags) != ERROR_SUCCESS)
    return boxDestroy(container);

  if (boxRecalculate(container, encoding, 0, 0, w, flags | BOX_FLAG_FONTSIZE) != ERROR_SUCCESS)
    return boxDestroy(container);
  boxDebugDump(container, 0);
  return container;
}

/**
@brief about

@param box about
@return writeme
*/

static int boxTreatAsText(const Box *box)
{
  return box ?
    box->kind == BOX_GUI_INPUT ||
    box->kind == BOX_ICON ||
    box->kind == BOX_TEXT ||
    box->kind == BOX_EMOJI ||
    box->kind == BOX_PARAGRAPH ||
    box->kind == BOX_LINEBREAK : 0;
}

/**
@brief about

@param box about
@param w about
@return writeme
*/

static error_t boxExpand(Box *box, int w)
{
  unsigned long i;
  int extra_space;
  int reposition;
  int address_bar_y;

  if (!box) { return ERROR_INVALID_PARAMETER; }

  /* Only very specific box types should expand-to-fit: */
  if (box->kind != BOX_GUI_TOOLBAR) { return ERROR_SUCCESS; }

  /* Expand this box to take up any extra horizontal space: */
  extra_space = w - box->w;
  if (extra_space <= 0) { return ERROR_SUCCESS; }
  box->w = w;

  /* Expand the address bar, and reposition anything after it: */
  for (reposition = 0, i = 0; i < box->num_boxes; ++i)
  {
    Box *child = box->box[i];

    if (child->kind == BOX_GUI_ADDRESS_BAR)
    {
      unsigned long j;

      /* Expand the address bar itself... */
      child->w += extra_space;

      /* ...and the input box it contains: */
      for (j = 0; j < child->num_boxes; ++j)
      {
        Box *input = child->box[j];
        if (input->kind == BOX_GUI_INPUT) { input->w += extra_space; }
      }

      /* Any boxes on the same line after the address bar need moving right: */
      address_bar_y = child->y;
      reposition = 1;
    }
    else if (reposition && child->y == address_bar_y)
    {
      /* This element is on the same line as the address bar; move it right: */
      child->x += extra_space;
    }
  }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] Recalculate the positions of a box (and any child boxes) on the page.
*/

error_t boxRecalculate(Box *box, TextEncoding encoding, int x, int y, int w, BoxFlags flags)
{
  const StyleDisplay display = boxStyleDisplay(box, flags);
  StyleDisplay previous_display = STYLE_DISPLAY_BLOCK;
  Style style;
  unsigned long i;

  if (!box) { return ERROR_INVALID_PARAMETER; }

  styleDirect(box, &style);

  /* Work out the top-left corner for this box, relative to the parent box: */
  if (!box->parent)
  {
    box->x = x;
    box->y = y;
  }
  else
  {
    box->x = 0;
    box->y = 0;
    for (i = 0; i < box->parent->num_boxes; ++i)
    {
      /* Look at all PRECEDING boxes, to figure out where this one should go: */
      const Box *other = box->parent->box[i];
      if (other == box) { break; }

      previous_display = boxStyleDisplay(other, flags);
      if (display != STYLE_DISPLAY_BLOCK && previous_display != STYLE_DISPLAY_BLOCK)
      {
        /* This element should appear next to the previous one: */
        if (box->y < other->y)
        {
          /* The other box is on a new line; move this box down, next to it. */
          box->x = other->x + other->w;
          box->y = other->y;
        }
        else if (box->x < other->x + other->w)
        {
          /* The other box is on this line; put this box to the right of it. */
          box->x = other->x + other->w;
        }
      }
      else
      {
        /* Either this is a block, or the previous box was; start a new line: */
        box->x = -box->parent->x;
        if (box->y < other->y + other->h)
            box->y = other->y + other->h;
      }
    }
  }

  /* Work out the width/height for this box, based on what it contains: */
  if (!boxTreatAsText(box))
  {
    /* If this is an "inline" box, newlines will need a "negative" X offset: */
    int parent_x = x + box->x;
    int parent_w = w;

    /* For blocks, X=0 is the left edge, but decrease the "width" available: */
    if (display != STYLE_DISPLAY_INLINE)
    {
      parent_x = 0;
      parent_w -=
        style.margin[STYLE_SIDE_LEFT]  + style.margin[STYLE_SIDE_RIGHT] +
        style.border[STYLE_SIDE_LEFT]  + style.border[STYLE_SIDE_RIGHT] +
        style.padding[STYLE_SIDE_LEFT] + style.padding[STYLE_SIDE_RIGHT];
    }

    box->w = 0;
    box->h = 0;

    for (i = 0; i < box->num_boxes; ++i)
    {
      box->box[i]->parent = box;
      boxRecalculate(box->box[i], encoding, parent_x, 0, parent_w, flags);

      if (box->w < box->box[i]->x + box->box[i]->w)
          box->w = box->box[i]->x + box->box[i]->w;
      if (box->h < box->box[i]->y + box->box[i]->h)
          box->h = box->box[i]->y + box->box[i]->h;
    }

    for (i = 0; i < box->num_boxes; ++i)
    {
      box->box[i]->x +=
        style.margin[STYLE_SIDE_LEFT] +
        style.border[STYLE_SIDE_LEFT] +
        style.padding[STYLE_SIDE_LEFT];
      box->box[i]->y +=
        style.margin[STYLE_SIDE_TOP] +
        style.border[STYLE_SIDE_TOP] +
        style.padding[STYLE_SIDE_TOP];
    }
  }
  else if ((flags & BOX_FLAG_FONTSIZE) || box->kind == BOX_GUI_INPUT)
  {
    error_t error = viewGetTextExtents(box, encoding, &box->w, &box->h);
    if (error != ERROR_SUCCESS) { return error; }
  }

  /* Now that we know the "calculated" width/height, enforce a minimum: */
  if (box->w < style.min_width)
      box->w = style.min_width;
  if (box->h < style.min_height)
      box->h = style.min_height;

  /* Add padding, margin, and border widths to the basic "content" size: */
  if (!boxTreatAsText(box) || box->kind == BOX_GUI_INPUT)
  {
    box->w +=
      style.margin[STYLE_SIDE_LEFT]  + style.margin[STYLE_SIDE_RIGHT] +
      style.border[STYLE_SIDE_LEFT]  + style.border[STYLE_SIDE_RIGHT] +
      style.padding[STYLE_SIDE_LEFT] + style.padding[STYLE_SIDE_RIGHT];
    box->h +=
      style.margin[STYLE_SIDE_TOP]  + style.margin[STYLE_SIDE_BOTTOM] +
      style.border[STYLE_SIDE_TOP]  + style.border[STYLE_SIDE_BOTTOM] +
      style.padding[STYLE_SIDE_TOP] + style.padding[STYLE_SIDE_BOTTOM];
  }

  if (box->kind == BOX_LINEBREAK)
  {
    /* Don't behave like `BOX_PARAGRAPH` (which pushes things down): */
    box->h = 0;
    /* Don't act like a "space" if we're at the start of a line: */
    if (previous_display == STYLE_DISPLAY_BLOCK) { box->w = 0; }
  }

  /* If this inline box is too wide for the page, wrap it on to a new line: */
  if (display != STYLE_DISPLAY_BLOCK && x + box->x + box->w > w)
  {
    box->x = -x;
    box->y = box->parent ? box->parent->h : 0;
  }

  return boxExpand(box, w - (x + box->x));
}

/**
@brief about
@param box about
@return writeme
*/

static int boxIsHighPriority(const Box *box)
{
  if (!box) { return 0; }
  return (
    box->kind == BOX_GUI_BUTTON  || box->kind == BOX_LINK ||
    box->kind == BOX_GOPHER_FILE ||
    box->kind == BOX_GOPHER_MENU || box->kind == BOX_GOPHER_TELNET ||
    box->kind == BOX_IMAGE || box->kind == BOX_UNKNOWN
  );
}

/*
[PUBLIC] about
*/

Box *boxAtPoint(Box *box, int x, int y)
{
  Box *child;
  unsigned long i;

  if (!box) { return NULL; }

  /*
  Inline "container" boxes (such as `BOX_BOLD`) could span multiple lines, so a
  basic AABB test won't work. Instead, we'll need to check all sub-boxes:
  */

  if (styleDisplay(box) == STYLE_DISPLAY_INLINE && box->num_boxes > 0)
  {
    for (i = 0; i < box->num_boxes; ++i)
    {
      /* If this box is "special", it "takes credit" for the overlap: */
      child = boxAtPoint(box->box[i], x - box->x, y - box->y);
      if (child) { return boxIsHighPriority(box) ? box : child; }
    }
  }
  else if (x >= box->x && x < box->x + box->w && y >= box->y && y < box->y + box->h)
  {
    /* If this box has no children (or it's "special"), use it as the result: */
    if (boxIsHighPriority(box) || box->num_boxes < 1) { return box; }

    /* Otherwise, see if any of the children are hit: */
    for (i = 0; i < box->num_boxes; ++i)
    {
      child = boxAtPoint(box->box[i], x - box->x, y - box->y);
      if (child) { return child; }
    }

    /* If no children are hit, just return this "container" box: */
    return box;
  }

  return NULL;
}

/*
[PUBLIC] about
*/

Box *boxRoot(Box *box)
{
  if (!box)
    return NULL;
  return box->parent ? boxRoot(box->parent) : box;
}

/*
[PUBLIC] about
*/

error_t boxInsertText(Box *box, const char *text, unsigned long *offset)
{
  size_t length, added, i;
  void *tmp;

  if (!box || !text || !offset) { return ERROR_INVALID_PARAMETER; }

  /* Like `strlen()`, but treats tabs as NUL-terminators: */
  for (added = 0; text[added] && text[added] != '\t'; ++added) { }

  length = box->text ? strlen(box->text) : 0;
  if (*offset > (unsigned long)length) { *offset = (unsigned long)length; }

  /* Allocate some extra memory to store the new characters: */
  tmp = realloc(box->text, length + added + 1);
  if (!tmp) { return ERROR_OUT_OF_MEMORY; }
  box->text = (char*)tmp;

  /* Insert the new text at the specified point: */
  for (i = length + 1; i > (size_t)*offset; --i) { box->text[i + added - 1] = box->text[i - 1]; }
  memcpy(&box->text[*offset], text, added);
  box->text[length + added] = 0;

  *offset += (unsigned long)added;
  return ERROR_SUCCESS;
}

error_t boxDeleteText(Box *box, unsigned long a, unsigned long b)
{
  unsigned long length, c, i;

  if (!box) { return ERROR_INVALID_PARAMETER; }

  /* Ensure that `a` and `b` are within range: */
  length = box->text ? strlen(box->text) : 0;
  if (a > length) { a = length; }
  if (b > length) { b = length; }

  /* If there aren't actually any characters to delete, just stop here: */
  if (a == b) { return ERROR_SUCCESS; }

  /* Otherwise, ensure that `a` comes before `b`: */
  if (a > b) { c = b; b = a; a = c; }

  /* Shift the text after the cut-off backwards, covering up the "gap" left: */
  for (c = b - a, i = a; i <= length - c; ++i) { box->text[i] = box->text[i + c]; }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] about
*/

error_t boxClearText(Box *box)
{
  if (!box) { return ERROR_INVALID_PARAMETER; }
  if (box->text) { box->text[0] = 0; }
  return ERROR_SUCCESS;
}
