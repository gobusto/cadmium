/**
@file
@brief writeme

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "History.h"

/**
@brief about

@param item about
@return writeme
*/

static HistoryItem *historyItemDestroy(HistoryItem *item)
{
  if (item) { free(item->url); }
  free(item);
  return NULL;
}

/**
@brief about

@param url about
@return writeme
*/

static HistoryItem *historyItemCreate(const char *url)
{
  HistoryItem *item;

  if (!url) { return NULL; }

  item = (HistoryItem*)malloc(sizeof(HistoryItem));
  if (!item) { return NULL; }
  memset(item, 0, sizeof(HistoryItem));

  item->url = (char*)malloc(strlen(url) + 1);
  if (!item->url) { return historyItemDestroy(item); }
  strcpy(item->url, url);

  return item;
}

/*
[PUBLIC] about
*/

History *historyCreate(void)
{
  History *history;

  history = (History*)malloc(sizeof(History));
  if (!history) { return NULL; }
  memset(history, 0, sizeof(History));

  return history;
}

/*
[PUBLIC] about
*/

History *historyDestroy(History *history)
{
  if (!history) { return NULL; }

  if (history->item)
  {
    HistorySize i;
    for (i = 0; i < history->num_items; ++i) { historyItemDestroy(history->item[i]); }
    free(history->item);
  }

  free(history);
  return NULL;
}

/*
[PUBLIC] about
*/

error_t historyAdd(History *history, const char *text)
{
  HistorySize i;

  if (!history || !text) { return ERROR_INVALID_PARAMETER; }

  if (history->current_item + 1 < history->num_items)
  {
    /* We went back, so there are later "history" items ahead; clear them: */
    for (i = history->current_item + 1; i < history->num_items; ++i)
    {
      historyItemDestroy(history->item[i]);
    }
    /* Pretend that the current item is at the end of the list: */
    history->num_items = history->current_item + 1;
  }
  else
  {
    /* We genuinely ARE at the end of the list; expand it */
    void *p = realloc(history->item, sizeof(HistoryItem*) * (history->num_items + 1));
    if (!p) { return ERROR_OUT_OF_MEMORY; }
    history->item = (HistoryItem**)p;
  }

  /* Either way, we now have at least one extra slot at the end of the list: */
  history->item[history->num_items] = historyItemCreate(text);
  if (!history->item[history->num_items]) { return ERROR_OUT_OF_MEMORY; }
  history->current_item = history->num_items++;
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about
*/

error_t historyBack(History *history)
{
  if (!history) { return ERROR_INVALID_PARAMETER; }
  if (!historyCanGoBack(history)) { return ERROR_INVALID_PARAMETER; }
  --history->current_item;
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about
*/

error_t historyForward(History *history)
{
  if (!history) { return ERROR_INVALID_PARAMETER; }
  if (!historyCanGoForward(history)) { return ERROR_INVALID_PARAMETER; }
  ++history->current_item;
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about
*/

int historyCanGoBack(const History *history)
{
  return history ? history->current_item > 0 : 0;
}

/*
[PUBLIC] about
*/

int historyCanGoForward(const History *history)
{
  return history ? history->current_item + 1 < history->num_items : 0;
}

/*
[PUBLIC] about
*/

HistoryItem *historyCurrent(const History *history)
{
  if (!history || history->current_item >= history->num_items) { return NULL; }
  return history->item[history->current_item];
}
