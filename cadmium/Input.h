/**
@file
@brief writeme

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_INPUT_H__
#define __QQQ_INPUT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "Browser.h"

/**
@brief about

@param browser about
@param text about
@return writeme
*/

error_t inputText(Browser *browser, const char *text);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputClear(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputLeft(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputRight(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputHome(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputEnd(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputDelete(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputBackspace(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t inputReturn(Browser *browser);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_INPUT_H__ */
