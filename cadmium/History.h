/**
@file
@brief writeme

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_HISTORY_H__
#define __QQQ_HISTORY_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "css/error/error.h"

/**
@brief about

@param h about
@return writeme
*/

#define historyCurrentUrl(h) (historyCurrent(h) ? historyCurrent(h)->url : NULL)

/**
@brief about
*/

typedef unsigned long HistorySize;

/**
@brief about
*/

typedef struct {

  char *url;  /**< @brief about */

  int x;  /**< @brief about */
  int y;  /**< @brief about */

} HistoryItem;

/**
@brief about
*/

typedef struct {

  HistorySize current_item; /**< @brief about */
  HistorySize num_items;    /**< @brief about */

  HistoryItem **item; /**< @brief about */

} History;

/**
@brief about

@return writeme
*/

History *historyCreate(void);

/**
@brief about

@param history about
@return Always returns `NULL`.
*/

History *historyDestroy(History *history);

/**
@brief about

@param history about
@param text about
@return writeme
*/

error_t historyAdd(History *history, const char *text);

/**
@brief about

@param history about
@return writeme
*/

error_t historyBack(History *history);

/**
@brief about

@param history about
@return writeme
*/

error_t historyForward(History *history);

/**
@brief about

@param history about
@return writeme
*/

int historyCanGoBack(const History *history);

/**
@brief about

@param history about
@return writeme
*/

int historyCanGoForward(const History *history);

/**
@brief about

@param history about
@return writeme
*/

HistoryItem *historyCurrent(const History *history);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_HISTORY_H__ */
