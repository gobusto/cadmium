/**
@file
@brief writeme

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Input.h"
#include "encoding/unicode.h"

/**
@brief Calculate the number of bytes the previous UTF-8 character takes up.

This is used for moving the text cursor and backspace-ing text.

@param browser about
@return writeme
*/

static unsigned long inputPreviousCharacterByteCount(const Browser *browser)
{
  const Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  unsigned long i;

  if (!box || !box->text) { return 0; }

  for (i = 1; i <= browser->text_cursor_position; ++i)
  {
    int n = utfDecodeUTF8(&box->text[browser->text_cursor_position - i], NULL);
    if (n > 0) { return (unsigned long)n; }
  }
  return 0;
}

/**
@brief Calculate the number of bytes the next UTF-8 character takes up.

This is used for moving the text cursor and delete-ing text.

@param browser about
@return writeme
*/

static unsigned long inputNextCharacterByteCount(const Browser *browser)
{
  const Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  const char *text;

  if (!box || !box->text) { return 0; }
  text = &box->text[browser->text_cursor_position];
  if (!text || text[0] == 0) { return 0; }

  return (unsigned long)utfDecodeUTF8(text, NULL);
}

/**
@brief about

@param browser about
@param text about
@return writeme
*/

static error_t inputTextLatin1(Browser *browser, const char *text)
{
  Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  if (!box || !text) { return ERROR_INVALID_PARAMETER; }

  for (; text[0]; ++text)
  {
    unsigned char value = ((unsigned char*)text)[0];
    char temp[8];
    utfEncodeUTF8(utfFromLatin1(value), temp);
    boxInsertText(box, temp, &browser->text_cursor_position);
  }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] about.
*/

error_t inputText(Browser *browser, const char *text)
{
  Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  const char *t;

  if (!box || !text) { return ERROR_INVALID_PARAMETER; }

  /* Determine whether or not this is valid UTF-8 text: */
  for (t = text; t[0]; )
  {
    int utf8_byte_count = utfDecodeUTF8(t, NULL);
    if (utf8_byte_count < 1) { return inputTextLatin1(browser, text); }
    t += utf8_byte_count;
  }

  /* If it is, insert it without changing anything: */
  return boxInsertText(box, text, &browser->text_cursor_position);
}

/*
[PUBLIC] about.
*/

error_t inputClear(Browser *browser)
{
  return boxClearText(browserGetFocus(browser, NULL, NULL, NULL, NULL));
}

/*
[PUBLIC] about.
*/

error_t inputLeft(Browser *browser)
{
  if (!browser) { return ERROR_INVALID_PARAMETER; }
  browser->text_cursor_position -= inputPreviousCharacterByteCount(browser);
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about.
*/

error_t inputRight(Browser *browser)
{
  if (!browser) { return ERROR_INVALID_PARAMETER; }
  browser->text_cursor_position += inputNextCharacterByteCount(browser);
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about.
*/

error_t inputHome(Browser *browser)
{
  if (!browser) { return ERROR_INVALID_PARAMETER; }
  browser->text_cursor_position = 0;
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about.
*/

error_t inputEnd(Browser *browser)
{
  Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  if (!browser) { return ERROR_INVALID_PARAMETER; }
  browser->text_cursor_position = (box && box->text) ? strlen(box->text) : 0;
  return ERROR_SUCCESS;
}

/*
[PUBLIC] about.
*/

error_t inputDelete(Browser *browser)
{
  Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  if (!browser) { return ERROR_INVALID_PARAMETER; }
  return boxDeleteText(
    box,
    browser->text_cursor_position,
    browser->text_cursor_position + inputNextCharacterByteCount(browser)
  );
}

/*
[PUBLIC] about.
*/

error_t inputBackspace(Browser *browser)
{
  if (!browser) { return ERROR_INVALID_PARAMETER; }
  if (browser->text_cursor_position == 0) { return ERROR_SUCCESS; }
  inputLeft(browser);
  return inputDelete(browser);
}

/*
[PUBLIC] about.
*/

error_t inputReturn(Browser *browser)
{
  const Box *box = browserGetFocus(browser, NULL, NULL, NULL, NULL);
  const char *value;
  char *search;
  error_t error;

  if (!box || !box->parent) { return ERROR_INVALID_PARAMETER; }

  value = box->text ? box->text : "";

  /*
  We can rely on `box->text` not being freed when `browserRequest()` clears the
  old page's box data, since `BOX_GUI_ADDRESS_BAR` is always part of the GUI:
  */

  if (box->parent->kind == BOX_GUI_ADDRESS_BAR)
    return browserRequest(browser, value);

  /*
  If this ISN'T an address bar, then it's either the GUI search OR an (in-page)
  Gopher query item. Both of these work in the same way - their `text` value is
  the Gopher selector used to handle the query text:
  */

  if (!box->parent->text)
    return ERROR_INVALID_PARAMETER;

  search = (char*)malloc(strlen(box->parent->text) + 1 + strlen(value) + 1);
  if (!search) { return ERROR_OUT_OF_MEMORY; }
  sprintf(search, "%s\t%s", box->parent->text, value);

  /*
  If this box is part of the page (rather than the UI), it will be destroyed by
  `browserRequest()` when the old page data is freed; we cannot assume that the
  box (or its `text` value) will be valid afterwards. Fortunately, we shouldn't
  need to either of those during (or after) the request:
  */

  error = browserRequest(browser, search);
  free(search);
  return error;
}
