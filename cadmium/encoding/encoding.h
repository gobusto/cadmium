/**
@file
@brief All functionality related to text encoding.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_ENCODING_H__
#define __QQQ_ENCODING_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates possible text encodings.

There is no `TEXT_ENCODING_ASCII` value, since UTF8 and LATIN-1 cover all ASCII
characters anyway.

Additional encodings (such as SHIFT-JIS) might be added in the future.
*/

typedef enum {

  TEXT_ENCODING_UNKNOWN,  /**< @brief The text encoding is not recognised.  */
  TEXT_ENCODING_LATIN1,   /**< @brief The text is encoded using ISO-8859-1. */
  TEXT_ENCODING_UTF8      /**< @brief The text is encoded as 8-bit Unicode. */

} TextEncoding;

/**
@brief Try to determine the encoding used by a string of text.

@param text The text to inspect.
@return The encoding detected.
*/

TextEncoding textEncoding(const char *text);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_ENCODING_H__ */
