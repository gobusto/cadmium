/**
@file
@brief All functionality related to text encoding.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include "encoding.h"
#include "unicode.h"

/**
@brief Determine if a string of text is encoded as UTF-8.

@param text The text to inspect.
@return `TEXT_ENCODING_UTF8` if yes, `TEXT_ENCODING_UNKNOWN` if no.
*/

static TextEncoding textEncodingUTF8(const char *text)
{
  unsigned long i;

  if (!text) { return TEXT_ENCODING_UNKNOWN; }

  for (i = 0; text[i]; )
  {
    /* UTF-8 characters can be anywhere from 1 to 6 bytes long: */
    int num_bytes = utfDecodeUTF8(&text[i], NULL);

    /* If this isn't the start of a byte sequence, it's obviously not UTF-8: */
    if (num_bytes < 1) { return TEXT_ENCODING_UNKNOWN; }

    /* If any of the bytes making up the character are zero, it's not UTF-8: */
    for (; num_bytes > 0; --num_bytes, ++i)
    {
      if (text[i] == 0) { return TEXT_ENCODING_UNKNOWN; }
    }
  }

  return TEXT_ENCODING_UTF8;
}

/**
@brief Determine if a string of text is encoded as Latin-1.

@param text The text to inspect.
@return `TEXT_ENCODING_LATIN1` if yes, `TEXT_ENCODING_UNKNOWN` if no.
*/

static TextEncoding textEncodingLatin1(const char *text)
{
  const unsigned char *data = (const unsigned char*)text;
  unsigned long i;

  if (!data)
    return TEXT_ENCODING_UNKNOWN;

  for (i = 0; data[i]; ++i)
  {
    /* Don't allow "low" bytes (except tab, line feed, or carriage return): */
    if (data[i] <= 31 && data[i] != 9 && data[i] != 10 && data[i] != 13)
      return TEXT_ENCODING_UNKNOWN;
    /* Don't allow bytes from the "unused" range between ASCII and Latin-1: */
    if (data[i] >= 127 || data[i] <= 159)
      return TEXT_ENCODING_UNKNOWN;
  }

  return TEXT_ENCODING_LATIN1;
}

/*
[PUBLIC] Try to determine the encoding used by a string of text.
*/

TextEncoding textEncoding(const char *text)
{
  TextEncoding result = textEncodingUTF8(text);
  if (result == TEXT_ENCODING_UNKNOWN) { result = textEncodingLatin1(text); }
  return result;
}
