/**
@file
@brief writeme

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_BROWSER_H__
#define __QQQ_BROWSER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "Model.h"
#include "Style.h"

/**
Represents the "browser" as a whole - tabs, content, etc.
*/

typedef struct {

  Box *gui;       /**< @brief about */

  Box *loading; /**< @brief about */
  Box *undisplayable; /**< @brief about */

  int client_w;   /**< @brief about */
  int client_h;   /**< @brief about */
  BoxFlags flags; /**< @brief about */

  unsigned long active_tab; /**< @brief about */
  unsigned long num_models; /**< @brief about */
  Model **model;            /**< @brief about */

  /* Text input state. */
  unsigned long text_cursor_position; /**< @brief about */

} Browser;

/**
@brief Create a new browser instance.

@param w about
@param h about
@param flags about
@return A new browser instance on success, or `NULL` on failure.
*/

Browser *browserCreate(int w, int h, BoxFlags flags);

/**
@brief Destroy a previously-created browser instance.

@param browser The browser instance to be destroyed.
@return Always returns `NULL`.
*/

Browser *browserDestroy(Browser *browser);

/**
@brief Resize the client area of a browser.

@param browser The browser instance to resize.
@param w about
@param h about
@return writeme
*/

error_t browserResize(Browser *browser, int w, int h);

/**
@brief writeme

@param browser The browser instance that contains the tab.
@param flags about
@return writeme
*/

error_t browserToggleFlags(Browser *browser, BoxFlags flags);

/**
@brief Create a new browser tab.

@return The index of the new tab.
*/

unsigned long browserCreateTab(Browser *browser);

/**
@brief Destroy a previously-created browser tab.

@param browser The browser instance that contains the tab.
@param tab The index of the tab be be destroyed.
@return writeme
*/

error_t browserDestroyTab(Browser *browser, unsigned long tab);

/**
@brief Start a new request.

@param browser The browser used to handle the request.
@param uri A `gopher://` or `file://` path.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t browserRequest(Browser *browser, const char *uri);

/**
@brief about

@param browser about
@return writeme
*/

error_t browserReload(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t browserHome(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t browserBack(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

error_t browserForward(Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

ModelState browserUpdate(Browser *browser);

/**
@brief about

@param browser about
@param n about
@return writeme
*/

error_t browserSetTab(Browser *browser, unsigned long n);

/**
@brief about

@param browser about
@return writeme
*/

unsigned long browserGetTab(const Browser *browser);

/**
@brief about

@param browser about
@return writeme
*/

Model *browserGetModel(const Browser *browser);

/**
@brief about

@param browser about
@param x about
@param y about
@return writeme
*/

error_t browserScrollBy(Browser *browser, int x, int y);

/**
@brief about

@param browser about
@param x about
@param y about
@return writeme
*/

StyleMouse browserSetMouse(const Browser *browser, int x, int y);

/**
@brief about

@param browser about
@param x about
@param y about
@return writeme
*/

int browserClick(Browser *browser, int x, int y);

/**
@brief about

@param browser about
@param x about
@param y about
@param w about
@param h about
@return writeme
*/

Box *browserGetFocus(const Browser *browser, int *x, int *y, int *w, int *h);

/**
@brief Save a model's data to a file.

@param browser The browser whose active model is to be written to disk.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t browserSave(Browser *browser);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_BROWSER_H__ */
