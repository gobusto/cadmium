/**
@file
@brief writeme

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Browser.h"

/**
@brief about

@param browser about
*/

static void browserReplaceAddressBarText(Box *box, const char *text)
{
  unsigned long i;

  if (!box || !text) { return; }

  /* Don't replace the address bar text with itself when return is pressed: */
  if (box->text == text) { return; }

  if (box->parent && box->parent->kind == BOX_GUI_ADDRESS_BAR && box->kind == BOX_GUI_INPUT)
  {
    unsigned long tmp = 0;
    boxClearText(box);
    boxInsertText(box, text, &tmp);
  }

  for (i = 0; i < box->num_boxes; ++i)
    browserReplaceAddressBarText(box->box[i], text);
}

/**
@brief about

@param browser about
@return writeme
*/

static int browserDefocus(Browser *browser)
{
  Model *model = browserGetModel(browser);
  int focus_lost = 0;

  if (!browser) { return 0; }

  if (browser->gui->user_data)
  {
    browser->gui->user_data = NULL;
    focus_lost = 1;
  }

  if (model)
  {
    if (model->basic_layout && model->basic_layout->user_data)
    {
      model->basic_layout->user_data = NULL;
      focus_lost = 1;
    }
    if (model->fancy_layout && model->fancy_layout->user_data)
    {
      model->fancy_layout->user_data = NULL;
      focus_lost = 1;
    }
  }

  return focus_lost;
}

/**
@brief about

@param browser about
@return writeme
*/

static Box *browserActiveElement(const Browser *browser)
{
  const Model *model = browserGetModel(browser);

  if (!browser)
    return NULL;

  if (browser->gui->user_data)
    return browser->gui->user_data;

  if (browser->flags & BOX_FLAG_MARKDOWN)
  {
    if (model && model->fancy_layout)
      return model->fancy_layout->user_data;
  }
  else
  {
    if (model && model->basic_layout)
      return model->basic_layout->user_data;
  }

  return NULL;
}

/*
[PUBLIC] about
*/

Browser *browserCreate(int w, int h, BoxFlags flags)
{
  Browser *browser;

  browser = (Browser*)malloc(sizeof(Browser));
  if (!browser) { return NULL; }
  memset(browser, 0, sizeof(Browser));

  browser->client_w = w;
  browser->client_h = h;
  browser->flags = flags;

  browser->gui = boxFromToolbar(browser->client_w);
  if (!browser->gui) { return browserDestroy(browser); }

  browser->loading = boxFromText(
    "Loading...",
    TEXT_ENCODING_UTF8,
    w,
    BOX_FLAG_MARKDOWN
  );
  if (!browser->loading) { return browserDestroy(browser); }

  browser->undisplayable = boxFromText(
    "This page cannot be displayed\n\nYou can save it by pressing CTRL+S",
    TEXT_ENCODING_UTF8,
    w,
    BOX_FLAG_MARKDOWN
  );
  if (!browser->undisplayable) { return browserDestroy(browser); }

  return browser;
}

/*
[PUBLIC] about
*/

Browser *browserDestroy(Browser *browser)
{
  unsigned long i;

  if (!browser) { return NULL; }

  if (browser->model)
  {
    for (i = 0; i < browser->num_models; ++i)
    {
      modelDestroy(browser->model[i]);
    }
    free(browser->model);
  }

  boxDestroy(browser->undisplayable);
  boxDestroy(browser->loading);
  boxDestroy(browser->gui);

  free(browser);
  return NULL;
}

/*
[PUBLIC] Resize the client area of a browser.
*/

error_t browserResize(Browser *browser, int w, int h)
{
  Model *model = browserGetModel(browser);

  if (!browser || w < 1 || h < 1) { return ERROR_INVALID_PARAMETER; }

  browser->client_w = w;
  browser->client_h = h;

  boxRecalculate(browser->gui, TEXT_ENCODING_UTF8, 0, 0, browser->client_w, 0);
  boxRecalculate(browser->loading, TEXT_ENCODING_UTF8, 0, 0, browser->client_w, 0);
  boxRecalculate(browser->undisplayable, TEXT_ENCODING_UTF8, 0, 0, browser->client_w, 0);

  if (!model) { return ERROR_SUCCESS; }
  return modelLayout(model, browser->client_w, browser->flags);
}

/*
[PUBLIC] about
*/

error_t browserToggleFlags(Browser *browser, BoxFlags flags)
{
  Model *model = browserGetModel(browser);

  if (!browser) { return ERROR_INVALID_PARAMETER; }

  if ((browser->flags & flags) == flags)
    browser->flags -= flags;
  else
    browser->flags |= flags;

  if (model) { return modelLayout(model, browser->client_w, browser->flags); }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] about
*/

unsigned long browserCreateTab(Browser *browser)
{
  void *tmp;

  if (!browser) { return 0; }

  tmp = realloc(browser->model, sizeof(Model*) * (browser->num_models + 1));
  if (!tmp) { return 0; }
  browser->model = (Model**)tmp;

  browser->model[browser->num_models] = modelCreate();
  if (!browser->model[browser->num_models]) { return 0; }
  return browser->num_models++;
}

/*
[PUBLIC] about
*/

error_t browserDestroyTab(Browser *browser, unsigned long tab)
{
  return ERROR_SUCCESS;  /* TODO */
}

/*
[PUBLIC] about
*/

error_t browserRequest(Browser *browser, const char *uri)
{
  Model *model = browserGetModel(browser);
  error_t error;

  if (!browser || !uri || !model) { return ERROR_INVALID_PARAMETER; }
  error = modelRequest(model, uri);
  browserReplaceAddressBarText(browser->gui, uri);
  if (error != ERROR_SUCCESS) { return error; }

  /* If we've made a `file://` request, it will already be complete: */
  return modelLayout(model, browser->client_w, browser->flags);
}

/*
[PUBLIC] about
*/

error_t browserReload(Browser *browser)
{
  Model *model = browserGetModel(browser);
  error_t error;

  if (!browser || !model) { return ERROR_INVALID_PARAMETER; }
  error = modelReload(model);
  browserReplaceAddressBarText(browser->gui, historyCurrentUrl(model->history));
  if (error != ERROR_SUCCESS) { return error; }

  /* If we've made a `file://` request, it will already be complete: */
  return modelLayout(model, browser->client_w, browser->flags);
}

/*
[PUBLIC] about
*/

error_t browserHome(Browser *browser)
{
  const char *uri = "gopher://gopher.floodgap.com/";
  browserReplaceAddressBarText(browser->gui, uri);
  return browserRequest(browser, uri);
}

/*
[PUBLIC] about
*/

error_t browserBack(Browser *browser)
{
  Model *model = browserGetModel(browser);
  error_t error;

  if (!browser || !model) { return ERROR_INVALID_PARAMETER; }
  error = modelBack(model);
  browserReplaceAddressBarText(browser->gui, historyCurrentUrl(model->history));
  if (error != ERROR_SUCCESS) { return error; }

  /* If we've made a `file://` request, it will already be complete: */
  return modelLayout(model, browser->client_w, browser->flags);
}

/*
[PUBLIC] about
*/

error_t browserForward(Browser *browser)
{
  Model *model = browserGetModel(browser);
  error_t error;

  if (!browser || !model) { return ERROR_INVALID_PARAMETER; }
  error = modelForward(model);
  browserReplaceAddressBarText(browser->gui, historyCurrentUrl(model->history));
  if (error != ERROR_SUCCESS) { return error; }

  /* If we've made a `file://` request, it will already be complete: */
  return modelLayout(model, browser->client_w, browser->flags);
}

/*
[PUBLIC] about
*/

ModelState browserUpdate(Browser *browser)
{
  ModelState active_state = MODEL_IDLE;
  unsigned long i;

  if (!browser) { return MODEL_INVALID; }

  for (i = 0; i < browser->num_models; ++i)
  {
    ModelState model_state = modelUpdate(browser->model[i]);

    if (i == browser->active_tab)
    {
      active_state = model_state;
      if (model_state == MODEL_FINISHED)
      {
        modelLayout(browser->model[i], browser->client_w, browser->flags);
      }
    }
  }

  return active_state;
}

/*
[PUBLIC] about
*/

error_t browserSetTab(Browser *browser, unsigned long n)
{
  if (!browser || n >= browser->num_models) { return ERROR_INVALID_PARAMETER; }
  browser->active_tab = n;
  browserReplaceAddressBarText(browser->gui, historyCurrentUrl(browser->model[n]->history));
  return browserResize(browser, browser->client_w, browser->client_h);
}

/*
[PUBLIC] about
*/

unsigned long browserGetTab(const Browser *browser)
{
  return browser ? browser->active_tab : 0;
}

/*
[PUBLIC] about
*/

Model *browserGetModel(const Browser *browser)
{
  if (!browser || browser->active_tab >= browser->num_models) { return NULL; }
  return browser->model[browser->active_tab];
}

/*
[PUBLIC] about
*/

error_t browserScrollBy(Browser *browser, int x, int y)
{
  return modelScrollBy(browserGetModel(browser), x, y);
}

/**
@brief about

@param browser about
@param x about
@param y about
@return writeme
*/

static Box *browserBoxAtPoint(const Browser *browser, int x, int y)
{
  const Model *model = browserGetModel(browser);
  Box *box;
  int scroll_x, scroll_y;

  if (!browser)
    return NULL;

  box = boxAtPoint(browser->gui, x, y);
  if (box)
    return box;
  else if (!model)
    return NULL;

  y -= browser->gui->y + browser->gui->h;
  modelGetScroll(model, &scroll_x, &scroll_y);
  x += scroll_x;
  y += scroll_y;

  if (browser->flags & BOX_FLAG_MARKDOWN)
    return boxAtPoint(model->fancy_layout, x, y);
  else
    return boxAtPoint(model->basic_layout, x, y);
}

/*
[PUBLIC] about
*/

StyleMouse browserSetMouse(const Browser *browser, int x, int y)
{
  const Box *box;
  Style style;

  if (!browser) { return STYLE_MOUSE_ARROW; }
  box = browserBoxAtPoint(browser, x, y);
  if (!box) { return STYLE_MOUSE_ARROW; }

  styleDirect(box, &style);
  return style.mouse;
}

/*
[PUBLIC] about
*/

int browserClick(Browser *browser, int x, int y)
{
  Box *box = browserBoxAtPoint(browser, x, y);
  int redraw_due_to_input_defocus = 0;

  /* FIXME: What if we clicked on the same text box again? */
  redraw_due_to_input_defocus = browserDefocus(browser);

  if (!box) { return redraw_due_to_input_defocus; }

  if (box->text && (
    box->kind == BOX_LINK ||
    box->kind == BOX_GOPHER_MENU ||
    box->kind == BOX_GOPHER_FILE ||
    box->kind == BOX_UNKNOWN ||
    box->kind == BOX_IMAGE)
  )
  {
    char *uri;

    /*
    Make a copy of the URI (since the box it belongs to is about to be replaced
    by the new page) to avoid use-after-free problems:
    */

    uri = (char*)malloc(strlen(box->text) + 1);
    if (!uri) { return 0; }
    strcpy(uri, box->text);

    browserRequest(browser, uri);

    free(uri);
    return 1;
  }
  else if (box->kind == BOX_GUI_BUTTON && box->text)
  {
    if (strcmp(box->text, "MARKDOWN") == 0)
    {
      browserToggleFlags(browser, BOX_FLAG_MARKDOWN);
      return 1;
    }
    else if (strcmp(box->text, "NEWLINES") == 0)
    {
      browserToggleFlags(browser, BOX_FLAG_NEWLINES);
      return 1;
    }
    else if (strcmp(box->text, "RELOAD") == 0)
    {
      browserReload(browser);
      return 1;
    }
    else if (strcmp(box->text, "BACK") == 0)
    {
      browserBack(browser);
      return 1;
    }
    else if (strcmp(box->text, "FORWARD") == 0)
    {
      browserForward(browser);
      return 1;
    }
    else if (strcmp(box->text, "HOME") == 0)
    {
      browserHome(browser);
      return 1;
    }
    else { fprintf(stderr, "[DEBUG] Unknown button type: %s\n", box->text); }
  }
  else if (box->kind == BOX_GUI_INPUT)
  {
    fprintf(stderr, "[DEBUG] Input focus has been changed...\n");
    boxRoot(box)->user_data = box;
    browser->text_cursor_position = box->text ? strlen(box->text) : 0;
    return 1;
  }

  /* We didn't click on anything; only redraw if we've lost input box focus: */
  return redraw_due_to_input_defocus;
}

/*
[PUBLIC about
*/

Box *browserGetFocus(const Browser *browser, int *x, int *y, int *w, int *h)
{
  Box *box = browserActiveElement(browser);

  if (!box) { return NULL; }

  /* FIXME: Report the absolute (NOT relative!) X/Y position here... */
  if (x) { *x = 0; }
  if (y) { *y = 0; }
  if (w) { *w = box->w; }
  if (h) { *h = box->h; }

  return box;
}

/*
[PUBLIC about
*/

error_t browserSave(Browser *browser)
{
  return modelSave(browserGetModel(browser));
}
