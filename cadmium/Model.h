/**
@file
@brief Essentially describes the contents of a single tab or browser window.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_MODEL_H__
#define __QQQ_MODEL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include "Box.h"
#include "History.h"
#include "bitmap/bitmap.h"
#include "gopher/gopher.h"
#include "encoding/encoding.h"
#include "xpsocket/xpsocket.h"
#include "css/error/error.h"

/**
@brief Enumerates possible "states" for a model to be in.

This isn't quite the same thing as `error_t`, though there is some overlap; the
state of a model indicates "what happened last", so the flow might be:

    receiving -> receiving -> receiving -> finished _> idle -> idle -> idle

That is, the model would use a "receiving" state until the data transfer ended,
at which point it would report that it has "finished" what it was doing. If any
further "updates" were requested, the state would be reported as "idle".
*/

typedef enum
{

  MODEL_INVALID,     /**< @brief Reported if a NULL pointer is supplied. */
  MODEL_OUTOFMEMORY, /**< @brief Reported if malloc/realloc has failed.  */
  MODEL_RECEIVING,   /**< @brief Reported when data transfer is ongoing. */
  MODEL_FINISHED,    /**< @brief Reported when data transfer completes.  */
  MODEL_IDLE         /**< @brief Indicates that nothing is in progress.  */

} ModelState;

/**
@brief Essentially describes the contents of a single tab or browser window.
*/

typedef struct {

  SOCKET sock;  /**< @brief WinSock/POSIX socket ID. */

  History *history; /**< @brief about */

  /* Raw response data, as received from the server: */
  size_t data_size;     /**< @brief Response size. */
  unsigned char *data;  /**< @brief Response data. */

  /* Interpreted response data - As a Gopher menu, image, etc. */
  GopherPage *gopher;   /**< @brief Response data as a Gopher menu.  */
  BitmapImage *bitmap;  /**< @brief Response data as a bitmap image. */

  /* Displayable response data - Converted from a Gopher menu or Markdown. */
  Box *basic_layout;  /**< @brief A simple page layout with minimal styling. */
  Box *fancy_layout;  /**< @brief A more complex page layout using Markdown. */

  /* Miscellaneous: */
  TextEncoding encoding;  /**< @brief Auto-detected response text encoding. */

} Model;

/**
@brief Create a new model structure, in a "default" state, ready to be used.

@return A new model structure on success, or NULL on failure.
*/

Model *modelCreate(void);

/**
@brief Destroy a previously-created model structure.

@param model The model to be freed from memory.
@return Always returns NULL.
*/

Model *modelDestroy(Model *model);

/**
@brief Save a model's data to a file.

@param model The model whose contents are to be written to disk.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t modelSave(const Model *model);

/**
@brief Recalculate the layout of a model.

@param model The model used to update.
@param w The width of the viewport in pixels.
@param flags Various flags influencing how the box model is calculated.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t modelLayout(Model *model, int w, BoxFlags flags);

/**
@brief Start a new request.

@param model The model used to handle the request.
@param uri A `gopher://` or `file://` path.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t modelRequest(Model *model, const char *uri);

/**
@brief writeme

@param model The model used to handle the request.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t modelReload(Model *model);

/**
@brief writeme

@param model The model used to handle the request.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t modelBack(Model *model);

/**
@brief writeme

@param model The model used to handle the request.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t modelForward(Model *model);

/**
@brief Update a model (i.e. download more data from an open connection).

@param model The model to be updated.
@return A suitable "state" value, describing what the model is actually doing.
*/

ModelState modelUpdate(Model *model);

/**
@brief about

@param model about
@return writeme
*/

ModelState modelState(const Model *model);

/**
@brief about

@param model about
@param x about
@param y about
@return writeme
*/

error_t modelGetScroll(const Model *model, int *x, int *y);

/**
@brief about

@param model about
@param x about
@param y about
@return writeme
*/

error_t modelScrollBy(Model *model, int x, int y);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_MODEL_H__ */
