/**
@file
@brief All functionality related to layout calculations.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_BOX_H__
#define __QQQ_BOX_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "encoding/encoding.h"
#include "gopher/gopher.h"
#include "css/error/error.h"

/* Flags: */

#define BOX_FLAG_MARKDOWN 0x01  /**< @brief Interpret text as Markdown.   */
#define BOX_FLAG_NEWLINES 0x02  /**< @brief Pay attention to line breaks. */
#define BOX_FLAG_FONTSIZE 0x04  /**< @brief Recalculate text block sizes. */
#define BOX_FLAG_UNUSED_3 0x08  /**< @brief Reserved for future use.      */
#define BOX_FLAG_UNUSED_4 0x10  /**< @brief Reserved for future use.      */
#define BOX_FLAG_UNUSED_5 0x20  /**< @brief Reserved for future use.      */
#define BOX_FLAG_UNUSED_6 0x40  /**< @brief Reserved for future use.      */
#define BOX_FLAG_UNUSED_7 0x80  /**< @brief Reserved for future use.      */

typedef unsigned char BoxFlags; /**< @brief Used to store box-related flags. */

/**
@brief Enumerates possible kinds of box within a box model.
*/

typedef enum {

  BOX_TEXT,       /**< @brief A single word. May be prefixed with spaces. */
  BOX_EMOJI,      /**< @brief Like `BOX_TEXT`, but explicitly for emoji.  */
  BOX_ICON,       /**< @brief Like `BOX_TEXT`, but explicitly for icons.  */
  BOX_LINEBREAK,  /**< @brief Represents a line break in the source text. */
  BOX_PARAGRAPH,  /**< @brief Like `BOX_LINEBREAK`, but with a "height".  */
  BOX_IMAGE,      /**< @brief An image in a Gopher menu or Markdown text. */

  /* Gopher menu items get their own types, distinct from Markdown things: */
  BOX_GOPHER_FILE,    /**< @brief A Gopher file link. */
  BOX_GOPHER_MENU,    /**< @brief A Gopher directory. */
  BOX_GOPHER_QUERY,   /**< @brief A Gopher Index-Search. */
  BOX_GOPHER_ERROR,   /**< @brief A Gopher error message. */
  BOX_GOPHER_TELNET,  /**< @brief A Gopher telnet (or CSO) link. */

  /* Markdown formatting containers: */
  BOX_HEADER1,          /**< @brief Heading (level 1). */
  BOX_HEADER2,          /**< @brief Heading (level 2). */
  BOX_HEADER3,          /**< @brief Heading (level 3). */
  BOX_HEADER4,          /**< @brief Heading (level 4). */
  BOX_HEADER5,          /**< @brief Heading (level 5). */
  BOX_HEADER6,          /**< @brief Heading (level 6). */
  BOX_BULLET_LIST,      /**< @brief An unordered (bullet-point) list. */
  BOX_NUMBER_LIST,      /**< @brief An ordered (numbered) list.       */
  BOX_LIST_ITEM,        /**< @brief An item within a list.            */
  BOX_LINK,             /**< @brief A link within Markdown text. */
  BOX_QUOTE,            /**< @brief Block-quote container. */
  BOX_CODE_BLOCK,       /**< @brief A block of code. */
  BOX_CODE_INLINE,      /**< @brief An inline code fragment. */
  BOX_ITALIC,           /**< @brief Italic text container. */
  BOX_BOLD,             /**< @brief Bold text container. */
  BOX_HORIZONTAL_RULE,  /**< @brief A horizontal rule. */

  /* Internal browser containers (used for interface chrome): */
  BOX_GUI,              /**< @brief The "toolbar" container (for toolbars). */
  BOX_GUI_CONTENT,      /**< @brief The "content" container (for the page). */
  BOX_GUI_TOOLBAR,      /**< @brief Contains various other interface items. */
  BOX_GUI_BUTTON,       /**< @brief A button of some sort: Back, Stop, etc. */
  BOX_GUI_ADDRESS_BAR,  /**< @brief The URL entry bar.                      */
  BOX_GUI_SEARCH_BOX,   /**< @brief The search box.                         */
  BOX_GUI_TAB_BAR,      /**< @brief Contains one or more tabs.              */
  BOX_GUI_TAB,          /**< @brief Represents a single tab within the bar. */
  BOX_GUI_INPUT,        /**< @brief Represents a text input box.            */

  /* Miscellaneous: */
  BOX_ICON_CONTAINER, /**< @brief Used to contain the icons on Gopher items. */
  BOX_UNKNOWN         /**< @brief Represents something not otherwise covered. */

} BoxType;

/**
@brief Represents a single box within a tree of boxes.
*/

struct _Box {

  /* Basic information about this element: */
  BoxType kind; /**< @brief Which type of box is this?                      */
  char *text;   /**< @brief Text for `BOX_TEXT`, URI for `BOX_GOPHER_FILE`. */

  /* AABB information: */
  int x;  /**< @brief Calculated X position. */
  int y;  /**< @brief Calculated Y position. */
  int w;  /**< @brief Calculated width. */
  int h;  /**< @brief Calculated height. */

  /* Child elements (if present): */
  unsigned long num_boxes;  /**< @brief Child box count. */
  struct _Box **box;        /**< @brief Child box array. */

  /* Parent element (if present): */
  struct _Box *parent;  /**< @brief Parent box structure. */

  /* General data pointer; not used internally by the Box code. */
  void *user_data;  /**< @brief General-use, miscellaneous data pointer. */
};

typedef struct _Box Box;  /**< @brief A `typedef`'d name for convenience. */

/**
@brief about

@param box about
@return writeme
*/

const char *boxTypeText(const Box *box);

/**
@brief Create a new box structure.

@param parent The box that contains this box, or `NULL` if none.
@param kind The kind of box to create.
@param text The text value (if any) for the box, or `NULL` if none.
@param len The length of the text field.
@return A new Box structure on success, or `NULL` on failure.
*/

Box *boxCreate(Box *parent, BoxType kind, const char *text, size_t len);

/**
@brief Destroy a box, along with any child boxes that it contains.

@param box The box to be destroyed.
@return Always returns `NULL`
*/

Box *boxDestroy(Box *box);

/**
@brief Build a box hierarchy for the toolbar and tabs.

@param page_width The width of the viewport in pixels.
@return A new Box structure on success, or `NULL` on failure.
*/

Box *boxFromToolbar(int page_width);

/**
@brief Build a box hierarchy from a Gopher menu.

@param page The Gopher menu to build from.
@param encoding The text encoding to use.
@param w The width of the viewport in pixels.
@param flags Various flags influencing how the box model is built.
@return A new Box structure on success, or `NULL` on failure.
*/

Box *boxFromGopher(const GopherPage *page, TextEncoding encoding, int w, BoxFlags flags);

/**
@brief Build a box hierarchy from a string of plain (or Markdown) text.

@param text The text to build from.
@param encoding The text encoding to use.
@param w The width of the viewport in pixels.
@param flags Various flags influencing how the box model is built.
@return A new Box structure on success, or `NULL` on failure.
*/

Box *boxFromText(const char *text, TextEncoding encoding, int w, BoxFlags flags);

/**
@brief Recalculate the positions of a box (and any child boxes) on the page.

This could be called when the window is resized, or if `BOX_FLAG_NEWLINES` gets
toggled on/off by the user.

Note that the `BOX_FLAG_MARKDOWN` flag is ignored here; this function only sets
the positions of existing elements, and doesn't create new ones.

@param box The box whose layout is to be recalculated.
@param encoding The text encoding to use.
@param x The horizontal offset of the left edge of the viewport in pixels.
@param y The vertical offset of the top edge of the viewport in pixels.
@param w The width of the viewport in pixels.
@param flags Various flags influencing how the box model is calculated.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t boxRecalculate(Box *box, TextEncoding encoding, int x, int y, int w, BoxFlags flags);

/**
@brief about

@param box about
@param x about
@param y about
@return writeme
*/

Box *boxAtPoint(Box *box, int x, int y);

/**
@brief about

@param box about
@return writeme
*/

Box *boxRoot(Box *box);

/**
@brief Insert additional text into the `text` string of a box.

@param box The box to be updated.
@param text The text to insert.
@param offset The position (in bytes, NOT characters) to insert the text at.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t boxInsertText(Box *box, const char *text, unsigned long *offset);

/**
@brief Delete text from the `text` string of a box.

@param box The box to be updated.
@param a The position (in bytes, NOT characters) at which to start deletion.
@param b The position (in bytes, NOT characters) at which to end deletion.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t boxDeleteText(Box *box, unsigned long a, unsigned long b);

/**
@brief Clear the `text` string of a box.

@param box The box to be updated.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t boxClearText(Box *box);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_BOX_H__ */
