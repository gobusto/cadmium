/**
@file
@brief All functionality related to visual styling.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_STYLE_H__
#define __QQQ_STYLE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "Box.h"

/* Box sides: */

#define STYLE_SIDE_TOP    0 /**< @brief about */
#define STYLE_SIDE_BOTTOM 1 /**< @brief about */
#define STYLE_SIDE_LEFT   2 /**< @brief about */
#define STYLE_SIDE_RIGHT  3 /**< @brief about */

/* Flags: */

#define STYLE_FLAG_FONTSIZE 0x01  /**< @brief Don't inherit font size.       */
#define STYLE_FLAG_FGCOLOUR 0x02  /**< @brief Don't inherit foreground RGBA. */
#define STYLE_FLAG_BGCOLOUR 0x04  /**< @brief Don't inherit background RGBA. */
#define STYLE_FLAG_UNUSED_3 0x08  /**< @brief Reserved for future use.       */
#define STYLE_FLAG_UNUSED_4 0x10  /**< @brief Reserved for future use.       */
#define STYLE_FLAG_UNUSED_5 0x20  /**< @brief Reserved for future use.       */
#define STYLE_FLAG_UNUSED_6 0x40  /**< @brief Reserved for future use.       */
#define STYLE_FLAG_UNUSED_7 0x80  /**< @brief Reserved for future use.       */

typedef unsigned char StyleFlags; /**< @brief Used to store styling flags. */

/**
@brief Enumerates possible "mouse pointer" types.
*/

typedef enum {

  STYLE_MOUSE_ARROW,  /**< @brief about */
  STYLE_MOUSE_IBEAM,  /**< @brief about */
  STYLE_MOUSE_HAND    /**< @brief about */

} StyleMouse;

/**
@brief Enumerates possible "display" types for a box.
*/

typedef enum {

  STYLE_DISPLAY_INLINE,       /**< @brief Display alongside other boxes.   */
  STYLE_DISPLAY_INLINE_BLOCK, /**< @brief Display alongside other boxes.   */
  STYLE_DISPLAY_BLOCK         /**< @brief Display on a new line by itself. */

} StyleDisplay;

/**
@brief Enumerates possible font weights.
*/

typedef enum {

  STYLE_WEIGHT_INHERIT, /**< @brief Do whatever the parent does. */
  STYLE_WEIGHT_NORMAL,  /**< @brief Non-bold text.                           */
  STYLE_WEIGHT_BOLD     /**< @brief Bold text.                               */

} StyleWeight;

/**
@brief Enumerates possible font slants.
*/

typedef enum {

  STYLE_SLANT_INHERIT,  /**< @brief Do whatever the parent does. */
  STYLE_SLANT_NORMAL,   /**< @brief Non-italic text.             */
  STYLE_SLANT_ITALIC    /**< @brief Italic text.                 */

} StyleSlant;

/**
@brief Enumerates possible font underline styles.
*/

typedef enum {

  STYLE_UNDERLINE_INHERIT,  /**< @brief Do whatever the parent does. */
  STYLE_UNDERLINE_NONE,     /**< @brief Non-underlined text.         */
  STYLE_UNDERLINE_SINGLE    /**< @brief Underlined text.             */

} StyleUnderline;

/**
@brief Enumerates possible font families for a box.
*/

typedef enum {

  STYLE_FONT_INHERIT,   /**< @brief Do whatever the parent does.           */
  STYLE_FONT_MONOSPACE, /**< @brief Use a fixed-width font.                */
  STYLE_FONT_VARIABLE,  /**< @brief Use a variable-width font.             */
  STYLE_FONT_ICON,      /**< @brief Use a font capable of rendering icons. */
  STYLE_FONT_EMOJI      /**< @brief Use a font capable of rendering emoji. */

} StyleFont;

/**
@brief This structure represents various styling rules in a single place.

Fields are (mostly) named after their nearest CSS equivalent.

@todo Add a "strikethrough" attribute?
*/

typedef struct {

  StyleMouse mouse; /**< @brief about */

  StyleWeight    font_weight;     /**< @brief Is the text bold?           */
  StyleSlant     font_style;      /**< @brief Is the text italic?         */
  StyleUnderline text_decoration; /**< @brief Is the text underlined?     */
  StyleFont      font_family;     /**< @brief Which kind of font is used? */

  int font_size;                  /**< @brief Which font size is used?     */
  unsigned char color[4];         /**< @brief What is the foreground RGBA? */
  unsigned char background[4];    /**< @brief What is the background RGBA? */
  /* FIXME: Allow this to be set for each side, as per CSS: */
  unsigned char border_color[4];  /**< @brief What is the border RGBA?     */

  int margin[4];  /**< @brief about */
  int border[4];  /**< @brief about */
  int padding[4]; /**< @brief about */

  int border_top_left_radius;     /**< @brief about */
  int border_top_right_radius;    /**< @brief about */
  int border_bottom_left_radius;  /**< @brief about */
  int border_bottom_right_radius; /**< @brief about */

  int min_width;  /**< @brief The minimum width in pixels.  */
  int min_height; /**< @brief The minimum height in pixels. */

  StyleFlags flags; /**< @brief Indicates which properties "matter" or not. */

} Style;

/**
@brief Determine whether a particular box should be displayed inline or not.

@param box The box whose inline-ness is to be determined.
@return The display style to be used for the specified box.
*/

StyleDisplay styleDisplay(const Box *box);

/**
@brief Get styling information for a particular box, with no inheritance.

@param box about
@param style about
@return writeme
*/

error_t styleDirect(const Box *box, Style *style);

/**
@brief Get styling information for a particular box including inherited values.

@param box about
@param style about
@return writeme
*/

error_t styleCalculated(const Box *box, Style *style);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_STYLE_H__ */
