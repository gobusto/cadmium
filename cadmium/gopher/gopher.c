/**
@file
@brief All functionality related to the parsing of Gopher server responses.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gopher.h"

/* Constants: */

#define GOPHER_COLUMN_CODE 0  /**< @brief The item type.     */
#define GOPHER_COLUMN_TEXT 1  /**< @brief The display text.  */
#define GOPHER_COLUMN_PATH 2  /**< @brief The selector text. */
#define GOPHER_COLUMN_HOST 3  /**< @brief The host text.     */
#define GOPHER_COLUMN_PORT 4  /**< @brief The port number.   */

/**
@brief Determine if a string of text starts with a newline.

Technically, only CR+LF counts as a newline, but various servers use just LF...

@param text The text to inspect.
@return The number of bytes making up the newline, or zero for anything else.
*/

static int gopherIsNewline(const char *text)
{
  if (!text) { return 0; }
  else if (text[0] == '\r' && text[1] == '\n') { return 2; }
  else if (text[0] == '\r' || text[0] == '\n') { return 1; }
  return 0;
}

/**
@brief Like `strlen()`, but treats `\t` as `0`.

@param text The text to get thye length of.
@param The length of the text in bytes.
*/

static size_t gopherTextLength(const char *text)
{
  size_t i;
  if (!text) { return 0; }
  for (i = 0; text[i] && text[i] != '\t' && text[i] != '\n' && text[i] != '\r'; ++i) {}
  return i;
}

/**
@brief Free a previously-created Gopher item structure from memory.

@param item The item structure to be freed.
@param Always returns `NULL`.
*/

static GopherItem *gopherDestroyItem(GopherItem *item)
{
  if (!item) { return NULL; }
  free(item->uri);
  free(item->text);
  free(item->path);
  free(item->host);
  free(item);
  return NULL;
}

/**
@brief Build a Gropher menu item structure.

@param code Determines which kind of item this is.
@param text Display text.
@param path Selector.
@param host Hostname.
@param port Port number.
@return A new Gopher item structure on success, or NULL on failure.
*/

static GopherItem *gopherCreateItem(
  GopherCode code,
  const char *text,
  const char *path,
  const char *host,
  unsigned short port
)
{
  GopherItem *item;
  size_t text_length, path_length, host_length;

  item = (GopherItem*)malloc(sizeof(GopherItem));
  if (!item) { return NULL; }
  memset(item, 0, sizeof(GopherItem));

  item->code = code;
  item->port = port;

  text_length = gopherTextLength(text);
  if (text_length)
  {
    item->text = (char*)malloc(text_length + 1);
    if (!item->text) { return gopherDestroyItem(item); }
    strncpy(item->text, text, text_length); item->text[text_length] = 0;
  }

  path_length = gopherTextLength(path);
  if (path_length)
  {
    item->path = (char*)malloc(path_length + 1);
    if (!item->path) { return gopherDestroyItem(item); }
    strncpy(item->path, path, path_length); item->path[path_length] = 0;
  }

  host_length = gopherTextLength(host);
  if (host_length)
  {
    item->host = (char*)malloc(host_length + 1);
    if (!item->host) { return gopherDestroyItem(item); }
    strncpy(item->host, host, host_length); item->host[host_length] = 0;
  }

  if (code == GOPHER_HTML && item->path && strncmp(item->path, "URL:", 4) == 0)
  {
    /* NOTE: Not `- 4`, since we need an extra byte for the NULL-terminator. */
    item->uri = (char*)malloc(path_length - 3);
    if (!item->uri) { return gopherDestroyItem(item); }
    strcpy(item->uri, &item->path[4]);
  }
  else if (code != GOPHER_ERROR && code != GOPHER_INFO)
  {
    const char *uri = "gopher";
    char code_as_string[2] = { 0, 0 };

    if (code == GOPHER_TELNET || code == GOPHER_TN3270)
      uri = "telnet";
    else if (code == GOPHER_PHONEBOOK)
      uri = "cso";
    else
      code_as_string[0] = code;

    /* "gopher" + "://" + "gopher.quux.org" + ":65535/" + "9" + "abcdef" + 0 */
    item->uri = (char*)malloc(strlen(uri) + host_length + path_length + 12 + 1);
    if (!item->uri) { return gopherDestroyItem(item); }
    sprintf(item->uri, "%s://%s:%u/%s%s",
      uri,
      item->host ? item->host : "",
      port,
      code_as_string,
      item->path ? item->path : ""
    );
  }

  return item;
}

/*
[PUBLIC] Build a page structure from a string of Gopher server response text.
*/

GopherPage *gopherCreatePage(const char *data)
{
  GopherPage *page;
  size_t length, i;
  int pass;

  if (!data) { return NULL; }
  length = strlen(data);

  for (pass = 0; pass < 2; ++pass)
  {
    unsigned long num_rows = 0;
    unsigned long num_cols = 0;

    GopherCode code;
    const char *text;
    const char *path;
    const char *host;
    unsigned short port;

    for (i = 0; i < length; ++i)
    {
      switch (num_cols)
      {
        /* The first character of the first column determines the item type: */
        case GOPHER_COLUMN_CODE:
          code = data[i];
          if (code == '.')
          {
            /*
            A `.` on a line by itself is supposed to mean "end-of-data"; some
            servers only send a LF afterwards, and some don't even send that.
            */
            for (++i; data[i] == '\r' || data[i] == '\n'; ++i) { }

            if (data[i] != 0)
            {
              fprintf(stderr, "[ERROR] Found extra data after a `.` item.\n");
              return NULL;
            }
          }
          /* Ignore newlines at the start of a line; just use the next byte: */
          else if (code != '\r' && code != '\n')
          {
            ++num_cols;
            text = &data[i + 1];
          }
        break;

        /* The result of the first column represents the displayable text: */
        case GOPHER_COLUMN_TEXT:
          if (data[i] == '\t')
          {
            ++num_cols;
            path = &data[i + 1];
          }
          else if (gopherIsNewline(&data[i]))
          {
            /*
            A newline in the text column SHOULD be an error... but some servers
            take liberties on info-type lines, so we need to handle those here:
            */
            if (code != GOPHER_INFO)
            {
              fprintf(stderr, "[ERROR] The text data contained a newline.\n");
              return NULL;
            }

            /* Skip past the newline characters: */
            while (data[i] == '\r' || data[i] == '\n') { ++i; }
            --i;

            /* `gopher.blog.benjojo.co.uk` has newlines before the tab: */
            if (data[i + 1] != '\t')
            {
              /* Other servers omit everything after the text column: */
              path = NULL;
              host = NULL;
              port = 0;
              /* Pretend that we're at the end of parsing the port column: */
              num_cols = GOPHER_COLUMN_PORT;
              i -= (data[i-1] == '\r' && data[i] == '\n') ? 2 : 1;
            }
          }
        break;

        /* This is followed by the "selector" for the resource... */
        case GOPHER_COLUMN_PATH:
          if (data[i] == '\t')
          {
            ++num_cols;
            host = &data[i + 1];
          }
          else if (gopherIsNewline(&data[i]))
          {
            fprintf(stderr, "[ERROR] The path data contained a newline.\n");
            return NULL;
          }
        break;

        /* ...and then the host on which that resource can be found: */
        case GOPHER_COLUMN_HOST:
          if (data[i] == '\t')
          {
            ++num_cols;
            /* Some servers return NEGATIVE port numbers for `i` lines! */
            if (data[i + 1] == '-')
            {
              fprintf(stderr, "[WARNING] Negative port number!\n");
              ++i;
            }
            port = atoi(&data[i + 1]);
          }
          else if (gopherIsNewline(&data[i]))
          {
            /*
            WORKAROUND: Some Gopher servers omit the "selector" column, meaning
            this is actually the "port" column - try to fix this retroactively:
            */
            fprintf(stderr, "[WARNING] Missing selector column!\n");
            /* Go back to the tab character at the start of this column: */
            while (&data[i] != host) { --i; }
            --i;
            /* What we thought was the selector is actually the host: */
            host = path;
            path = NULL;
          }
        break;

        /* The port column is (usually) the last one... */
        case GOPHER_COLUMN_PORT:
          /* SuperGlobalMegaCorp doesn't use a newline between port and `.`: */
          if (data[i] == '\t' || gopherIsNewline(&data[i]) || data[i] == '.')
          {
            if (pass > 0)
            {
              page->item[num_rows] = gopherCreateItem(code, text, path, host, port);
              if (!page->item[num_rows]) { return gopherDestroyPage(page); }
            }

            if (data[i] == '\t')
            {
              ++num_cols;
            }
            else
            {
              i += gopherIsNewline(&data[i]) - 1;
              num_cols = 0;
              ++num_rows;
            }
          }
          else if (data[i] < '0' || data[i] > '9')
          {
            fprintf(stderr, "[ERROR] The port contained a non-numeric character: 0x%02x\n", data[i]);
            if (code != GOPHER_INFO) { return NULL; }
          }
        break;

        /* Additional columns (such as Gopher+) may occur after the port: */
        default:
          if (data[i] == '\t')
          {
            ++num_cols;
          }
          else if (gopherIsNewline(&data[i]))
          {
            i += gopherIsNewline(&data[i]) - 1;
            num_cols = 0;
            ++num_rows;
          }
        break;
      }
    }

    /* If we reached the end of the data half-way though a row, that's bad: */
    if (num_rows < 1 || num_cols != 0)
    {
      fprintf(stderr, "[ERROR] Text is not a complete, valid Gopher menu.\n");
      return NULL;
    }

    /* Otherwise, allocate memory for a page structure and load ther data: */
    if (pass == 0)
    {
      page = (GopherPage*)malloc(sizeof(GopherPage));
      if (!page) { return NULL; }
      memset(page, 0, sizeof(GopherPage));

      page->num_items = num_rows;

      page->item = (GopherItem**)malloc(sizeof(GopherItem*) * num_rows);
      if (!page->item) { return gopherDestroyPage(page); }
      memset(page->item, 0, sizeof(GopherItem*) * num_rows);
    }
  }

  return page;
}

/*
[PUBLIC] Free a previously-created Gopher page structure from memory.
*/

GopherPage *gopherDestroyPage(GopherPage *page)
{
  if (!page) { return NULL; }
  if (page->item)
  {
    unsigned long i;
    for (i = 0; i < page->num_items; ++i) { gopherDestroyItem(page->item[i]); }
    free(page->item);
  }
  free(page);
  return NULL;
}

/*
[PUBLIC] Dump a Gopher Menu as a text file.
*/

int gopherSavePage(const GopherPage *page, const char *file_name)
{
  FILE *out;
  unsigned long i;

  if (!page || !file_name) { return -1; }
  out = fopen(file_name, "wb");
  if (!out) { return 1; }

  for (i = 0; i < page->num_items; ++i)
  {
    fprintf(out, "%c%s\t%s\t%s\t%u\r\n",
      page->item[i]->code,
      page->item[i]->text ? page->item[i]->text : "",
      page->item[i]->path ? page->item[i]->path : "",
      page->item[i]->host ? page->item[i]->host : "",
      page->item[i]->port
    );
  }
  fprintf(out, ".\r\n");

  fclose(out);
  return 0;
}
