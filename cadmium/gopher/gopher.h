/**
@file
@brief All functionality related to the parsing of Gopher server responses.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __GOPHER_H__
#define __GOPHER_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates known Gopher item types.

Most are from RFC-1436: <http://www.ietf.org/rfc/rfc1436.txt>

Additional items are from <https://en.wikipedia.org/wiki/Gopher_%28protocol%29>
and <http://lists.alioth.debian.org/pipermail/gopher-project/2011-March/000856.html>
in addition to various sightings around Gopherspace.

NOTE: <gopher://gopher.meulie.net:70/1/gopher/> has an 'M'-type file, though it
is just a plain-text document; this can probably be ignored...
*/

typedef enum {

  /* RFC-1436: */

  GOPHER_TEXT_FILE = '0', /**< @brief Item is a text file.                */
  GOPHER_DIRECTORY = '1', /**< @brief Item is a directory.                */
  GOPHER_PHONEBOOK = '2', /**< @brief Item is a CSO phone-book server.    */
  GOPHER_ERROR     = '3', /**< @brief Error.                              */
  GOPHER_BINHEX    = '4', /**< @brief Item is a BinHexed Macintosh file.  */
  GOPHER_ARCHIVE   = '5', /**< @brief Item is DOS binary archive.         */
  GOPHER_UUENCODE  = '6', /**< @brief Item is a UNIX uuencoded file.      */
  GOPHER_SEARCH    = '7', /**< @brief Item is an Index-Search server.     */
  GOPHER_TELNET    = '8', /**< @brief Item points to a telnet session.    */
  GOPHER_BIN_FILE  = '9', /**< @brief Item is a binary file!              */
  GOPHER_SERVER    = '+', /**< @brief Item is a redundant server.         */
  GOPHER_TN3270    = 'T', /**< @brief Item points to a tn3270 session.    */
  GOPHER_GIF       = 'g', /**< @brief Item is a GIF format graphics file. */
  GOPHER_IMAGE     = 'I', /**< @brief Item is some kind of image file.    */

  /* lists.alioth.debian.org */

  GOPHER_PNG       = 'p', /**< @brief Item is a PNG image file. */
  GOPHER_PDF       = 'd', /**< @brief Item is a PDF document.   */
  GOPHER_XML       = 'x', /**< @brief Item is an XML file.      */
  GOPHER_CSS       = 'c', /**< @brief Item is a CSS file.       */

  /* Wikipedia: */

  GOPHER_HTML      = 'h', /**< @brief HTML file.             */
  GOPHER_INFO      = 'i', /**< @brief Informational message. */
  GOPHER_SOUND     = 's', /**< @brief Sound file.            */

  /* gopher://ratthing.com:70/1/~rlopez */

  GOPHER_PDF_2     = 'P', /**< @brief Item is a PDF document. */

  /* gopher://gopher.petergarner.net:70/1/Political */

  GOPHER_MP4       = 'm', /**< @brief Item is a .MP4 video. */

  /* gopher://xepb.org/1/XEPb/68kavr/files */
  /* gopher://gopher.meulie.net:70/1/WorldofSpectrum/sinclair/videos/ */

  GOPHER_VIDEO     = ';', /**< @brief Item is a video file (MOV/AVI/MPEG). */

  /* gopher://gopher.meulie.net:70/0/textfiles/magazines/MISC */

  GOPHER_EMAIL     = 'M'  /**< @brief Item is a plain-text .EML file. */

} GopherCode;

/**
@brief A structure representing a single Gopher item.
*/

typedef struct {

  int code;  /**< @brief Determines which kind of item this is. */

  char *text; /**< @brief Display text. */
  char *path; /**< @brief Selector.     */
  char *host; /**< @brief Hostname.     */

  unsigned short port; /**< @brief Port number. */

  char *uri;  /**< @brief A combined URI string, if appropriate. */

} GopherItem;

/**
@brief A structure representing a complete Gopher response.
*/

typedef struct {

  unsigned long num_items;  /**< @brief The number of items on the page. */
  GopherItem **item;        /**< @brief A list of items on this page.    */

} GopherPage;

/**
@brief Build a page structure from a string of Gopher server response text.

@param data A string of text received from a Gopher server.
@return A new Gopher page structure on success, or NULL on failure.
*/

GopherPage *gopherCreatePage(const char *data);

/**
@brief Free a previously-created Gopher page structure from memory.

@param page The page structure to be freed.
@return Always returns `NULL`.
*/

GopherPage *gopherDestroyPage(GopherPage *page);

/**
@brief Dump a Gopher Menu as a text file.

@param page The page structure to output.
@param file_name The name of (and path to) the file to be written.
@return Zero on success, or non-zero on failure.
*/

int gopherSavePage(const GopherPage *page, const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __GOPHER_H__ */
