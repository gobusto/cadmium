/**
@file
@brief Handles basic GUI stuff such a user input and window events.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_SYSTEM_H__
#define __QQQ_SYSTEM_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise everything.

This function should be called when the program first starts.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int ctrlInit(int argc, char **argv);

/**
@brief Shut down everything and free any currently allocated memory.

This function should be called when the program terminates.
*/

void ctrlQuit(void);

/**
@brief This function handles the main progam loop.

Once called, this function will continue to run until a termination event is
received.
*/

void ctrlMainLoop(void);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_SYSTEM_H__ */
