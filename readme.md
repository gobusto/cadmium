Cadmium - A graphical browser for Gopherspace
=============================================

> "Now we're cooking with Cadmium!" - _Dan Avidan_

![A screenshot of the Cadmium Gopher client as of 2017-07-16](screenshot.png)

Cadmium is a modern GUI-based Gopher client. It supports Latin-1 or UTF-8 text,
and can render Gopher menus or text files in either classic Gopher style (fixed
width fonts, hard line breaks, no styling) or as Markdown-interpreted text with
"soft" line breaks.

GNU/Linux and Windows are officially supported, though macOS should work too.

How it works
------------

Strictly speaking, Cadmium refers to the browser engine, rather than the actual
GUI-based browser - compare Gecko vs. Firefox, or WebKit vs. Safari.

The Cadmium "core" is largely self-contained, and could be used as a "headless"
Gopher browser. The SDL2 front-end simply builds a GUI on top of this codebase,
so replacing SDL2 with something else (GTK+, Qt, etc.) is entirely possible.

Internally, the browser instance is represented by a `Browser` structure, which
may contain one or more `Model` structures, each representing a browser tab (or
file transfer). Models keep track of their navigation `History` and contain any
sub-structures necessary to represent the menu/file data they download, such as
`BitmapImage` data for images, or `Box` layout information for Gopher menus and
text files.

Box-based layouts work in much the same way as HTML/CSS do in web browsers; the
basic page data is structured into a tree of box "elements", whose position and
appearance are calculated based on the width of the browser window and any page
styling that may apply to them - see `Style.h` for more information.

Supported Gopher Types
----------------------

All "official" item types from [RFC-1436](https://www.ietf.org/rfc/rfc1436.txt)
are recognised, along with a few "de facto" ones seen around Gopherspace:

+ `+` - Mirror server.
+ `0` - Generic text file.
+ `1` - Gopher menu.
+ `2` - CSO phone-book server.
+ `3` - Error message.
+ `4` - BinHexed file.
+ `5` - Archive file.
+ `6` - uuencoded file.
+ `7` - Index-Search server.
+ `8` - Telnet session.
+ `9` - Generic binary file.
+ `;` - Video file.
+ `I` - Generic image file.
+ `M` - Plain-text EML file.
+ `T` - TN3270 session.
+ `c` - CSS file.
+ `d` or `P` - PDF file.
+ `g` - GIF image file.
+ `h` - HTML file.
+ `i` - Informational message.
+ `m` - MP4 video file.
+ `p` - PNG image file.
+ `s` - Sound file.
+ `x` - XML file.

Unrecognised codes are categorised as "unknown" items - these are usually files
in obscure formats not covered by the list above.

Keyboard Shortcuts
------------------

Several common web browser keyboard shortcuts are available:

+ **Arrow Keys:** Scroll the page up/down or left/right.
+ **F5:** Reload the current page.
+ **Alt + Home:** Go to the home-page.
+ **Alt + Left Arrow:** Go back (if possible).
+ **Alt + Right Arrow:** Go forward (if possible).
+ **Alt + N:** Toggle "soft" newlines on/off.
+ **Alt + M:** Toggle Markdown mode on/off.
+ **Ctrl + S:** Save the current menu/image/etc. as a local file.

Note that most of these actions are also available as buttons on the toolbar.

TODO
----

There's lots to do right now, so I've organised this section into a rough order
of priority in [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) style.

### Must

+ Don't use "blocking" socket connections; they can freeze the browser.
+ Implement Markdown parsing.

### Should

+ Get the "multiple tabs" code 100% working.
+ Use the correct directories for user settings/program files.
+ Support text selection for text boxes and page contents.
+ Implement Cut/Copy clipboard integration (Paste already works).
+ Include screen reader support.
+ Remember settings and history after restart.
+ Allow bookmarking.
+ Implement a "stop" button.
+ Show a vertical (and horizontal?) scrollbar.
+ Allow Page Up/Page Down to be used to scroll the page.
+ Properly handle text boxes overflowing if their contents are too long.
+ Fix the encoding checking so that binary files starting with ASCII text work.

### Could

+ Allow custom styles to be loaded from CSS files (code exists but isn't used).
+ Allow images to be displayed inline. This could be a toggleable option.
+ SDL2_TTF can't render non-BMP glyphs; maybe use Freetype (and Cairo) instead?

### Won't

+ Support HTML (though HTTP 1.0 support might be useful for downloading files).

Resources used
--------------

The Cadmium browser takes advantage of the following third-party projects:

### Libraries

+ [stb_image](https://github.com/nothings/stb) is used by the Cadmium core to handle bitmap images.
+ [SDL2](http://libsdl.org/) is used for the GUI front-end.
+ [SDL2_TTF](https://www.libsdl.org/projects/SDL_ttf/) is used to render text.

### Fonts

+ [Font Awesome](http://fontawesome.io/icons/) is used for the various icons.
+ [Twemoji](https://github.com/eosrei/twemoji-color-font) provides the emoji.
+ [Google Fonts](https://fonts.google.com/) provided the other bundled fonts.

Licensing
---------

Copyright (c) 2017 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
