/**
@file
@brief Handles all aspects of drawing.

Copyright (C) 2015-2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_RENDER__
#define __QQQ_RENDER__

#ifdef __cplusplus
extern "C" {
#endif

#include "cadmium/Browser.h"

/**
@brief Initialise the rendering subsystem.

@param w The width of the canvas in pixels.
@param h The height of the canvas in pixels.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t viewInit(int w, int h);

/**
@brief Shut down the rendering subsystem.
*/

void viewQuit(void);

/**
@brief Resize the canvas.

@param w The width of the canvas in pixels.
@param h The height of the canvas in pixels.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t viewResize(int w, int h);

/**
@brief Draw a visual representation of a model on the canvas.

@param browser The browser to render.
*/

void viewDraw(const Browser *browser);

/**
@brief Get a pointer to the raw canvas surface pixel data.

This is used by the controller (GUI) code to show the image within a window.

@param stride Optional: If present, the pixel stride value is copied here.
@return A pointer to the pixel data on success, or NULL on failure.
*/

unsigned char *viewGetData(int *stride);

/**
@brief Determine the bounding box size of a given string of text.

@param box A box representing an element within a page.
@param encoding The text encoding used by the `box->text` field.
@param w Optional: If present, the bounding box width is stored here.
@param h Optional: If present, the bounding box height is stored here.
@return ERROR_SUCCESS if all went well, or a suitable error value if not.
*/

error_t viewGetTextExtents(const Box *box, TextEncoding encoding, int *w, int *h);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_RENDER__ */
