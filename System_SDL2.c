/**
@file
@brief An SDL2 implementation of the functions outlined in Browser.h

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL2/SDL.h>
#include <stdlib.h>
#include "Render.h"
#include "System.h"
#include "cadmium/Input.h"

/* Global objects and variables. */

SDL_Window *ctrl_window = NULL; /**< @brief The SDL window. */
Browser *ctrl_browser = NULL;   /**< @brief about */

SDL_Cursor *ctrl_cursor_arrow = NULL; /**< @brief about */
SDL_Cursor *ctrl_cursor_ibeam = NULL; /**< @brief about */
SDL_Cursor *ctrl_cursor_hand  = NULL; /**< @brief about */

/*
[PUBLIC] Initialise everything.
*/

int ctrlInit(int argc, char *argv[])
{
  SDL_Surface *icon;
  int w = 1024;
  int h = 700;
  error_t error;
  unsigned long tab_id;

  ctrlQuit();

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) { return -1; }

  ctrl_window = SDL_CreateWindow(
    "Cadmium", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    w, h, SDL_WINDOW_RESIZABLE
  );
  if (!ctrl_window) { return -1; }

  /* Attempt to set a window icon: */
  icon = SDL_LoadBMP("icon.bmp");
  if (icon)
  {
    SDL_SetWindowIcon(ctrl_window, icon);
    SDL_FreeSurface(icon);
  }

  error = viewInit(w, h);
  if (error != ERROR_SUCCESS) { return -1; }

  ctrl_cursor_arrow = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
  ctrl_cursor_ibeam = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
  ctrl_cursor_hand  = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND );

  if (sockInit() != 0) { return -1; }

  ctrl_browser = browserCreate(w, h, BOX_FLAG_NEWLINES);
  if (!ctrl_browser) { return -1; }

  tab_id = browserCreateTab(ctrl_browser);
  browserSetTab(ctrl_browser, tab_id);
  browserRequest(ctrl_browser, argc > 1 ? argv[1] : "gopher://gopher.floodgap.com/");

  return 0;
}

/*
[PUBLIC] Shut down everything and free any currently allocated memory.
*/

void ctrlQuit(void)
{
  if (!SDL_WasInit(0)) { return; }

  ctrl_browser = browserDestroy(ctrl_browser);

  sockQuit();
  viewQuit();

  SDL_FreeCursor(ctrl_cursor_arrow); ctrl_cursor_arrow = NULL;
  SDL_FreeCursor(ctrl_cursor_ibeam); ctrl_cursor_ibeam = NULL;
  SDL_FreeCursor(ctrl_cursor_hand);  ctrl_cursor_hand = NULL;

  SDL_DestroyWindow(ctrl_window);
  ctrl_window = NULL;

  SDL_Quit();
}

/**
@brief Copy the image data into the window display surface.

@param window The SDL window to draw the image to.
@return SDL_TRUE on success, or SDL_FALSE if something goes wrong.
*/

static SDL_bool ctrlRepaint(SDL_Window *window)
{
  SDL_Surface *screen = SDL_GetWindowSurface(window);
  int pixmap_stride, r, g, b, x, y;
  unsigned char *dst = screen ? (unsigned char*)screen->pixels : NULL;
  unsigned char *src = viewGetData(&pixmap_stride);

  if (!src || !dst) { return SDL_FALSE; }

  switch (screen->format->format)
  {
    case SDL_PIXELFORMAT_BGR888:
    case SDL_PIXELFORMAT_BGRX8888:
    case SDL_PIXELFORMAT_BGRA8888: r = 2; g = 1; b = 0; break;

    case SDL_PIXELFORMAT_RGB888:
    case SDL_PIXELFORMAT_RGBX8888:
    case SDL_PIXELFORMAT_RGBA8888: r = 0; g = 1; b = 2; break;

    case SDL_PIXELFORMAT_ARGB8888: r = 1; g = 2; b = 3; break;
    case SDL_PIXELFORMAT_ABGR8888: r = 3; g = 2; b = 1; break;

    default: /* Not 24-bit or 32-bit. */ return SDL_FALSE;
  }

  SDL_LockSurface(screen);
  for (y = 0; y < screen->h; ++y) {
    for (x = 0; x < screen->w; ++x) {
      int src_i = (y * pixmap_stride) + (x * 4);  /* <- For RGB and ARGB. */
      int dst_i = (y * screen->pitch) + (x * screen->format->BytesPerPixel);
      dst[dst_i + r] = src[src_i + 0];
      dst[dst_i + g] = src[src_i + 1];
      dst[dst_i + b] = src[src_i + 2];
    }
  }
  SDL_UnlockSurface(screen);

  SDL_UpdateWindowSurface(window);
  return SDL_TRUE;
}

/**
@brief about

@param event about
@return writeme
*/

static SDL_bool ctrlHandleCommonKeyboardEvent(const SDL_Event *event)
{
  if (!event) { return SDL_FALSE; }

  if (event->key.keysym.mod & KMOD_CTRL)
  {
    if (event->key.keysym.sym == SDLK_s)
    {
      browserSave(ctrl_browser);
      return SDL_TRUE;
    }
  }

  return SDL_FALSE;
}

/**
@brief about

@param event about
@return writeme
*/

static SDL_bool ctrlHandleTextInputKeyboardEvent(const SDL_Event *event)
{
  if (!event) { return SDL_FALSE; }

  if (event->key.keysym.mod & KMOD_CTRL)
  {
    if (event->key.keysym.sym == SDLK_a)
    {
      /* TODO: The browser needs to be able to highlight text first... */
      fprintf(stderr, "[UNIMPLEMENTED] Select all\n");
      return SDL_FALSE;
    }
    else if (event->key.keysym.sym == SDLK_c)
    {
      /* TODO: The browser needs to be able to highlight text first... */
      fprintf(stderr, "[UNIMPLEMENTED] Copy-to-clipboard\n");
      return SDL_FALSE;
    }
    else if (event->key.keysym.sym == SDLK_x)
    {
      /* TODO: The browser needs to be able to highlight text first... */
      fprintf(stderr, "[UNIMPLEMENTED] Cut-to-clipboard\n");
      return SDL_FALSE;
    }
    else if (event->key.keysym.sym == SDLK_v)
    {
      inputText(ctrl_browser, SDL_GetClipboardText());
      return SDL_TRUE;
    }
  }

  switch (event->key.keysym.sym)
  {
    case SDLK_LEFT:      inputLeft(ctrl_browser);      return SDL_TRUE;
    case SDLK_RIGHT:     inputRight(ctrl_browser);     return SDL_TRUE;
    case SDLK_HOME:      inputHome(ctrl_browser);      return SDL_TRUE;
    case SDLK_END:       inputEnd(ctrl_browser);       return SDL_TRUE;
    case SDLK_DELETE:    inputDelete(ctrl_browser);    return SDL_TRUE;
    case SDLK_BACKSPACE: inputBackspace(ctrl_browser); return SDL_TRUE;
    case SDLK_KP_ENTER:  /* The numpad "enter" key works as per "return"... */
    case SDLK_RETURN:    inputReturn(ctrl_browser);    return SDL_TRUE;
    case SDLK_ESCAPE:    inputClear(ctrl_browser);     return SDL_TRUE;
    default: break;
  }

  return SDL_FALSE;
}

/**
@brief about

@param event about
@return writeme
*/

static SDL_bool ctrlHandleNonTextInputKeyboardEvent(const SDL_Event *event)
{
  if (!event) { return SDL_FALSE; }

  if (event->key.keysym.mod & KMOD_ALT)
  {
    if (event->key.keysym.sym == SDLK_HOME)
    {
      browserHome(ctrl_browser);
      return SDL_TRUE;
    }
    else if (event->key.keysym.sym == SDLK_LEFT)
    {
      browserBack(ctrl_browser);
      return SDL_TRUE;
    }
    else if (event->key.keysym.sym == SDLK_RIGHT)
    {
      browserForward(ctrl_browser);
      return SDL_TRUE;
    }
    else if (event->key.keysym.sym == SDLK_n)
    {
      browserToggleFlags(ctrl_browser, BOX_FLAG_NEWLINES);
      return SDL_TRUE;
    }
    else if (event->key.keysym.sym == SDLK_m)
    {
      browserToggleFlags(ctrl_browser, BOX_FLAG_MARKDOWN);
      return SDL_TRUE;
    }
    return SDL_FALSE;
  }

  if (event->key.keysym.sym == SDLK_F5)
  {
    browserReload(ctrl_browser);
    return SDL_TRUE;
  }
  else if (event->key.keysym.sym == SDLK_LEFT)
  {
    browserScrollBy(ctrl_browser, -32, 0);
    return SDL_TRUE;
  }
  else if (event->key.keysym.sym == SDLK_RIGHT)
  {
    browserScrollBy(ctrl_browser, 32, 0);
    return SDL_TRUE;
  }
  else if (event->key.keysym.sym == SDLK_UP)
  {
    browserScrollBy(ctrl_browser, 0, -32);
    return SDL_TRUE;
  }
  else if (event->key.keysym.sym == SDLK_DOWN)
  {
    browserScrollBy(ctrl_browser, 0, 32);
    return SDL_TRUE;
  }

  return SDL_FALSE;
}

/**
@brief about

@param about
*/

static void ctrlSetPointer(StyleMouse mouse_pointer_type)
{
  switch (mouse_pointer_type)
  {
    case STYLE_MOUSE_HAND:  SDL_SetCursor(ctrl_cursor_hand);  break;
    case STYLE_MOUSE_IBEAM: SDL_SetCursor(ctrl_cursor_ibeam); break;
    case STYLE_MOUSE_ARROW: SDL_SetCursor(ctrl_cursor_arrow); break;
    default:                SDL_SetCursor(ctrl_cursor_arrow); break;
  }
}

/**
@brief about
*/

static void ctrlResize(void)
{
  int w, h;
  SDL_GetWindowSize(ctrl_window, &w, &h);
  if (viewResize(w, h) != ERROR_SUCCESS) { return; }
  if (browserResize(ctrl_browser, w, h) != ERROR_SUCCESS) { return; }
}

/*
[PUBLIC] This function handles the main progam loop.
*/

void ctrlMainLoop(void)
{
  SDL_bool want_redraw = SDL_TRUE;
  SDL_bool want_redisplay = SDL_TRUE;
  SDL_Event event;

  /* For some reason, this is active by default - turn it off... */
  if (SDL_IsTextInputActive()) { SDL_StopTextInput(); }

  while (SDL_TRUE)
  {
    ModelState status = browserUpdate(ctrl_browser);
    want_redraw = (status != MODEL_RECEIVING && status != MODEL_IDLE);

    while (SDL_PollEvent(&event))
    {
      if (event.type == SDL_WINDOWEVENT)
      {
        if (event.window.event == SDL_WINDOWEVENT_RESIZED)
        {
          ctrlResize();
          want_redraw = SDL_TRUE;
        }
        /* Draw the display when the window is first shown. */
        else if (event.window.event == SDL_WINDOWEVENT_SHOWN) { want_redraw = SDL_TRUE; }
        /* Only refresh the display if it's necessary to do so. */
        else if (event.window.event == SDL_WINDOWEVENT_EXPOSED) { want_redisplay = SDL_TRUE; }
      }
      else if (event.type == SDL_TEXTEDITING)
      {
        /* NOTE: SDL2 doesn't seem to work with SCIM (or iBus?) yet... */
        fprintf(stderr, "[DEBUG] Got a SDL_TEXTEDITING event...\n");
      }
      else if (event.type == SDL_TEXTINPUT)
      {
        inputText(ctrl_browser, event.text.text);
        want_redraw = SDL_TRUE;
      }
      else if (event.type == SDL_KEYDOWN)
      {
        /* Some keyboard shortcuts always do the same thing... */
        if (ctrlHandleCommonKeyboardEvent(&event)) { want_redraw = SDL_TRUE; }

        /* ...whilst others become (un)available when a text box has focus: */
        if (SDL_IsTextInputActive())
        {
          if (ctrlHandleTextInputKeyboardEvent(&event)) { want_redraw = SDL_TRUE; }
        }
        else
        {
          if (ctrlHandleNonTextInputKeyboardEvent(&event)) { want_redraw = SDL_TRUE; }
        }
      }
      else if (event.type == SDL_MOUSEBUTTONUP)
      {
        SDL_Rect rect;

        if (browserClick(ctrl_browser, event.button.x, event.button.y)) { want_redraw = SDL_TRUE; }

        if (SDL_IsTextInputActive()) { SDL_StopTextInput(); }

        if (browserGetFocus(ctrl_browser, &rect.x, &rect.y, &rect.w, &rect.h))
        {
          SDL_StartTextInput();
          SDL_SetTextInputRect(&rect);
        }
      }
      else if (event.type == SDL_MOUSEMOTION)
      {
        ctrlSetPointer(browserSetMouse(ctrl_browser, event.motion.x, event.motion.y));
      }
      else if (event.type == SDL_MOUSEWHEEL)
      {
        browserScrollBy(ctrl_browser, event.wheel.x * -32, event.wheel.y * -32);
        want_redraw = SDL_TRUE;
      }
      else if (event.type == SDL_QUIT) { return; }
    }

    /* Redraw the canvas contents (if necessary) and update the display: */
    if (want_redraw) { viewDraw(ctrl_browser); }
    if (want_redraw || want_redisplay) { ctrlRepaint(ctrl_window); }
    want_redraw = want_redisplay = SDL_FALSE;

    /* Thread sleep (prevents constant 100% CPU usage). */
    SDL_Delay(1);
  }
}
