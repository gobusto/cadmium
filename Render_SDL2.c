/**
@file
@brief An SDL2 implementation of the functions outlined in webView.h

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include "Render.h"
#include "cadmium/Style.h"
#include "cadmium/encoding/unicode.h"

/**
@brief Used to store a specific variant of an individual font.
*/

typedef struct {

  int ptsize;     /**< @brief The size of the font. */
  TTF_Font *font; /**< @brief The actual font data. */

} GfxFontVariant;

/**
@brief Used to store a set of variations for an individual font.
*/

typedef struct {

  char *filename; /**< @brief The name of (and path to) the file to load. */

  int num_variants;         /**< @brief The number of variations for a font. */
  GfxFontVariant **variant; /**< @brief Data for all known variations.       */

} GfxFont;

/**
@brief Destroy a collection of font variants.

@param font The font to be destroyed.
@return Always returns `NULL`.
*/

static GfxFont *gfxFontDestroy(GfxFont *font)
{
  if (!font) { return NULL; }

  if (font->variant)
  {
    int i;
    for (i = 0; i < font->num_variants; ++i)
    {
      if (font->variant[i])
      {
        TTF_CloseFont(font->variant[i]->font);
        free(font->variant[i]);
      }
    }
    free(font->variant);
  }

  free(font->filename);

  free(font);
  return NULL;
}

/**
@brief Create a collection of font variants.

@return A new `GfxFont` structure on success, or `NULL` on failure.
*/

static GfxFont *gfxFontCreate(const char *filename)
{
  GfxFont *font;

  if (!filename) { return NULL; }

  font = (GfxFont*)malloc(sizeof(GfxFont));
  if (!font) { return NULL; }
  memset(font, 0, sizeof(GfxFont));

  font->filename = (char*)malloc(strlen(filename) + 1);
  if (!font->filename) { return gfxFontDestroy(font); }
  strcpy(font->filename, filename);

  return font;
}

/**
@brief Find (or create) a specific font variant.

@param ptsize The size we want.
@return The SDL font structure (if found), or `NULL` on failure.
*/

static TTF_Font *gfxFontFind(GfxFont *font, int ptsize)
{
  GfxFontVariant *variant;
  void *tmp;
  int i;

  if (!font || ptsize < 1) { return NULL; }

  for (i = 0; i < font->num_variants; ++i)
  {
    if (font->variant[i]->ptsize == ptsize) { return font->variant[i]->font; }
  }

  tmp = realloc(font->variant, sizeof(GfxFontVariant*) * (font->num_variants + 1));
  if (!tmp) { return NULL; }
  font->variant = (GfxFontVariant**)tmp;

  variant = (GfxFontVariant*)malloc(sizeof(GfxFontVariant));
  if (!variant) { return NULL; }
  memset(variant, 0, sizeof(GfxFontVariant));

  variant->ptsize = ptsize;
  variant->font = TTF_OpenFont(font->filename, ptsize);

  fprintf(stderr, "[DEBUG] Caching Size %d %s\n", ptsize, font->filename);
  if (!variant->font)
  {
    fprintf(stderr, "[WARNING] Could not load %s (size %d)\n", font->filename, ptsize);
    free(variant);
    return NULL;
  }

  font->variant[font->num_variants] = variant;
  font->num_variants++;
  return variant->font;
}

/** @brief Unicode Character 'WHITE QUESTION MARK ORNAMENT' (U+2754) */

const unsigned char gfx_emoji_placeholder[4] = { 0xE2, 0x9D, 0x94, 0x00 };

/* Global variables: */

SDL_Surface *gfx_surface = NULL;  /**< @brief The main "display" surface. */

GfxFont *gfx_font_icon = NULL;      /**< @brief For icon characters only. */
GfxFont *gfx_font_emoji = NULL;     /**< @brief For emoji characters only. */
GfxFont *gfx_font_variable = NULL;  /**< @brief For non-monospaced text.   */
GfxFont *gfx_font_monospace = NULL; /**< @brief For everything else.       */

/**
@brief writeme

@param box about
@param rgba about
@return writeme
*/

static TTF_Font *gfxFontFor(const Box *box, SDL_Color *fg, SDL_Color *bg)
{
  TTF_Font *font;
  Style style;
  int flags;

  if (styleCalculated(box, &style) != ERROR_SUCCESS) { return NULL; }

  switch (style.font_family)
  {
    case STYLE_FONT_ICON:
      font = gfxFontFind(gfx_font_icon, style.font_size);
    break;

    case STYLE_FONT_EMOJI:
      font = gfxFontFind(gfx_font_emoji, style.font_size);
    break;

    case STYLE_FONT_VARIABLE:
      font = gfxFontFind(gfx_font_variable, style.font_size);
    break;

    case STYLE_FONT_MONOSPACE:
    case STYLE_FONT_INHERIT:
    default:
      font = gfxFontFind(gfx_font_monospace, style.font_size);
    break;
  }

  if (!font) { return NULL; }

  flags = 0;
  if (style.font_weight == STYLE_WEIGHT_BOLD)
    flags |= TTF_STYLE_BOLD;
  if (style.font_style == STYLE_SLANT_ITALIC)
    flags |= TTF_STYLE_ITALIC;
  if (style.text_decoration == STYLE_UNDERLINE_SINGLE)
    flags |= TTF_STYLE_UNDERLINE;
  TTF_SetFontStyle(font, flags);

  if (fg)
  {
    fg->r = style.color[0];
    fg->g = style.color[1];
    fg->b = style.color[2];
    fg->a = style.color[3];
  }

  if (bg)
  {
    bg->r = style.background[0];
    bg->g = style.background[1];
    bg->b = style.background[2];
    bg->a = style.background[3];
  }

  return font;
}

/*
[PUBLIC] Initialise the rendering subsystem.
*/

error_t viewInit(int w, int h)
{
  error_t error;

  viewQuit();

  error = viewResize(w, h);
  if (error != ERROR_SUCCESS) { return error; }

  if (TTF_Init() != 0) { return ERROR_FILE_IO; }
  gfx_font_icon = gfxFontCreate("fonts/fontawesome-webfont.ttf");
  gfx_font_emoji = gfxFontCreate("fonts/TwitterColorEmoji-SVGinOT.ttf");
  gfx_font_variable = gfxFontCreate("fonts/OpenSans-Regular.ttf");
  gfx_font_monospace = gfxFontCreate("fonts/DroidSansMono.ttf");
  if (!gfx_font_emoji || !gfx_font_variable || !gfx_font_monospace) { return ERROR_OUT_OF_MEMORY; }

  return ERROR_SUCCESS;
}

/*
[PUBLIC] Shut down the rendering subsystem.
*/

void viewQuit(void)
{
  gfx_font_icon = gfxFontDestroy(gfx_font_icon);
  gfx_font_emoji = gfxFontDestroy(gfx_font_emoji);
  gfx_font_variable = gfxFontDestroy(gfx_font_variable);
  gfx_font_monospace = gfxFontDestroy(gfx_font_monospace);

  SDL_FreeSurface(gfx_surface);
  gfx_surface = NULL;

  TTF_Quit();
}

/*
[PUBLIC] Resize the canvas.
*/

error_t viewResize(int w, int h)
{
  if (w < 1 || h < 1) { return ERROR_INVALID_PARAMETER; }

  /*
  This file was originally implemented using Cairo rather than SDL2; the format
  of `gfx_surface` is set to `SDL_PIXELFORMAT_ARGB8888` to match how Cairo used
  `cairo_image_surface_create(CAIRO_FORMAT_RGB24, w, h)`, just in case I decide
  to switch back to using Cairo again (or something else) in the future.
  */

  SDL_FreeSurface(gfx_surface);
  gfx_surface = SDL_CreateRGBSurfaceWithFormat(0, w, h, 32, SDL_PIXELFORMAT_ARGB8888);
  return gfx_surface ? ERROR_SUCCESS : ERROR_OUT_OF_MEMORY;
}

/**
@brief about

SDL2_TTF can't handle Unicode values above 0xFFFF, so we replace any emoji-type
text above that value with a "placeholder" emoji from the BMP range instead.

@param box about
@return writeme
*/

static const char *gfxTextFor(const Box *box)
{
  long c;

  if (!box)
    return NULL;

  if (box->kind == BOX_EMOJI && utfDecodeUTF8(box->text, &c) > 0 && c > 0xFFFF)
    return (const char*)gfx_emoji_placeholder;

  return box->text;
}

/**
@brief about

@param box about
@param encoding about
@param x about
@param y about
@param w about
@return writeme
*/

static error_t gfxDrawText(const Box *box, TextEncoding encoding, int x, int y, int w)
{
  SDL_Surface *surface;
  TTF_Font *font;
  SDL_Color fg;
  SDL_Color bg;
  SDL_Rect src_rect;
  SDL_Rect dst_rect;

  if (!box || !box->text) { return ERROR_INVALID_PARAMETER; }
  font = gfxFontFor(box, &fg, &bg);
  if (!font) { return ERROR_INVALID_PARAMETER; }

  if (encoding == TEXT_ENCODING_UTF8)
    surface = TTF_RenderUTF8_Shaded(font, gfxTextFor(box), fg, bg);
  else
    surface = TTF_RenderText_Shaded(font, gfxTextFor(box), fg, bg);
  if (!surface) { return ERROR_OUT_OF_MEMORY; }

  src_rect.x = 0;
  src_rect.y = 0;
  src_rect.w = w;
  src_rect.h = surface->h;

  dst_rect.x = x;
  dst_rect.y = y;
  dst_rect.w = surface->w;
  dst_rect.h = surface->h;

  SDL_BlitSurface(surface, &src_rect, gfx_surface, &dst_rect);
  SDL_FreeSurface(surface);

  return ERROR_SUCCESS;
}

/**
@brief about

@param x about
@param y about
@param bitmap about
@return writeme
*/

static error_t gfxDrawImage(int x, int y, BitmapImage *bitmap)
{
  SDL_Surface *surface;
  SDL_Rect rect;
  Uint32 format;
  int stride;

  if (!bitmap) { return ERROR_INVALID_PARAMETER; }

  rect.x = x;
  rect.y = y;
  rect.w = bitmap->w;
  rect.h = bitmap->h;

  switch (bitmap->bpp)
  {
    case 24: format = SDL_PIXELFORMAT_RGB24;  break;
    case 32: format = SDL_PIXELFORMAT_RGBA32; break;
    default: return ERROR_INVALID_PARAMETER;
  }

  stride = bitmap->w * bitmap->bpp/8;
  surface = SDL_CreateRGBSurfaceWithFormatFrom(
    bitmap->data, bitmap->w, bitmap->h, bitmap->bpp, stride, format
  );
  if (!surface) { return ERROR_OUT_OF_MEMORY; }

  SDL_BlitSurface(surface, NULL, gfx_surface, &rect);
  SDL_FreeSurface(surface);

  return ERROR_SUCCESS;
}

/**
@brief about

@param browser about
@param box about
@param encoding about
@param x about
@param y about
@return writeme
*/

static error_t gfxDrawBox(const Browser *browser, Box *box, TextEncoding encoding, int x, int y)
{
  SDL_Rect content;
  SDL_Rect border;
  Style style;
  unsigned long i;

  if (!box)
    return ERROR_INVALID_PARAMETER;

  if (box->kind == BOX_TEXT || box->kind == BOX_EMOJI || box->kind == BOX_ICON)
    return gfxDrawText(box, encoding, x + box->x, y + box->y, box->w);

  styleDirect(box, &style);

  /* The "content" area of a box excludes the margin and border: */
  content.x = x + box->x + (
    style.margin[STYLE_SIDE_LEFT] + style.border[STYLE_SIDE_LEFT]
  );
  content.y = y + box->y + (
    style.margin[STYLE_SIDE_TOP] + style.border[STYLE_SIDE_TOP]
  );
  content.w = box->w - (
    style.margin[STYLE_SIDE_LEFT] + style.margin[STYLE_SIDE_RIGHT] +
    style.border[STYLE_SIDE_LEFT] + style.border[STYLE_SIDE_RIGHT]
  );
  content.h = box->h - (
    style.margin[STYLE_SIDE_TOP] + style.margin[STYLE_SIDE_BOTTOM] +
    style.border[STYLE_SIDE_TOP] + style.border[STYLE_SIDE_BOTTOM]
  );

  if (style.background[3] > 0)
    SDL_FillRect(gfx_surface, &content, SDL_MapRGB(
      gfx_surface->format,
      style.background[0], style.background[1], style.background[2]
    ));

  /* Top border: */
  border.x = content.x - style.border[STYLE_SIDE_LEFT];
  border.y = content.y - style.border[STYLE_SIDE_TOP];
  border.w = content.w + style.border[STYLE_SIDE_LEFT] + style.border[STYLE_SIDE_RIGHT];
  border.h = style.border[STYLE_SIDE_TOP];

  if (border.h > 0 && style.border_color[3] > 0)
    SDL_FillRect(gfx_surface, &border, SDL_MapRGB(
      gfx_surface->format,
      style.border_color[0], style.border_color[1], style.border_color[2]
    ));

  /* Left border: */
  border.w = style.border[STYLE_SIDE_LEFT];
  border.h = content.h + style.border[STYLE_SIDE_TOP] + style.border[STYLE_SIDE_BOTTOM];

  if (border.h > 0 && style.border_color[3] > 0)
    SDL_FillRect(gfx_surface, &border, SDL_MapRGB(
      gfx_surface->format,
      style.border_color[0], style.border_color[1], style.border_color[2]
    ));

  /* Right border: */
  border.x = content.x + content.w;
  border.w = style.border[STYLE_SIDE_RIGHT];

  if (border.h > 0 && style.border_color[3] > 0)
    SDL_FillRect(gfx_surface, &border, SDL_MapRGB(
      gfx_surface->format,
      style.border_color[0], style.border_color[1], style.border_color[2]
    ));

  /* Bottom border: */
  border.x = content.x - style.border[STYLE_SIDE_LEFT];
  border.y = content.y + content.h;
  border.w = content.w + style.border[STYLE_SIDE_LEFT] + style.border[STYLE_SIDE_RIGHT];
  border.h = style.border[STYLE_SIDE_BOTTOM];

  if (border.h > 0 && style.border_color[3] > 0)
    SDL_FillRect(gfx_surface, &border, SDL_MapRGB(
      gfx_surface->format,
      style.border_color[0], style.border_color[1], style.border_color[2]
    ));

  /* Finally, draw any sub-boxes: */
  for (i = 0; i < box->num_boxes; ++i)
    gfxDrawBox(browser, box->box[i], encoding, x + box->x, y + box->y);

  /* Special case: Draw the actual text content of a text box: */
  if (box->kind == BOX_GUI_INPUT)
  {
    content.x += style.padding[STYLE_SIDE_LEFT];
    content.y += style.padding[STYLE_SIDE_TOP];
    content.w -= style.padding[STYLE_SIDE_LEFT] + style.padding[STYLE_SIDE_RIGHT];
    gfxDrawText(box, TEXT_ENCODING_UTF8, content.x, content.y, content.w);

    /* If it's focussed, draw a text cursor: */
    if (boxRoot(box)->user_data == box)
    {
      TTF_Font *font = gfxFontFor(box, NULL, NULL);
      SDL_Rect ibeam;

      /* Get the full text width, then subtract the pixels after the cursor: */
      if (font)
      {
        const char *text = box->text ? box->text : " ";
        TTF_SizeUTF8(font, text, &ibeam.x, &ibeam.h);
        TTF_SizeUTF8(font, &text[browser->text_cursor_position], &ibeam.w, NULL);
        ibeam.x -= ibeam.w;
      }
      else
      {
        ibeam.x = style.font_size * browser->text_cursor_position;
        ibeam.h = style.font_size;
      }

      /* Offset the cursor to account for margins/borders/padding: */
      ibeam.x += content.x;
      ibeam.y = content.y;
      ibeam.w = 1;
      SDL_FillRect(gfx_surface, &ibeam, SDL_MapRGB(gfx_surface->format, 128, 128, 128));
    }
  }

  return ERROR_SUCCESS;
}

/**
@brief about

@param browser about
@param model about
@return writeme
*/

static error_t gfxDrawModel(const Browser *browser, const Model *model)
{
  Box *box;
  TextEncoding encoding;
  int offset_x, offset_y;

  if (!model)
    return ERROR_INVALID_PARAMETER;

  modelGetScroll(model, &offset_x, &offset_y);
  offset_y -= browser->gui->h;

  if (model->bitmap)
    return gfxDrawImage(-offset_x, -offset_y, model->bitmap);

  if (browser->flags & BOX_FLAG_MARKDOWN)
    box = model->fancy_layout;
  else
    box = model->basic_layout;
  encoding = model->encoding;

  if (!box)
  {
    encoding = TEXT_ENCODING_UTF8;
    offset_x = 0;
    offset_y = 0 - browser->gui->h;

    if (modelState(model) == MODEL_FINISHED)
      box = browser->undisplayable;
    else if (modelState(model) == MODEL_RECEIVING)
      box = browser->loading;
    else
      box = NULL;
  }

  return gfxDrawBox(browser, box, encoding, -offset_x, -offset_y);
}

/*
[PUBLIC] Draw a visual representation of a browser on the canvas.
*/

void viewDraw(const Browser *browser)
{
  if (!gfx_surface || !browser) { return; }
  SDL_FillRect(gfx_surface, NULL, SDL_MapRGB(gfx_surface->format, 204, 204, 204));
  gfxDrawModel(browser, browserGetModel(browser));
  gfxDrawBox(browser, browser->gui, TEXT_ENCODING_UTF8, 0, 0);
}

/*
[PUBLIC] Get a pointer to the raw canvas surface pixel data.
*/

unsigned char *viewGetData(int *stride)
{
  if (stride && gfx_surface) { *stride = gfx_surface->pitch; }
  return gfx_surface->pixels;
}

/*
[PUBLIC] Determine the bounding box size of a given string of text.
*/

error_t viewGetTextExtents(const Box *box, TextEncoding encoding, int *w, int *h)
{
  const char *text = " ";
  TTF_Font *font;

  if (!box) { return ERROR_INVALID_PARAMETER; }

  /* Treat input boxes as being "empty", as the text doesn't determine size: */
  if (box->kind != BOX_GUI_INPUT && box->text) { text = gfxTextFor(box); }

  font = gfxFontFor(box, NULL, NULL);
  if (!font) { return ERROR_FILE_IO; }

  switch (encoding)
  {
    case TEXT_ENCODING_UTF8:
      if (TTF_SizeUTF8(font, text, w, h) != 0)
        return ERROR_INVALID_PARAMETER;
      return ERROR_SUCCESS;

    case TEXT_ENCODING_LATIN1:
      if (TTF_SizeText(font, text, w, h) != 0)
        return ERROR_INVALID_PARAMETER;
      return ERROR_SUCCESS;

    case TEXT_ENCODING_UNKNOWN: default: break;
  }

  return ERROR_INVALID_PARAMETER;
}
